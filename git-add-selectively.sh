#!/usr/bin/env /bin/bash

IFS=$'\n'
regex="^([^[:blank:]]*)[[:blank:]]([^[:blank:]]*)[[:blank:]](.*)$"

for file in $(git diff --numstat)
do
  if [[ $file =~ $regex ]]; then

    inserstions="${BASH_REMATCH[1]}"
    deletions="${BASH_REMATCH[2]}"
    filepath="${BASH_REMATCH[3]}"

    if [[ $file == *.tfs ]]; then
        if [[ $((inserstions)) -eq 2 ]] && [[ $((deletions)) -eq 2 ]]; then
          echo "No changes in '$filepath', ignoring the file..."
        else
          echo "Changes in '$filepath', staging the file for commit..."
          git add "./$filepath"
        fi
    else
        if [[ $((inserstions)) -eq 1 ]] && [[ $((deletions)) -eq 1 ]]; then
          echo "No changes in '$filepath', ignoring the file..."
        else
          echo "Changes in '$filepath', staging the file for commit..."
          git add "./$filepath"
        fi
    fi
  fi
done

# Add all untracked files
echo -e "a\n*\nq\n"|git add -i
