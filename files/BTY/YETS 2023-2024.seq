

  /**********************************************************************************
  *
  * BTY version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 11-JUL-2023 02:09:17 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.BTY_BCTF_002                 := 0;
l.BTY_BPUIA003                 := 0;
l.BTY_MBXFBCWP                 := 1;
l.BTY_MCCAA005                 := 0;
l.BTY_MCCAA006                 := 0;
l.BTY_MQNCDNWP                 := 0.56;
l.BTY_MQNEETWC                 := 1;
l.BTY_OMK                      := 0;

//---------------------- MARKER         ---------------------------------------------
BTY_OMK        : MARKER      , L := l.BTY_OMK;           ! BTY markers
//---------------------- MONITOR        ---------------------------------------------
BTY_BCTF_002   : MONITOR     , L := l.BTY_BCTF_002;      ! Fast Beam Current Transformers (Booster Ejection Lines)
BTY_BPUIA003   : MONITOR     , L := l.BTY_BPUIA003;      ! Beam Position Pick-up Inductive type A
//---------------------- QUADRUPOLE     ---------------------------------------------
BTY_MQNCDNWP   : QUADRUPOLE  , L := l.BTY_MQNCDNWP;      ! Quadrupole magnet, type Q130, 0.5m
BTY_MQNEETWC   : QUADRUPOLE  , L := l.BTY_MQNEETWC;      ! Quadrupole magnet, type Q100, 1m
BTY_MQNEETWC   : QUADRUPOLE  , L := l.BTY_MQNEETWC;      ! Quadrupole magnet, type Q100, 1m - Magnetic length taken from EDMS 1786085
//---------------------- SBEND          ---------------------------------------------
BTY_MBXFBCWP   : SBEND       , L := l.BTY_MBXFBCWP;      ! Bending Magnet, H or V, type HB4, 1m gap 80mm
//---------------------- VKICKER        ---------------------------------------------
BTY_MCCAA005   : VKICKER     , L := l.BTY_MCCAA005;      ! Corrector magnet, H+V, type 1 (vertical function for BTY)
BTY_MCCAA006   : VKICKER     , L := l.BTY_MCCAA006;      ! Corrector magnet, H+V, type 1 (horizontal function for BTY)


/************************************************************************************/
/*                       BTY SEQUENCE                                                */
/************************************************************************************/

BTYGPS : SEQUENCE, refer = centre,      L = 99.173243831;
 BTY.BVT101                    : BTY_MBXFBCWP    , at = .8721705     , slot_id = 6154779;
 BTY.VVS101                    : BTY_OMK         , at = 1.821338     , slot_id = 54959055;
 BTY.STP103.ENTRYFACE          : BTY_OMK         , at = 3.885828     , slot_id = 6154780;
 BTY.STP103                    : BTY_OMK         , at = 4.185828     , slot_id = 6154780;
 BTY.QDE104                    : BTY_MQNCDNWP    , at = 5.070318     , slot_id = 54953953;
 BTY.QFO108                    : BTY_MQNCDNWP    , at = 9.276238     , slot_id = 54954842;
 BTY.QDE113                    : BTY_MQNCDNWP    , at = 13.482158    , slot_id = 54954913;
 BTY.BCT114                    : BTY_BCTF_002    , at = 15.176908    , slot_id = 54955521;
 BTY.BTV115                    : BTY_OMK         , at = 16.575658    , slot_id = 42759420;
 BTY.BVT116                    : BTY_MBXFBCWP    , at = 17.6250765   , slot_id = 54954949;
 BTY.DHZ118                    : BTY_MCCAA006    , at = 19.290244    , slot_id = 54959104, assembly_id= 56449854;
 BTY.DVT118                    : BTY_MCCAA005    , at = 19.290244    , slot_id = 55322577, assembly_id= 56449854;
 BTY.QFO119                    : BTY_MQNCDNWP    , at = 19.923244    , slot_id = 54954985;
 BTY.BPM119                    : BTY_BPUIA003    , at = 20.611744    , slot_id = 54955021;
 BTY.QDE120                    : BTY_MQNCDNWP    , at = 21.303244    , slot_id = 54955057;
 BTY.QFO122                    : BTY_MQNCDNWP    , at = 23.923244    , slot_id = 54955093;
 BTY.BTV123                    : BTY_OMK         , at = 24.531744    , slot_id = 42759421;
 BTY.DHZ124                    : BTY_MCCAA006    , at = 24.938744    , slot_id = 54959149, assembly_id= 56449890;
 BTY.DVT124                    : BTY_MCCAA005    , at = 24.938744    , slot_id = 54959187, assembly_id= 56449890;
 BTY.QFO148                    : BTY_MQNCDNWP    , at = 48.923244    , slot_id = 54955135;
 BTY.VVS149                    : BTY_OMK         , at = 49.922244    , slot_id = 54959225;
 BTY.BTV150                    : BTY_OMK         , at = 51.177844    , slot_id = 54955182;
 BTY.QDE151                    : BTY_MQNCDNWP    , at = 51.786344    , slot_id = 54955218;
 BTY.BPM152                    : BTY_BPUIA003    , at = 52.403444    , slot_id = 54955254;
 BTY.DHZ152                    : BTY_MCCAA006    , at = 52.819844    , slot_id = 54959261, assembly_id= 56449926;
 BTY.DVT152                    : BTY_MCCAA005    , at = 52.819844    , slot_id = 54959297, assembly_id= 56449926;
 BTY.QFO153                    : BTY_MQNCDNWP    , at = 53.423344    , slot_id = 54955290;
 BTY.QFO179                    : BTY_MQNCDNWP    , at = 79.743344    , slot_id = 54955331;
 BTY.BTV180                    : BTY_OMK         , at = 80.351844    , slot_id = 54955367;
 BTY.QDE182                    : BTY_MQNCDNWP    , at = 81.818844    , slot_id = 54955404;
 BTY.BCT183                    : BTY_BCTF_002    , at = 82.618844    , slot_id = 54955483;
 BTY.QFO184                    : BTY_MQNCDNWP    , at = 83.735944    , slot_id = 54955447;
 BTY.BHZ301                    : BTY_MBXFBCWP    , at = 85.008111652 , slot_id = 54962546;
 BTY.DHZ201                    : BTY_MCCAA006    , at = 86.609244    , slot_id = 54959333, assembly_id= 56449962;
 BTY.DVT201                    : BTY_MCCAA005    , at = 86.609244    , slot_id = 55322614, assembly_id= 56449962;
 BTY.BPM202                    : BTY_BPUIA003    , at = 87.409244    , slot_id = 54958670;
 BTY.QDE209                    : BTY_MQNEETWC    , at = 93.339244    , slot_id = 54958928;
 BTY.QFO210                    : BTY_MQNEETWC    , at = 94.823244    , slot_id = 54958967;
 BTY.DHZ211                    : BTY_MCCAA006    , at = 95.998244    , slot_id = 54959369, assembly_id= 56450002;
 BTY.DVT212                    : BTY_MCCAA005    , at = 96.348244    , slot_id = 54959412, assembly_id= 56450038;
 BTY.BCT213                    : BTY_BCTF_002    , at = 96.967244    , slot_id = 54959004;
 BTY.BSG214                    : BTY_OMK         , at = 97.767244    , slot_id = 57524545;
 BTY.NTB215                    : BTY_OMK         , at = 99.175244    , slot_id = 55310795;
ENDSEQUENCE;

BTYHRS : SEQUENCE, refer = centre,      L = 110.923147294;
 BTY.BVT101                    : BTY_MBXFBCWP    , at = .8721705     , slot_id = 6154779;
 BTY.VVS101                    : BTY_OMK         , at = 1.821338     , slot_id = 54959055;
 BTY.STP103.ENTRYFACE          : BTY_OMK         , at = 3.885828     , slot_id = 6154780;
 BTY.STP103                    : BTY_OMK         , at = 4.185828     , slot_id = 6154780;
 BTY.QDE104                    : BTY_MQNCDNWP    , at = 5.070318     , slot_id = 54953953;
 BTY.QFO108                    : BTY_MQNCDNWP    , at = 9.276238     , slot_id = 54954842;
 BTY.QDE113                    : BTY_MQNCDNWP    , at = 13.482158    , slot_id = 54954913;
 BTY.BCT114                    : BTY_BCTF_002    , at = 15.176908    , slot_id = 54955521;
 BTY.BTV115                    : BTY_OMK         , at = 16.575658    , slot_id = 42759420;
 BTY.BVT116                    : BTY_MBXFBCWP    , at = 17.6250765   , slot_id = 54954949;
 BTY.DHZ118                    : BTY_MCCAA006    , at = 19.290244    , slot_id = 54959104, assembly_id= 56449854;
 BTY.DVT118                    : BTY_MCCAA005    , at = 19.290244    , slot_id = 55322577, assembly_id= 56449854;
 BTY.QFO119                    : BTY_MQNCDNWP    , at = 19.923244    , slot_id = 54954985;
 BTY.BPM119                    : BTY_BPUIA003    , at = 20.611744    , slot_id = 54955021;
 BTY.QDE120                    : BTY_MQNCDNWP    , at = 21.303244    , slot_id = 54955057;
 BTY.QFO122                    : BTY_MQNCDNWP    , at = 23.923244    , slot_id = 54955093;
 BTY.BTV123                    : BTY_OMK         , at = 24.531744    , slot_id = 42759421;
 BTY.DHZ124                    : BTY_MCCAA006    , at = 24.938744    , slot_id = 54959149, assembly_id= 56449890;
 BTY.DVT124                    : BTY_MCCAA005    , at = 24.938744    , slot_id = 54959187, assembly_id= 56449890;
 BTY.QFO148                    : BTY_MQNCDNWP    , at = 48.923244    , slot_id = 54955135;
 BTY.VVS149                    : BTY_OMK         , at = 49.922244    , slot_id = 54959225;
 BTY.BTV150                    : BTY_OMK         , at = 51.177844    , slot_id = 54955182;
 BTY.QDE151                    : BTY_MQNCDNWP    , at = 51.786344    , slot_id = 54955218;
 BTY.BPM152                    : BTY_BPUIA003    , at = 52.403444    , slot_id = 54955254;
 BTY.DHZ152                    : BTY_MCCAA006    , at = 52.819844    , slot_id = 54959261, assembly_id= 56449926;
 BTY.DVT152                    : BTY_MCCAA005    , at = 52.819844    , slot_id = 54959297, assembly_id= 56449926;
 BTY.QFO153                    : BTY_MQNCDNWP    , at = 53.423344    , slot_id = 54955290;
 BTY.QFO179                    : BTY_MQNCDNWP    , at = 79.743344    , slot_id = 54955331;
 BTY.BTV180                    : BTY_OMK         , at = 80.351844    , slot_id = 54955367;
 BTY.QDE182                    : BTY_MQNCDNWP    , at = 81.818844    , slot_id = 54955404;
 BTY.BCT183                    : BTY_BCTF_002    , at = 82.618844    , slot_id = 54955483;
 BTY.QFO184                    : BTY_MQNCDNWP    , at = 83.735944    , slot_id = 54955447;
 BTY.BHZ301                    : BTY_MBXFBCWP    , at = 85.008111652 , slot_id = 54962546;
 BTY.DHZ303                    : BTY_MCCAA006    , at = 88.256279473 , slot_id = 54962582, assembly_id= 56450074;
 BTY.DVT303                    : BTY_MCCAA005    , at = 88.256279473 , slot_id = 55322652, assembly_id= 56450074;
 BTY.QFO304                    : BTY_MQNCDNWP    , at = 89.006279473 , slot_id = 54962618;
 BTY.BTV305                    : BTY_OMK         , at = 89.614779473 , slot_id = 54962662;
 BTY.BHZ308                    : BTY_MBXFBCWP    , at = 93.006279473 , slot_id = 54962698;
 BTY.QDE310                    : BTY_MQNCDNWP    , at = 95.26814729  , slot_id = 54962734;
 BTY.BPM311                    : BTY_BPUIA003    , at = 96.04664729  , slot_id = 54962770;
 BTY.QFO311                    : BTY_MQNCDNWP    , at = 96.82514729  , slot_id = 54962806;
 BTY.QDE321                    : BTY_MQNEETWC    , at = 105.08914729 , slot_id = 54962842;
 BTY.QFO322                    : BTY_MQNEETWC    , at = 106.57314729 , slot_id = 54962878;
 BTY.DHZ323                    : BTY_MCCAA006    , at = 107.74814729 , slot_id = 54962916, assembly_id= 56450110;
 BTY.DVT324                    : BTY_MCCAA005    , at = 108.09814729 , slot_id = 54962952, assembly_id= 56450146;
 BTY.BCT325                    : BTY_BCTF_002    , at = 108.71714729 , slot_id = 54962988;
 BTY.BSG326                    : BTY_OMK         , at = 109.517148   , slot_id = 57524582;
 BTY.NTB327                    : BTY_OMK         , at = 110.925147   , slot_id = 55310819;
ENDSEQUENCE;


return;