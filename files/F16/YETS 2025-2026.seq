

  /**********************************************************************************
  *
  * F16 version (draft) YETS 2025-2026 in MAD X SEQUENCE format
  * Generated the 30-OCT-2024 02:17:25 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.F16_BCTF                     := 0;
l.F16_BCTFI                    := 0.482;
l.F16_BPCL                     := 0.72;
l.F16_BTVRB                    := 0;
l.F16_DFA                      := 0;
l.F16_MBXFACWP                 := 1;
l.F16_MBXGACWP                 := 2.2;
l.F16_MBXGBCWP                 := 1.85;
l.F16_MBXGCCWP                 := 1.4;
l.F16_MBXHCCWP                 := 2.5;
l.F16_MCXCFHWP                 := 0.4;
l.F16_MQNCDNWP                 := 0.56;
l.F16_MQNCN8WC                 := 0.645;
l.F16_MQNDCTWP                 := 0.82;
l.F16_MQNEFTWP                 := 1.2;
l.F16_MQNEGTWP                 := 0.8;
l.F16_MSG                      := 0;
l.F16_MTO                      := 0;
l.F16_OMK                      := 0;
l.F16_TDE__004                 := 0;
l.F16_TRA                      := 0;
l.F16_UES                      := 0;

//---------------------- HKICKER        ---------------------------------------------
F16_DFA                  : HKICKER     , L := l.F16_DFA;           ! Fast rising Dipole Kicker
F16_MCXCFHWP             : HKICKER     , L := l.F16_MCXCFHWP;      ! Corrector magnet, H or V, type MDX laminated aperture 150mm
//---------------------- INSTRUMENT     ---------------------------------------------
F16_BCTFI                : INSTRUMENT  , L := l.F16_BCTFI;         ! Fast Beam Current Transformer for the Transfer Lines
//---------------------- MARKER         ---------------------------------------------
F16_OMK                  : MARKER      , L := l.F16_OMK;           ! F16 markers
//---------------------- MONITOR        ---------------------------------------------
F16_BCTF                 : MONITOR     , L := l.F16_BCTF;          ! Fast Beam Current Transformers
F16_BPCL                 : MONITOR     , L := l.F16_BPCL;          ! beam position coupler, large
F16_BTVRB                : MONITOR     , L := l.F16_BTVRB;         ! Beam observation TV Rotatory screens type B
F16_MSG                  : MONITOR     , L := l.F16_MSG;           ! SEM grid
F16_MTO                  : MONITOR     , L := l.F16_MTO;           ! MT DIVISION DESIGN OFFICE
F16_TRA                  : MONITOR     , L := l.F16_TRA;           ! Beam Current Transformer Fast
F16_UES                  : MONITOR     , L := l.F16_UES;           ! UES Pick-up Monitor
//---------------------- PLACEHOLDER    ---------------------------------------------
F16_TDE__004             : PLACEHOLDER , L := l.F16_TDE__004;      ! Beam Dump
//---------------------- QUADRUPOLE     ---------------------------------------------
F16_MQNCDNWP             : QUADRUPOLE  , L := l.F16_MQNCDNWP;      ! Quadrupole magnet, type Q130, 0.5m
F16_MQNCN8WC             : QUADRUPOLE  , L := l.F16_MQNCN8WC;      ! Quadrupole magnet, type Q101
F16_MQNDCTWP             : QUADRUPOLE  , L := l.F16_MQNDCTWP;      ! Quadrupole magnet, ISR, type QDS, 0.82m
F16_MQNEFTWP             : QUADRUPOLE  , L := l.F16_MQNEFTWP;      ! Quadrupole magnet, type QFL, 1.2m
F16_MQNEGTWP             : QUADRUPOLE  , L := l.F16_MQNEGTWP;      ! Quadrupole magnet, type QFS, 0.8m
//---------------------- RBEND          ---------------------------------------------
F16_MBXFACWP             : RBEND       , L := l.F16_MBXFACWP;      ! Bending Magnet, H or V, type VB3, 1m gap 108mm - Magnetic length according to EDMS 1786052
F16_MBXGACWP             : RBEND       , L := l.F16_MBXGACWP;      ! Bending Magnet, H or V, type VB4, 2.2m gap 108mm
F16_MBXGBCWP             : RBEND       , L := l.F16_MBXGBCWP;      ! Bending Magnet, H or V, type HB3, 1.85m gap 80mm - Mechanical length based on drawing ISR 225-174-0. Height and depth from NORMA database.
F16_MBXGCCWP             : RBEND       , L := l.F16_MBXGCCWP;      ! Bending Magnet, H or V, type HB5, 1.4m gap 80mm
F16_MBXHCCWP             : RBEND       , L := l.F16_MBXHCCWP;      ! Bending Magnet, H or V, type HB1, 2.5m gap 80mm


/************************************************************************************/
/*                       F16 SEQUENCE                                                */
/************************************************************************************/

F16 : SEQUENCE, refer = centre,         L = 335.8975;
 F16.QFN105                    : F16_MQNCN8WC           , at = .3225        , slot_id = 5359456;
 F16.BPM106                    : F16_BPCL               , at = 4.53213      , slot_id = 5359515;
 F16.BTV107                    : F16_BTVRB              , at = 5.29213      , slot_id = 5359457;
 F16.BHZ117                    : F16_MBXGBCWP           , at = 7.0345       , slot_id = 5359458;
 F16.QDN120                    : F16_MQNEFTWP           , at = 9.2595       , slot_id = 5359459;
 F16.BVT123                    : F16_MBXGACWP           , at = 11.6595      , slot_id = 5359460;
 F16.BCTF126                   : F16_BCTF               , at = 13.4145      , slot_id = 5474898;
 F16.QFN135                    : F16_MQNEFTWP           , at = 15.6755      , slot_id = 5353274;
 F16.BTV138                    : F16_BTVRB              , at = 16.6705      , slot_id = 5359461;
 F16.MTO142                    : F16_MTO                , at = 18.0295      , slot_id = 5359463;
 F16.BHZ147                    : F16_MBXGBCWP           , at = 19.5715      , slot_id = 5359464;
 F16.QDN150                    : F16_MQNEFTWP           , at = 29.0035      , slot_id = 5359465;
 F16.BPM151                    : F16_BPCL               , at = 31.6835      , slot_id = 5359466;
 F16.STP152                    : F16_OMK                , at = 32.79541     , slot_id = 53635944;
 F16.STP154                    : F16_OMK                , at = 33.57361     , slot_id = 53635990;
 F16.QDN163                    : F16_MQNCDNWP           , at = 37.40091     , slot_id = 5359468;
 F16.QFN165                    : F16_MQNEFTWP           , at = 38.9064      , slot_id = 5359469;
 F16.BHZ167                    : F16_MBXGCCWP           , at = 40.9064      , slot_id = 5359470;
 F16.BVT173                    : F16_MBXGACWP           , at = 44.2594      , slot_id = 5359471;
 F16.STP176                    : F16_OMK                , at = 46.3314      , slot_id = 53636026;
 F16.QDN180                    : F16_MQNEFTWP           , at = 47.9254      , slot_id = 5359473;
 F16.BTV201                    : F16_BTVRB              , at = 55.0344      , slot_id = 5359474;
 F16.BCTF203                   : F16_TRA                , at = 55.9524      , slot_id = 5359476;
 F16.QFN205                    : F16_MQNEFTWP           , at = 57.0914      , slot_id = 5359477;
 F16.QDN207                    : F16_MQNEFTWP           , at = 58.92406     , slot_id = 5359478;
 F16.BPMW208                   : F16_UES                , at = 62.3725      , slot_id = 54972629;
 F16.TSTR209.A                 : F16_OMK                , at = 68.8084      , slot_id = 56468508, assembly_id= 56468447;
 F16.TSTR209.B                 : F16_OMK                , at = 69.1084      , slot_id = 56468544, assembly_id= 56468447;
 F16.TSTR209.C                 : F16_OMK                , at = 69.7564      , slot_id = 56468580, assembly_id= 56468456;
 F16.TSTR209.D                 : F16_OMK                , at = 70.0564      , slot_id = 56468616, assembly_id= 56468456;
 F16.QDN210                    : F16_MQNDCTWP           , at = 71.7814      , slot_id = 5359480;
 F16.BPM211                    : F16_BPCL               , at = 73.1154      , slot_id = 5359481;
 F16.BCTF212                   : F16_BCTFI              , at = 75.6         , slot_id = 5494047;
 F16.QDN213                    : F16_MQNEGTWP           , at = 78.68762     , slot_id = 5359482;
 F16.QFN215                    : F16_MQNEFTWP           , at = 80.5314      , slot_id = 5359483;
 F16.QDN217                    : F16_MQNEGTWP           , at = 82.15394     , slot_id = 5359484;
 F16.BTV218                    : F16_BTVRB              , at = 88.5929      , slot_id = 5359485;
 F16.QDN220                    : F16_MQNEGTWP           , at = 89.7614      , slot_id = 5359486;
 F16.QFN225                    : F16_MQNEGTWP           , at = 98.9914      , slot_id = 5359487;
 F16.BPMW228                   : F16_UES                , at = 104.6015     , slot_id = 54972652;
 F16.BTV229                    : F16_BTVRB              , at = 107          , slot_id = 5359489;
 F16.QDN230                    : F16_MQNEGTWP           , at = 108.2214     , slot_id = 5359490;
 F16.QFN235                    : F16_MQNEGTWP           , at = 117.4514     , slot_id = 5359491;
 F16.QDN240                    : F16_MQNDCTWP           , at = 126.6814     , slot_id = 5359492;
 F16.BTV241                    : F16_BTVRB              , at = 127.0914     , slot_id = 5359493;
 F16.DFA242                    : F16_DFA                , at = 132.467      , slot_id = 14281374;
 F16.QFN245                    : F16_MQNEGTWP           , at = 135.9114     , slot_id = 5359494;
 F16.BTI247                    : F16_MBXHCCWP           , at = 138.1904     , slot_id = 5359495;
 F16.BTI248                    : F16_MBXHCCWP           , at = 142.086      , slot_id = 5359496;
 F16.QDN250                    : F16_MQNDCTWP           , at = 145.1415     , slot_id = 5359497;
 F16.BPM251                    : F16_BPCL               , at = 147.2515     , slot_id = 5359498;
 F16.DFA254                    : F16_DFA                , at = 152.9027     , slot_id = 5494049;
 F16.QFN255                    : F16_MQNEGTWP           , at = 154.3714     , slot_id = 5359499;
 F16.BSGF257                   : F16_MSG                , at = 162.0114     , slot_id = 54972698;
 F16.BSG258                    : F16_MSG                , at = 162.5964     , slot_id = 5359501;
 F16.QDN260                    : F16_MQNDCTWP           , at = 163.6014     , slot_id = 5359502;
 F16.QFN265                    : F16_MQNEGTWP           , at = 172.8314     , slot_id = 5359503;
 F16.BSGF267                   : F16_MSG                , at = 180.4714     , slot_id = 54972721;
 F16.BSG268                    : F16_MSG                , at = 181.0564     , slot_id = 5359505;
 F16.QDN270                    : F16_MQNDCTWP           , at = 182.0614     , slot_id = 5359506;
 F16.BPM274                    : F16_BPCL               , at = 190.0223     , slot_id = 5359507;
 F16.QFN275                    : F16_MQNEGTWP           , at = 191.2914     , slot_id = 5359508;
 F16.BSGF277                   : F16_MSG                , at = 198.9314     , slot_id = 54972744;
 F16.BSG278                    : F16_MSG                , at = 199.5164     , slot_id = 5359510;
 F16.QDN280                    : F16_MQNDCTWP           , at = 200.5214     , slot_id = 5359511;
 F16.QFN285                    : F16_MQNEGTWP           , at = 209.7514     , slot_id = 5359512;
 F16.QDN310                    : F16_MQNDCTWP           , at = 218.9814     , slot_id = 5359513;
 F16.BPM311                    : F16_BPCL               , at = 221.0694     , slot_id = 5359514;
 F16.QFN315                    : F16_MQNEGTWP           , at = 228.2114     , slot_id = 5359516;
 F16.QDN320                    : F16_MQNDCTWP           , at = 237.4414     , slot_id = 5359517;
 F16.QFN325                    : F16_MQNEGTWP           , at = 246.6714     , slot_id = 5359519;
 F16.BTV326                    : F16_BTVRB              , at = 247.4739     , slot_id = 5359520;
 F16.DHZ327                    : F16_MCXCFHWP           , at = 249.0199     , slot_id = 58655891;
 F16.QDN330                    : F16_MQNDCTWP           , at = 255.9014     , slot_id = 5359521;
 F16.DHZ337                    : F16_MBXHCCWP           , at = 262.7814     , slot_id = 5359522;
 F16.QFN345                    : F16_MQNEGTWP           , at = 265.1314     , slot_id = 5359523;
 F16.BPM346                    : F16_BPCL               , at = 266.4034     , slot_id = 5359524;
 F16.QDN350                    : F16_MQNDCTWP           , at = 274.3614     , slot_id = 5359525;
 F16.BTV352                    : F16_BTVRB              , at = 275.2015     , slot_id = 5359526;
 F16.DVT353                    : F16_MBXFACWP           , at = 277.0266     , slot_id = 5359527;
 F16.QFN365                    : F16_MQNEGTWP           , at = 283.5914     , slot_id = 5359528;
 F16.BPM368                    : F16_BPCL               , at = 291.4344     , slot_id = 5359529;
 F16.QDN370                    : F16_MQNDCTWP           , at = 294.0964     , slot_id = 5359530;
 F16.BCT372                    : F16_BCTF               , at = 301.8314     , slot_id = 14423475;
 F16.BTV374                    : F16_BTVRB              , at = 303.4129     , slot_id = 5359531;
 F16.QFN375                    : F16_MQNEGTWP           , at = 304.2954     , slot_id = 5359532;
 F16.QDN380                    : F16_MQNDCTWP           , at = 313.5255     , slot_id = 54873214;
 F16.QFN385                    : F16_MQNEGTWP           , at = 329.6355     , slot_id = 54873250;
 F16.BCT386                    : F16_TRA                , at = 330.46125    , slot_id = 54964790;
 F16.BTV387                    : F16_BTVRB              , at = 330.9305     , slot_id = 54872970;
 F16.TDU394                    : F16_TDE__004           , at = 335.8975     , slot_id = 34486506;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTH CONSTANTS                                         */
/************************************************************************************/

angle.F16.BHZ117               := 0.0150000;
angle.F16.BHZ147               := 0.0132000;
angle.F16.BHZ167               := -0.0085340;
angle.F16.BVT123               := 0.0176380;
angle.F16.BVT173               := -0.0176380;
tilt.F16.BVT123                := -PI/2;
tilt.F16.BVT173                := -PI/2;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/
  F16.BHZ117,                ANGLE := angle.F16.BHZ117;
  F16.BVT123,                ANGLE := angle.F16.BVT123, TILT := tilt.F16.BVT123;
  F16.BHZ147,                ANGLE := angle.F16.BHZ147;
  F16.BHZ167,                ANGLE := angle.F16.BHZ167;
  F16.BVT173,                ANGLE := angle.F16.BVT173, TILT := tilt.F16.BVT173;
return;