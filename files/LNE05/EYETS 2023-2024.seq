

  /**********************************************************************************
  *
  * LNE05 version (draft) EYETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 07-NOV-2024 03:07:44 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE05_BSGWAMEC001            := 0.3;
l.LNE05_VVGBF                  := 0.075;
l.LNE05_VVGBH                  := 0.085;
l.LNE05_ZCH                    := 0.037;
l.LNE05_ZCV                    := 0.037;
l.LNE05_ZDSB_001               := 0.503702;
l.LNE05_ZQ                     := 0.1;
l.LNE05_ZQFD                   := 0.1;
l.LNE05_ZQFF                   := 0.1;
l.LNE05_ZQMD                   := 0.1;
l.LNE05_ZQMF                   := 0.1;
l.LNE05_ZQNA                   := 0.39;

//---------------------- HCORRECTOR     ---------------------------------------------
LNE05_ZCH                : HCORRECTOR  , L := l.LNE05_ZCH;         ! Electrostatic Corrector, Horizontal
//---------------------- INSTRUMENT     ---------------------------------------------
LNE05_BSGWAMEC001        : INSTRUMENT  , L := l.LNE05_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
LNE05_VVGBF              : INSTRUMENT  , L := l.LNE05_VVGBF;       ! Vacuum valve gate all metal, electropneumatic controlled, DN63-CFF63 (VAT-S48)
LNE05_VVGBH              : INSTRUMENT  , L := l.LNE05_VVGBH;       ! Vacuum valve gate all metal, electropneumatic controlled, DN100-CFF100 (VAT-S48)
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE05_ZQ                 : QUADRUPOLE  , L := l.LNE05_ZQ;          ! Electrostatic Quadrupoles
LNE05_ZQFD               : QUADRUPOLE  , L := l.LNE05_ZQFD;        ! Electrostatic Quadrupoles, powered in series, Defocussing
LNE05_ZQFF               : QUADRUPOLE  , L := l.LNE05_ZQFF;        ! Electrostatic Quadrupoles, powered in series, Focussing
LNE05_ZQMD               : QUADRUPOLE  , L := l.LNE05_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE05_ZQMF               : QUADRUPOLE  , L := l.LNE05_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE05_ZQNA               : QUADRUPOLE  , Lrad := l.LNE05_ZQNA;        ! Electrostatic Quadrupole, Normal, type A
//---------------------- RBEND          ---------------------------------------------
LNE05_ZDSB_001           : RBEND       , L := l.LNE05_ZDSB_001;    ! Electrostatic Deflector, Slow pulsed, type B (ELENA), 48.1deg
//---------------------- VCORRECTOR     ---------------------------------------------
LNE05_ZCV                : VCORRECTOR  , L := l.LNE05_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE05 SEQUENCE                                              */
/************************************************************************************/

LNE05 : SEQUENCE, refer = centre,       L = 15.017624;
 LNE.ZQFD.0501                 : LNE05_ZQFD             , at = 1.410969     , slot_id = 52753784, assembly_id= 52753775;
 LNE.ZCV.0501                  : LNE05_ZCV              , at = 1.509469     , slot_id = 52753785, assembly_id= 52753775;
 LNE.ZQNA.0501                 : LNE05_ZQNA             , at = 1.533469     , slot_id = 52753775;
 LNE.ZCH.0501                  : LNE05_ZCH              , at = 1.556469     , slot_id = 52753786, assembly_id= 52753775;
 LNE.ZQ.0502                   : LNE05_ZQ               , at = 1.654969     , slot_id = 52753787, assembly_id= 52753775;
 LNE.BSGWA.0503                : LNE05_BSGWAMEC001      , at = 1.878469     , slot_id = 52753853;
 LNE.VV.0505                   : LNE05_VVGBF            , at = 2.150969     , slot_id = 52753877;
 LNE.ZQ.0508                   : LNE05_ZQ               , at = 2.716969     , slot_id = 52753901, assembly_id= 52753892;
 LNE.ZCV.0508                  : LNE05_ZCV              , at = 2.815469     , slot_id = 52753902, assembly_id= 52753892;
 LNE.ZQNA.0508                 : LNE05_ZQNA             , at = 2.839469     , slot_id = 52753892;
 LNE.ZCH.0508                  : LNE05_ZCH              , at = 2.862469     , slot_id = 52753903, assembly_id= 52753892;
 LNE.ZQFF.0509                 : LNE05_ZQFF             , at = 2.960969     , slot_id = 52753904, assembly_id= 52753892;
 LNE.BSGWA.0510                : LNE05_BSGWAMEC001      , at = 3.184469     , slot_id = 52754828;
 LNE.ZDSHR.0515                : LNE05_ZDSB_001         , at = 3.921296512  , slot_id = 52754852;
 LNE.BSGWA.0520                : LNE05_BSGWAMEC001      , at = 4.954624     , slot_id = 52754910;
 LNE.ZQMF.0521                 : LNE05_ZQMF             , at = 5.177124     , slot_id = 52754943, assembly_id= 52754934;
 LNE.ZCV.0521                  : LNE05_ZCV              , at = 5.275624     , slot_id = 52754944, assembly_id= 52754934;
 LNE.ZQNA.0521                 : LNE05_ZQNA             , at = 5.299624     , slot_id = 52754934;
 LNE.ZCH.0521                  : LNE05_ZCH              , at = 5.322624     , slot_id = 52754945, assembly_id= 52754934;
 LNE.ZQMD.0522                 : LNE05_ZQMD             , at = 5.421124     , slot_id = 52754946, assembly_id= 52754934;
 LNE.ZQMD.0527                 : LNE05_ZQMD             , at = 6.397124     , slot_id = 52755055, assembly_id= 52755046;
 LNE.ZCV.0527                  : LNE05_ZCV              , at = 6.495624     , slot_id = 52755056, assembly_id= 52755046;
 LNE.ZQNA.0527                 : LNE05_ZQNA             , at = 6.519624     , slot_id = 52755046;
 LNE.ZCH.0527                  : LNE05_ZCH              , at = 6.542624     , slot_id = 52755057, assembly_id= 52755046;
 LNE.ZQMF.0528                 : LNE05_ZQMF             , at = 6.641124     , slot_id = 52755058, assembly_id= 52755046;
 LNE.BSGWA.0533                : LNE05_BSGWAMEC001      , at = 7.743624     , slot_id = 52755118;
 LNE.ZQFD.0534                 : LNE05_ZQFD             , at = 7.966124     , slot_id = 52755151, assembly_id= 52755142;
 LNE.ZCV.0534                  : LNE05_ZCV              , at = 8.064624     , slot_id = 52755152, assembly_id= 52755142;
 LNE.ZQNA.0534                 : LNE05_ZQNA             , at = 8.088624     , slot_id = 52755142;
 LNE.ZCH.0534                  : LNE05_ZCH              , at = 8.111624     , slot_id = 52755153, assembly_id= 52755142;
 LNE.ZQ.0535                   : LNE05_ZQ               , at = 8.210124     , slot_id = 52755154, assembly_id= 52755142;
 LNE.ZQFF.0540                 : LNE05_ZQFF             , at = 9.516124     , slot_id = 52755226, assembly_id= 52755217;
 LNE.ZCV.0540                  : LNE05_ZCV              , at = 9.614624     , slot_id = 52755227, assembly_id= 52755217;
 LNE.ZQNA.0540                 : LNE05_ZQNA             , at = 9.638624     , slot_id = 52755217;
 LNE.ZCH.0540                  : LNE05_ZCH              , at = 9.661624     , slot_id = 52755228, assembly_id= 52755217;
 LNE.ZQ.0541                   : LNE05_ZQ               , at = 9.760124     , slot_id = 52755229, assembly_id= 52755217;
 LNE.BSGWA.0546                : LNE05_BSGWAMEC001      , at = 10.843624    , slot_id = 52755289;
 LNE.ZQFD.0547                 : LNE05_ZQFD             , at = 11.066124    , slot_id = 52755322, assembly_id= 52755313;
 LNE.ZCV.0547                  : LNE05_ZCV              , at = 11.164624    , slot_id = 52755323, assembly_id= 52755313;
 LNE.ZQNA.0547                 : LNE05_ZQNA             , at = 11.188624    , slot_id = 52755313;
 LNE.ZCH.0547                  : LNE05_ZCH              , at = 11.211624    , slot_id = 52755324, assembly_id= 52755313;
 LNE.ZQ.0548                   : LNE05_ZQ               , at = 11.310124    , slot_id = 52755325, assembly_id= 52755313;
 LNE.ZQMD.0553                 : LNE05_ZQMD             , at = 12.366124    , slot_id = 52755394, assembly_id= 52755385;
 LNE.ZCV.0553                  : LNE05_ZCV              , at = 12.464624    , slot_id = 52755395, assembly_id= 52755385;
 LNE.ZQNA.0553                 : LNE05_ZQNA             , at = 12.488624    , slot_id = 52755385;
 LNE.ZCH.0553                  : LNE05_ZCH              , at = 12.511624    , slot_id = 52755396, assembly_id= 52755385;
 LNE.ZQMF.0554                 : LNE05_ZQMF             , at = 12.610124    , slot_id = 52755397, assembly_id= 52755385;
 LNE.ZQMD.0559                 : LNE05_ZQMD             , at = 13.305124    , slot_id = 52755469, assembly_id= 52755460;
 LNE.ZCV.0559                  : LNE05_ZCV              , at = 13.403624    , slot_id = 52755470, assembly_id= 52755460;
 LNE.ZQNA.0559                 : LNE05_ZQNA             , at = 13.427624    , slot_id = 52755460;
 LNE.ZCH.0559                  : LNE05_ZCH              , at = 13.450624    , slot_id = 52755471, assembly_id= 52755460;
 LNE.ZQMF.0560                 : LNE05_ZQMF             , at = 13.549124    , slot_id = 52755472, assembly_id= 52755460;
 LNE.BSGWA.0561                : LNE05_BSGWAMEC001      , at = 13.772624    , slot_id = 52755536;
 LNE.VV.0565                   : LNE05_VVGBH            , at = 14.050124    , slot_id = 52755560;
ENDSEQUENCE;

return;