

  /**********************************************************************************
  *
  * TT43 version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 05-AUG-2023 03:08:41 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TT43_ACT3A                   := 0;
l.TT43_BEPPA                   := 0;
l.TT43_BPCWA                   := 0;
l.TT43_BTVWA                   := 0;
l.TT43_MBAWAHWC                := 0.1775;
l.TT43_MCAWAWAC                := 0.0406;
l.TT43_MQAWANAP                := 0.0708;
l.TT43_TBSLA001                := 0.15;

//---------------------- COLLIMATOR     ---------------------------------------------
TT43_TBSLA001  : COLLIMATOR  , L := l.TT43_TBSLA001;     ! Beam Stopper, Low energy, type A
//---------------------- CORRECTOR      ---------------------------------------------
TT43_MCAWAWAC  : CORRECTOR   , L := l.TT43_MCAWAWAC;     ! Corrector - AWAKE electron line
//---------------------- HKICKER        ---------------------------------------------
TT43_MQAWANAP  : HKICKER     , L := l.TT43_MQAWANAP;     ! Quadrupole - AWAKE electron line
//---------------------- INSTRUMENT     ---------------------------------------------
TT43_BTVWA     : INSTRUMENT  , L := l.TT43_BTVWA;        ! Light screen monitor - 17° screen axis inclination - type A (based on CTF design)
//---------------------- MONITOR        ---------------------------------------------
TT43_BEPPA     : MONITOR     , L := l.TT43_BEPPA;        ! Beam Emittance meter based on Pepper-Pot for AWAKE
TT43_BPCWA     : MONITOR     , L := l.TT43_BPCWA;        ! Beam position coupler - AWAKE electron type A
//---------------------- RBEND          ---------------------------------------------
TT43_MBAWAHWC  : RBEND       , L := l.TT43_MBAWAHWC;     ! Bending magnet - AWAKE electron line
//---------------------- RFCAVITY       ---------------------------------------------
TT43_ACT3A     : RFCAVITY    , L := l.TT43_ACT3A;        ! Accelerating cavity, travelling wave 3GHz, type A


/************************************************************************************/
/*                      TT43 SEQUENCE                                               */
/************************************************************************************/

TT43 : SEQUENCE, refer = centre,        L = 24.016227;
 MCAWA.430007                  : TT43_MCAWAWAC   , at = .7215        , slot_id = 57431030;
 BPM.430010                    : TT43_BPCWA      , at = .9989        , slot_id = 42401366;
 BEPPA.430013                  : TT43_BEPPA      , at = 1.4132       , slot_id = 42750287;
 MCAWA.430016                  : TT43_MCAWAWAC   , at = 1.6397       , slot_id = 57431317;
 ACT3A.430017                  : TT43_ACT3A      , at = 2.20705      , slot_id = 42750296;
 BPM.430028                    : TT43_BPCWA      , at = 2.87357      , slot_id = 42401367;
 MCAWA.430029                  : TT43_MCAWAWAC   , at = 2.96942      , slot_id = 42495595;
 MQAWD.430031                  : TT43_MQAWANAP   , at = 3.177        , slot_id = 42401383;
 MQAWF.430034                  : TT43_MQAWANAP   , at = 3.4795       , slot_id = 42401384;
 MQAWD.430037                  : TT43_MQAWANAP   , at = 3.782        , slot_id = 42401387;
 BPM.430039                    : TT43_BPCWA      , at = 3.9295       , slot_id = 42401388;
 MCAWA.430040                  : TT43_MCAWAWAC   , at = 4.0247       , slot_id = 42495599;
 BTV.430042                    : TT43_BTVWA      , at = 4.3218       , slot_id = 42401402;
 MBAWV.430100                  : TT43_MBAWAHWC   , at = 4.864        , slot_id = 42478032;
 BPM.430103                    : TT43_BPCWA      , at = 5.1367       , slot_id = 42495242;
 MCAWA.430104                  : TT43_MCAWAWAC   , at = 5.2399       , slot_id = 42495600;
 BTV.430106                    : TT43_BTVWA      , at = 5.468        , slot_id = 42495243;
 MQAWD.430109                  : TT43_MQAWANAP   , at = 5.7381       , slot_id = 42495244;
 TBSLA.430112                  : TT43_TBSLA001   , at = 5.9795       , slot_id = 42693219;
 MQAWF.430118                  : TT43_MQAWANAP   , at = 6.6533       , slot_id = 42495249;
 MQAWD.430128                  : TT43_MQAWANAP   , at = 7.5685       , slot_id = 42495255;
 BPM.430129                    : TT43_BPCWA      , at = 7.7074       , slot_id = 42495256;
 MCAWA.430130                  : TT43_MCAWAWAC   , at = 7.8026       , slot_id = 42495601;
 MBAWV.430200                  : TT43_MBAWAHWC   , at = 8.5249       , slot_id = 42495257;
 BPM.430203                    : TT43_BPCWA      , at = 8.7219       , slot_id = 42495258;
 MCAWA.430204                  : TT43_MCAWAWAC   , at = 8.8171       , slot_id = 42496263;
 MQAWF.430205                  : TT43_MQAWANAP   , at = 9.0138       , slot_id = 42495259;
 MBAWH.430300                  : TT43_MBAWAHWC   , at = 9.4699       , slot_id = 42495263;
 BPM.430308                    : TT43_BPCWA      , at = 10.2166      , slot_id = 42495264;
 MCAWA.430309                  : TT43_MCAWAWAC   , at = 10.3118      , slot_id = 42495593;
 MQAWF.430311                  : TT43_MQAWANAP   , at = 10.5085      , slot_id = 42495295;
ENDSEQUENCE;

return;