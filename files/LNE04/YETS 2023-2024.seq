

  /**********************************************************************************
  *
  * LNE04 version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 05-AUG-2023 02:45:41 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE04_BSGWAMEC001            := 0.3;
l.LNE04_TBSLA001               := 0.15;
l.LNE04_VVGBH                  := 0.085;
l.LNE04_ZCH                    := 0.037;
l.LNE04_ZCV                    := 0.037;
l.LNE04_ZQ                     := 0.1;
l.LNE04_ZQFD                   := 0.1;
l.LNE04_ZQFF                   := 0.1;
l.LNE04_ZQMD                   := 0.1;
l.LNE04_ZQMF                   := 0.1;
l.LNE04_ZQNA                   := 0.39;

//---------------------- COLLIMATOR     ---------------------------------------------
LNE04_TBSLA001 : COLLIMATOR  , L := l.LNE04_TBSLA001;    ! Beam Stopper, Low energy, type A
//---------------------- HCORRECTOR     ---------------------------------------------
LNE04_ZCH      : HCORRECTOR  , L := l.LNE04_ZCH;         ! Electrostatic Corrector, Horizontal
//---------------------- INSTRUMENT     ---------------------------------------------
LNE04_BSGWAMEC0: INSTRUMENT  , L := l.LNE04_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
LNE04_VVGBH    : INSTRUMENT  , L := l.LNE04_VVGBH;       ! Vacuum valve gate all metal, electropneumatic controlled, DN100-CFF100 (VAT-S48)
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE04_ZQ       : QUADRUPOLE  , L := l.LNE04_ZQ;          ! Electrostatic Quadrupoles
LNE04_ZQFD     : QUADRUPOLE  , L := l.LNE04_ZQFD;        ! Electrostatic Quadrupoles, powered in series, Defocussing
LNE04_ZQFF     : QUADRUPOLE  , L := l.LNE04_ZQFF;        ! Electrostatic Quadrupoles, powered in series, Focussing
LNE04_ZQMD     : QUADRUPOLE  , L := l.LNE04_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE04_ZQMF     : QUADRUPOLE  , L := l.LNE04_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE04_ZQNA     : QUADRUPOLE  , Lrad := l.LNE04_ZQNA;        ! Electrostatic Quadrupole, Normal, type A (ELENA)
//---------------------- VCORRECTOR     ---------------------------------------------
LNE04_ZCV      : VCORRECTOR  , L := l.LNE04_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE04 SEQUENCE                                              */
/************************************************************************************/

LNE04 : SEQUENCE, refer = centre,       L = 10.204708;
 LNE.ZQFF.0401                 : LNE04_ZQFF      , at = 1.441263     , slot_id = 52749896, assembly_id= 52749887;
 LNE.ZCV.0401                  : LNE04_ZCV       , at = 1.539763     , slot_id = 52749897, assembly_id= 52749887;
 LNE.ZQNA.0401                 : LNE04_ZQNA      , at = 1.563763     , slot_id = 52749887;
 LNE.ZCH.0401                  : LNE04_ZCH       , at = 1.586763     , slot_id = 52749898, assembly_id= 52749887;
 LNE.ZQ.0402                   : LNE04_ZQ        , at = 1.685263     , slot_id = 52749899, assembly_id= 52749887;
 LNE.BSGWA.0403                : LNE04_BSGWAMEC00, at = 1.908763     , slot_id = 52749942;
 LNE.TBS.0405                  : LNE04_TBSLA001  , at = 2.218763     , slot_id = 52751177;
 LNE.ZQFD.0408                 : LNE04_ZQFD      , at = 2.991263     , slot_id = 52751579, assembly_id= 52751570;
 LNE.ZCV.0408                  : LNE04_ZCV       , at = 3.089763     , slot_id = 52751580, assembly_id= 52751570;
 LNE.ZQNA.0408                 : LNE04_ZQNA      , at = 3.113763     , slot_id = 52751570;
 LNE.ZCH.0408                  : LNE04_ZCH       , at = 3.136763     , slot_id = 52751581, assembly_id= 52751570;
 LNE.ZQ.0409                   : LNE04_ZQ        , at = 3.235263     , slot_id = 52751582, assembly_id= 52751570;
 LNE.BSGWA.0410                : LNE04_BSGWAMEC00, at = 3.458763     , slot_id = 52751753;
 LNE.ZCV.0420                  : LNE04_ZCV       , at = 5.942208     , slot_id = 52751961, assembly_id= 52751951;
 LNE.ZQNA.0420                 : LNE04_ZQNA      , at = 5.966208     , slot_id = 52751951;
 LNE.ZCH.0420                  : LNE04_ZCH       , at = 5.989208     , slot_id = 52751962, assembly_id= 52751951;
 LNE.ZQMD.0421                 : LNE04_ZQMD      , at = 6.087708     , slot_id = 52751963, assembly_id= 52751951;
 LNE.BSGWA.0422                : LNE04_BSGWAMEC00, at = 6.311208     , slot_id = 52752245;
 LNE.ZQMF.0427                 : LNE04_ZQMF      , at = 7.343708     , slot_id = 52752316, assembly_id= 52752307;
 LNE.ZCV.0427                  : LNE04_ZCV       , at = 7.442208     , slot_id = 52752317, assembly_id= 52752307;
 LNE.ZQNA.0427                 : LNE04_ZQNA      , at = 7.466208     , slot_id = 52752307;
 LNE.ZCH.0427                  : LNE04_ZCH       , at = 7.489208     , slot_id = 52752318, assembly_id= 52752307;
 LNE.ZQMD.0428                 : LNE04_ZQMD      , at = 7.587708     , slot_id = 52752319, assembly_id= 52752307;
 LNE.BSGWA.0429                : LNE04_BSGWAMEC00, at = 7.811208     , slot_id = 52752521;
 LNE.VV.0434                   : LNE04_VVGBH     , at = 8.494708     , slot_id = 52753242;
ENDSEQUENCE;

return;