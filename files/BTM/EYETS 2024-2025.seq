

  /**********************************************************************************
  *
  * BTM version (draft) EYETS 2024-2025 in MAD X SEQUENCE format
  * Generated the 13-MAR-2024 14:12:10 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.BTM_BCT__006                 := 0;
l.BTM_BPUIA003                 := 0;
l.BTM_BTVBM002                 := 0;
l.BTM_BTVBP007                 := 0;
l.BTM_MBHGAWWP                 := 2.000457;
l.BTM_MBXFBCWP                 := 1;
l.BTM_MBXGEHWP                 := 2.324;
l.BTM_MCXAE001                 := 0.3;
l.BTM_MQNCDNWP                 := 0.56;
l.BTM_OMK                      := 0;

//---------------------- INSTRUMENT     ---------------------------------------------
BTM_BTVBM002             : INSTRUMENT  , L := l.BTM_BTVBM002;      ! Beam TV Booster Type, Motorised, variant 002
BTM_BTVBP007             : INSTRUMENT  , L := l.BTM_BTVBP007;      ! Beam TV Booster Type, Pneumatic, variant 007
//---------------------- MARKER         ---------------------------------------------
BTM_OMK                  : MARKER      , L := l.BTM_OMK;           ! BTM markers
//---------------------- MONITOR        ---------------------------------------------
BTM_BCT__006             : MONITOR     , L := l.BTM_BCT__006;      ! Beam current transformer
BTM_BPUIA003             : MONITOR     , L := l.BTM_BPUIA003;      ! Beam Position Pick-up Inductive type A
//---------------------- QUADRUPOLE     ---------------------------------------------
BTM_MQNCDNWP             : QUADRUPOLE  , L := l.BTM_MQNCDNWP;      ! Quadrupole magnet, type Q130, 0.5m
//---------------------- RBEND          ---------------------------------------------
BTM_MCXAE001             : RBEND       , L := l.BTM_MCXAE001;      ! Corrector magnet, H or V, type 4af, with altered magnetic length for line BTM.
//---------------------- SBEND          ---------------------------------------------
BTM_MBHGAWWP             : SBEND       , L := l.BTM_MBHGAWWP;      ! Bending magnet, booster TL switching magnet
BTM_MBXFBCWP             : SBEND       , L := l.BTM_MBXFBCWP;      ! Bending Magnet, H or V, type HB4, 1m gap 80mm
BTM_MBXGEHWP             : SBEND       , L := l.BTM_MBXGEHWP;      ! Bending Magnet, H or V, type BTM


/************************************************************************************/
/*                       BTM SEQUENCE                                                */
/************************************************************************************/

BTM : SEQUENCE, refer = centre,         L = 25.124821;
 BT.BHZ10                      : BTM_MBHGAWWP           , at = 1.0994955    , slot_id = 5554781;
 BTM.VVS10                     : BTM_OMK                , at = 2.34097      , slot_id = 6023707;
 BTM.BTV10                     : BTM_BTVBP007           , at = 3.31417      , slot_id = 6029651;
 BTM.QNO05                     : BTM_MQNCDNWP           , at = 3.85367      , slot_id = 6023669;
 BTM.BHZ10                     : BTM_MBXGEHWP           , at = 5.732201     , slot_id = 6023702;
 BTM.QNO10                     : BTM_MQNCDNWP           , at = 7.535221     , slot_id = 6023703;
 BTM.DVT10                     : BTM_MCXAE001           , at = 8.270721     , slot_id = 6029649;
 BTM.DHZ10                     : BTM_MCXAE001           , at = 8.734721     , slot_id = 6029650;
 BTM.QNO20                     : BTM_MQNCDNWP           , at = 9.418221     , slot_id = 6023704;
 BTM.BPM00                     : BTM_BPUIA003           , at = 10.032621    , slot_id = 6029655;
 BTM.BTV15                     : BTM_BTVBM002           , at = 10.399621    , slot_id = 6029652;
 BTY.BVT101                    : BTM_MBXFBCWP           , at = 11.719221    , slot_id = 6154779;
 BTM.BSGH01                    : BTM_OMK                , at = 13.748721    , slot_id = 6029743;
 BTM.BSGV01                    : BTM_OMK                , at = 13.748721    , slot_id = 10409825;
 BTM.BSGH02                    : BTM_OMK                , at = 16.248721    , slot_id = 6029744;
 BTM.BSGV02                    : BTM_OMK                , at = 16.248721    , slot_id = 10409826;
 BTM.BPM10                     : BTM_BPUIA003           , at = 17.040221    , slot_id = 6029656;
 BTM.BTV20                     : BTM_BTVBP007           , at = 18.205221    , slot_id = 6029653;
 BTM.BSGH03                    : BTM_OMK                , at = 18.748721    , slot_id = 6029745;
 BTM.BSGV03                    : BTM_OMK                , at = 18.748721    , slot_id = 10409827;
 BTM.BCT10                     : BTM_BCT__006           , at = 19.096221    , slot_id = 6029654;
 BTM.TDU10.ENTRYFACE           : BTM_OMK                , at = 24.374821    , slot_id = 6029747;
 BTM.TDU10                     : BTM_OMK                , at = 25.124821    , slot_id = 6029747;
ENDSEQUENCE;

return;