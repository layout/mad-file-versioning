

  /**********************************************************************************
  *
  * LNE07 version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 15-AUG-2023 02:48:36 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE07_BSGWAMEC001            := 0.3;
l.LNE07_ZCH                    := 0.037;
l.LNE07_ZCV                    := 0.037;
l.LNE07_ZDFHR                  := 0;
l.LNE07_ZDSHR005               := 0;
l.LNE07_ZQ                     := 0.1;
l.LNE07_ZQFD                   := 0.1;
l.LNE07_ZQMD                   := 0.1;
l.LNE07_ZQMF                   := 0.1;
l.LNE07_ZQNA                   := 0.39;

//---------------------- HCORRECTOR     ---------------------------------------------
LNE07_ZCH      : HCORRECTOR  , L := l.LNE07_ZCH;         ! Electrostatic Corrector, Horizontal
LNE07_ZDFHR    : HCORRECTOR  , L := l.LNE07_ZDFHR;       ! Electrostatic Deflector, Fast pulsed, Horizontal, Right
//---------------------- HKICKER        ---------------------------------------------
LNE07_ZDSHR005 : HKICKER     , L := l.LNE07_ZDSHR005;    ! Electrostatic Deflector, Slow pulsed, Horizontal, Right, 48.1 deg
//---------------------- INSTRUMENT     ---------------------------------------------
LNE07_BSGWAMEC0: INSTRUMENT  , L := l.LNE07_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE07_ZQ       : QUADRUPOLE  , L := l.LNE07_ZQ;          ! Electrostatic Quadrupoles
LNE07_ZQFD     : QUADRUPOLE  , L := l.LNE07_ZQFD;        ! Electrostatic Quadrupoles, powered in series, Defocussing
LNE07_ZQMD     : QUADRUPOLE  , L := l.LNE07_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE07_ZQMF     : QUADRUPOLE  , L := l.LNE07_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE07_ZQNA     : QUADRUPOLE  , Lrad := l.LNE07_ZQNA;        ! Electrostatic Quadrupole, Normal, type A (ELENA)
//---------------------- VCORRECTOR     ---------------------------------------------
LNE07_ZCV      : VCORRECTOR  , L := l.LNE07_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE07 SEQUENCE                                              */
/************************************************************************************/

LNE07 : SEQUENCE, refer = centre,       L = 9.6615;
 LNE.ZDFHR.0701                : LNE07_ZDFHR     , at = .3           , slot_id = 52781768;
 LNE.ZQMF.0711                 : LNE07_ZQMF      , at = 1.459296     , slot_id = 52781846, assembly_id= 52781831;
 LNE.ZCV.0711                  : LNE07_ZCV       , at = 1.557796     , slot_id = 52781847, assembly_id= 52781831;
 LNE.ZQNA.0711                 : LNE07_ZQNA      , at = 1.581796     , slot_id = 52781831;
 LNE.ZCH.0711                  : LNE07_ZCH       , at = 1.604796     , slot_id = 52781848, assembly_id= 52781831;
 LNE.ZQMD.0712                 : LNE07_ZQMD      , at = 1.703296     , slot_id = 52781849, assembly_id= 52781831;
 LNE.ZDSHR.0717                : LNE07_ZDSHR005  , at = 2.245072     , slot_id = 52781918;
 LNE.BSGWA.0722                : LNE07_BSGWAMEC00, at = 2.9364       , slot_id = 52781933;
 LNE.ZQMF.0723                 : LNE07_ZQMF      , at = 3.1589       , slot_id = 52781972, assembly_id= 52781957;
 LNE.ZCV.0723                  : LNE07_ZCV       , at = 3.2574       , slot_id = 52781973, assembly_id= 52781957;
 LNE.ZQNA.0723                 : LNE07_ZQNA      , at = 3.2814       , slot_id = 52781957;
 LNE.ZCH.0723                  : LNE07_ZCH       , at = 3.3044       , slot_id = 52781974, assembly_id= 52781957;
 LNE.ZQMD.0724                 : LNE07_ZQMD      , at = 3.4029       , slot_id = 52781975, assembly_id= 52781957;
 LNE.ZQFD.0729                 : LNE07_ZQFD      , at = 4.3089       , slot_id = 52782047, assembly_id= 52782032;
 LNE.ZCV.0729                  : LNE07_ZCV       , at = 4.4074       , slot_id = 52782048, assembly_id= 52782032;
 LNE.ZQNA.0729                 : LNE07_ZQNA      , at = 4.4314       , slot_id = 52782032;
 LNE.ZCH.0729                  : LNE07_ZCH       , at = 4.4544       , slot_id = 52782049, assembly_id= 52782032;
 LNE.ZQ.0730                   : LNE07_ZQ        , at = 4.5529       , slot_id = 52782050, assembly_id= 52782032;
 LNE.ZCV.0735                  : LNE07_ZCV       , at = 5.450829     , slot_id = 52782123, assembly_id= 52782107;
 LNE.ZQNA.0735                 : LNE07_ZQNA      , at = 5.474829     , slot_id = 52782107;
 LNE.ZCH.0735                  : LNE07_ZCH       , at = 5.497829     , slot_id = 52782124, assembly_id= 52782107;
 LNE.BSGWA.0737                : LNE07_BSGWAMEC00, at = 5.819829     , slot_id = 52782182;
 LNE.ZQMF.0742                 : LNE07_ZQMF      , at = 6.719258     , slot_id = 52782221, assembly_id= 52782206;
 LNE.ZCV.0742                  : LNE07_ZCV       , at = 6.817758     , slot_id = 52782222, assembly_id= 52782206;
 LNE.ZQNA.0742                 : LNE07_ZQNA      , at = 6.841758     , slot_id = 52782206;
 LNE.ZCH.0742                  : LNE07_ZCH       , at = 6.864758     , slot_id = 52782223, assembly_id= 52782206;
 LNE.ZQMD.0743                 : LNE07_ZQMD      , at = 6.963258     , slot_id = 52782224, assembly_id= 52782206;
 LNE.BSGWA.0744                : LNE07_BSGWAMEC00, at = 7.186758     , slot_id = 52782281;
ENDSEQUENCE;

return;