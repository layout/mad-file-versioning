

  /**********************************************************************************
  *
  * LT version (draft) YETS 2025-2026 in MAD X SEQUENCE format
  * Generated the 17-JAN-2025 03:24:41 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LT_BCTFT001                  := 0;
l.LT_BPUSE011                  := 0;
l.LT_DHZ__09B                  := 0;
l.LT_DVT__09B                  := 0;
l.LT_MBHEJCWP                  := 1.05273805;
l.LT_MQNAIFAP                  := 0.255;
l.LT_MQNBSNAP                  := 0.452;
l.LT_OMK                       := 0;

//---------------------- HKICKER        ---------------------------------------------
LT_DHZ__09B              : HKICKER     , L := l.LT_DHZ__09B;       ! DHZ Horizontal Dipole Corrector Magnet type 9 b
//---------------------- MARKER         ---------------------------------------------
LT_OMK                   : MARKER      , L := l.LT_OMK;            ! LT markers
//---------------------- MONITOR        ---------------------------------------------
LT_BCTFT001              : MONITOR     , L := l.LT_BCTFT001;       ! Fast Beam Current Transformer, type T, variant 001
LT_BPUSE011              : MONITOR     , L := l.LT_BPUSE011;       ! Beam Position Monitor, Stripline, variant E.
//---------------------- QUADRUPOLE     ---------------------------------------------
LT_MQNAIFAP              : QUADRUPOLE  , L := l.LT_MQNAIFAP;       ! Quadrupole magnet, type Q11 or linac VII
LT_MQNBSNAP              : QUADRUPOLE  , L := l.LT_MQNBSNAP;       ! Quadrupole Magnet, Booster injection
//---------------------- SBEND          ---------------------------------------------
LT_MBHEJCWP              : SBEND       , L := l.LT_MBHEJCWP;       ! Bending magnet, type BHZ version 2 (high flow), 0.9m, Linac2
//---------------------- VKICKER        ---------------------------------------------
LT_DVT__09B              : VKICKER     , L := l.LT_DVT__09B;       ! DVT Vertical Dipole Corrector Magnet type 9 b


/************************************************************************************/
/*                       LT SEQUENCE                                                */
/************************************************************************************/

LT : SEQUENCE, refer = centre,          L = 29.928517649;
 LT.BEGLT                      : LT_OMK                 , at = 0            , slot_id = 3501080;
 LT.BHZ20                      : LT_MBHEJCWP            , at = .756369      , slot_id = 55604286;
 LT.BEC3                       : LT_OMK                 , at = 6.108474     , slot_id = 5332542;
 LT.QFO50                      : LT_MQNAIFAP            , at = 7.378474     , slot_id = 52930189;
 LT.VPI14                      : LT_OMK                 , at = 8.328474     , slot_id = 3777280;
 LT.BEC4                       : LT_OMK                 , at = 8.748474     , slot_id = 5332543;
 LT.BCT30                      : LT_BCTFT001            , at = 9.075974     , slot_id = 3501160;
 LT.DHZ40                      : LT_DHZ__09B            , at = 9.628474     , slot_id = 3501161, assembly_id= 3501106;
 LT.DVT40                      : LT_DVT__09B            , at = 9.628474     , slot_id = 3501162, assembly_id= 3501106;
 LT.BPM40                      : LT_BPUSE011            , at = 10.788474    , slot_id = 3501165;
 LT.QDE55                      : LT_MQNAIFAP            , at = 11.578474    , slot_id = 52937374;
 LT.VVS20                      : LT_OMK                 , at = 11.940474    , slot_id = 3501167;
 LT.QFO60                      : LT_MQNAIFAP            , at = 16.448474    , slot_id = 52937390;
 LT.QDE65                      : LT_MQNAIFAP            , at = 18.356474    , slot_id = 52937407;
 LT.VPI25                      : LT_OMK                 , at = 20.253474    , slot_id = 3777281;
 LT.EBCT20                     : LT_OMK                 , at = 22.600757    , slot_id = 3501080;
 LT.VPI26                      : LT_OMK                 , at = 22.849474    , slot_id = 3777282;
 LT.QDE70                      : LT_MQNBSNAP            , at = 23.488474    , slot_id = 52937424;
 LT.SBHZ20                     : LT_OMK                 , at = 24.460803    , slot_id = 3501080;
 LT.QFO75                      : LT_MQNAIFAP            , at = 26.878474    , slot_id = 52937440;
 LT.DHZ50                      : LT_DHZ__09B            , at = 27.848474    , slot_id = 3501173, assembly_id= 3501107;
 LT.DVT50                      : LT_DVT__09B            , at = 27.848474    , slot_id = 3501174, assembly_id= 3501107;
 LT.BCT40                      : LT_BCTFT001            , at = 28.804824    , slot_id = 3501175;
 LT.BPM50                      : LT_BPUSE011            , at = 29.252074    , slot_id = 3501176;
 LT.ENDLT                      : LT_OMK                 , at = 29.928518    , slot_id = 3501080;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTH CONSTANTS                                         */
/************************************************************************************/

angle.LT.BHZ20                 := -0.420475953;
e1.LT.BHZ20                    := -0.2102379765;
e2.LT.BHZ20                    := -0.2102379765;
fint.LT.BHZ20                  := 0.7;
hgap.LT.BHZ20                  := 0.052;
l.LT.BHZ20                     := 1.05273805058656;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/
  LT.BHZ20,                  L := l.LT.BHZ20, ANGLE := angle.LT.BHZ20, E1 := e1.LT.BHZ20, E2 := e2.LT.BHZ20, FINT := fint.LT.BHZ20, HGAP := hgap.LT.BHZ20;
return;