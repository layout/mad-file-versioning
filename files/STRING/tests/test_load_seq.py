#!/usr/bin/python

import sys
from pathlib import Path
import os

from cpymad.madx import Madx
import numpy as np


def trycall(mad, filename):
    try:
        mad.call(str(filename))
    except RuntimeError as er:
        print(f"MAD-X error: Calling `{filename}` generated error")
        sys.exit(0)


def tryinput(mad, cmds):
    for cmd in cmds.split(";"):
        try:
            mad.input(cmd)
        except RuntimeError as er:
            print(f"MAD-X error: Executing `{cmd}` generated error")
            sys.exit(0)


def make_survey(mad, beam="b1", file="survey.tfs", x0=0, y0=0, z0=0, theta0=0):
    sequence = "string" + beam
    bv = 1 if beam == "b1" else -1
    mad.beam(sequence=sequence, bv=bv)
    mad.use(sequence)
    cols = [
        "name",
        "s",
        "l",
        "angle",
        "x",
        "y",
        "z",
        "theta",
        "phi",
        "psi",
        "globaltilt",
        "tilt",
        "mech_sep",
        "assembly_id",
        "slot_id",
    ]

    mad.select(flag="survey", column=cols)
    mad.survey(
        sequence=sequence,
        file=str(file),
        x0=x0,
        z0=z0,
        y0=y0,
        theta0=theta0,
        phi0=0,
        psi0=0,
    )


def copy_table(tt, cols):
    out = {}
    for cc in cols:
        out[cc] = tt[cc]
    return out


# Beam 1
y0 = 2449.119672
x0b1 = -1668.47911628903
z0b1 = 2658.57580912703  # oldstart
x0b2 = -1668.5135226995
z0b2 = 2658.57399389065  # oldstart
theta0b1 = 3.0903853088802173
theta0b2 = 3.0873804010562442
ds = 13329.4416 - 89
cxb1 = np.cos(theta0b1)
sxb1 = np.sin(theta0b1)
cxb2 = np.cos(theta0b2)
sxb2 = np.sin(theta0b2)


if __name__ == "__main__":
    # validation
    seqpath = Path(sys.argv[1])
    mad = Madx(stdout=False)
    trycall(mad, seqpath)
    mad.input("MBXF.4SF,angle=+0.00150235590250025;")
    mad.beam(sequence="stringb1", particle="proton", bv=1)
    mad.beam(sequence="stringb2", particle="proton", bv=-1)
    sudir = seqpath.parent / f"SURVEY_{seqpath.stem}"
    mad.input('set, format="19.9f";')
    # make_survey(mad,'b1',sudir/"survey_stringb1.tfs",
    #        x0=-1668.47911628903,y0=2449.119672,z0=2658.57580912703,theta0=3.0903853088802173)
    make_survey(
        mad,
        "b1",
        sudir / "survey_stringb1.tfs",
        x0=x0b1 + sxb1 * (-ds),
        y0=y0,
        z0=z0b1 + cxb1 * (-ds),
        theta0=theta0b1,
    )
    make_survey(
        mad,
        "b2",
        sudir / "survey_stringb2.tfs",
        x0=x0b2 + sxb2 * (-ds),
        y0=y0,
        z0=z0b2 + cxb2 * (-ds),
        theta0=theta0b2,
    )
