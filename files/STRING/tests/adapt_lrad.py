from pyoptics import optics
import numpy as np


ss = optics.open("survey_stringb1.tfs")
tt = optics.open("twiss_stringb1.tfs")


def copy_line(src, dst, ii, jj):
    for cn in dst.col_names:
        dst[cn.lower()][jj] = src[cn.lower()][ii]


lines = len(np.where(ss // "^[^D]")[0])

ssnew = ss.resize(lines * 2)
ssnew._data["name"] = np.zeros(lines * 2, dtype="<U20")

jj = 0
for ii in np.where(ss // "^[^D]")[0]:
    if tt.l[ii] > 0:
        copy_line(ss, ssnew, ii - 1, jj)
        copy_line(ss, ssnew, ii, jj + 1)
        ssnew.l[jj] = 0
    elif tt.lrad[ii] > 0:
        copy_line(ss, ssnew, ii, jj)
        copy_line(ss, ssnew, ii, jj + 1)
        pos_m = ss.get_pos(ii)
        rot_m = ss.get_rotmat(ii)
        lrad = tt.lrad[ii]
        pos_e = pos_m + rot_m @ [0, 0, -lrad / 2]
        pos_s = pos_m + rot_m @ [0, 0, +lrad / 2]
        ssnew.x[jj] = pos_e[0]
        ssnew.y[jj] = pos_e[1]
        ssnew.z[jj] = pos_e[2]
        ssnew.s[jj] -= lrad / 2
        ssnew.x[jj + 1] = pos_s[0]
        ssnew.y[jj + 1] = pos_s[1]
        ssnew.z[jj + 1] = pos_s[2]
        ssnew.l[jj + 1] = lrad
        ssnew.s[jj + 1] += lrad / 2
    else:
        copy_line(ss, ssnew, ii, jj)
        copy_line(ss, ssnew, ii, jj + 1)
    ssnew.name[jj] = ss.name[ii] + ".E"
    ssnew.name[jj + 1] = ss.name[ii] + ".S"
    jj += 2

ssnew.save("survey_new.tfs")
