
  /**********************************************************************************
  *
  * STRING Facility (draft) version EYETS 2024-2025 in MAD X sequence format
  * Generated the 14-MAR-2024 03:29:30 from LAYOUT
  *
  ***********************************************************************************/


/************************************************************************************/
/*                       GEOMETRY                                                   */
/************************************************************************************/

if (version>=50401){option,-rbarc;};

LHCLENGTH           := 26658.8832;
sep_ARC             := 0.194;
sep_IR3             := 0.224;
sep_IR4             := 0.420;
sep_IR7             := 0.224;
IP1OFS.B1           := 0;
IP1OFS.B2           := 0;
IP2OFS.B1           := 154;
IP2OFS.B2           := -154;
IP3OFS.B1           := 0;
IP3OFS.B2           := 0;
IP4OFS.B1           := -154;
IP4OFS.B2           := 154;
IP5OFS.B1           := -308;
IP5OFS.B2           := 308;
IP6OFS.B1           := -154;
IP6OFS.B2           := 154;
IP7OFS.B1           := 0;
IP7OFS.B2           := 0;
IP8OFS.B1           := 154;
IP8OFS.B2           := -154;
Dsep1               := 85.913;
Dsep2               := 63.295;
Dsep3               := 26.493;
Dsep4               := 71.901;
Dsep5               := 85.913;
Dsep7               := 39.7395;
Dsep8               := 63.295;
AIP1                := ATAN(sep_ARC/2/Dsep1);
AIP2                := ATAN(sep_ARC/2/Dsep2);
AIP3                := ATAN((sep_IR3-sep_ARC)/2/Dsep3);
AIP4                := ATAN((sep_IR4-sep_ARC)/2/Dsep4);
AIP5                := ATAN(sep_ARC/2/Dsep5);
AIP7                := ATAN((sep_IR7-sep_ARC)/2/Dsep7);
AIP8                := ATAN((sep_ARC)/2/Dsep8);
pIP1                := 0;
pIP1.L1             := 26658.8832;
pIP2                := 3332.3604;
pIP3                := 6664.7208;
pIP4                := 9997.0812;
pIP5                := 13329.4416;
pIP6                := 16661.802;
pIP7                := 19994.1624;
pIP8                := 23315.3028;
HRF200              := 17820;
HRF400              := 35640;

A.MB                := TWOPI/8/(23*6+2*2*4);
AB.A12              := A.MB;
AB.A23              := A.MB;
AB.A34              := A.MB;
AB.A45              := A.MB;
AB.A56              := A.MB;
AB.A67              := A.MB;
AB.A78              := A.MB;
AB.A81              := A.MB;

l.BPMQSTZA          := 0;
l.BPMQSTZB          := 0;
l.MBXF              := 6.27;
l.MCBXFAH           := 2.2;
l.MCBXFAV           := 2.2;
l.MCBXFBH           := 1.2;
l.MCBXFBV           := 1.2;
l.MCDSXF            := 0.145;
l.MCDXF             := 0.145;
l.MCOSXF            := 0.145;
l.MCOXF             := 0.145;
l.MCSSXF            := 0.168;
l.MCSXF             := 0.168;
l.MCTSXF            := 0.103;
l.MCTXF             := 0.469;
l.MQSXF             := 0.401;
l.MQXFA             := 4.213;
l.MQXFB             := 7.172;
l.OMK               := 0;

R0                  := 1.0;
DS                  := 0.097*TWOPI/8/(23*6+2*2*4)*(1-R0);

AD1.L2              := AIP2*(1-R0);
AD1.L8              := AIP8*(1-R0);
AD1.LR1             := AIP1/6*(1-R0);
AD1.LR5             := AIP5/6*(1-R0);
AD1.R2              := AIP2*(1-R0);
AD1.R8              := AIP8*(1-R0);
AD2.L1              := AIP1*(1-R0);
AD2.L2              := AIP2*(1-R0);
AD2.L5              := AIP5*(1-R0);
AD2.L8              := AIP8*(1-R0);
AD2.R1              := AIP1*(1-R0);
AD2.R2              := AIP2*(1-R0);
AD2.R5              := AIP5*(1-R0);
AD2.R8              := AIP8*(1-R0);
AD3.L4              := AIP4*(1-R0);
AD3.LR3             := (AIP3/3)*(1-R0);
AD3.LR7             := (AIP7/2)*(1-R0);
AD3.R4              := AIP4*(1-R0);
AD4.L4              := AIP4*(1-R0);
AD4.LR3             := (AIP3/3)*(1-R0);
AD4.LR7             := (AIP7/2)*(1-R0);
AD4.R4              := AIP4*(1-R0);
AD34.LR3            := (AIP3/3)*(1-R0);
AD34.LR7            := (AIP7/2)*(1-R0);

/************************************************************************************/
/*                       CLASSES DEFINITION                                         */
/************************************************************************************/

HCORRECTOR: HKICKER;
VCORRECTOR: VKICKER;

//---------------------- HCORRECTOR     ---------------------------------------------
MCBXFAH : HCORRECTOR, Lrad := l.MCBXFAH, Kmax := Kmax_MCBXFAH, Kmin := Kmin_MCBXFAH, Calib := Kmax_MCBXFAH / Imax_MCBXFAH;        ! Single Aperture (150mm) Horizontal Orbit Corrector Associated to MQXFA (Q1-Q3) - 2.2m length
MCBXFBH : HCORRECTOR, Lrad := l.MCBXFBH, Kmax := Kmax_MCBXFBH, Kmin := Kmin_MCBXFBH, Calib := Kmax_MCBXFBH / Imax_MCBXFBH;        ! Single Aperture (150mm) Horizontal Orbit Corrector Associated to MQXFB (Q2) - 1.2m length
//---------------------- MARKER         ---------------------------------------------
OMK : MARKER, L := l.OMK;                                                                                                         ! LHC markers
//---------------------- MONITOR        ---------------------------------------------
BPMQSTZA : MONITOR, L := l.BPMQSTZA;                                                                                              ! HL-LHC BPM - Stripline - Inner triplet - Assembly Octagonal 99.7/99.7
BPMQSTZB : MONITOR, L := l.BPMQSTZB;                                                                                              ! HL-LHC BPM - Stripline - Inner triplet - Assembly Octagonal 119.7/110.7
//---------------------- MULTIPOLE      ---------------------------------------------
MCDSXF : MULTIPOLE, Lrad := l.MCDSXF, Kmax := Kmax_MCDSXF, Kmin := Kmin_MCDSXF, Calib := Kmax_MCDSXF / Imax_MCDSXF;               ! Single Aperture (150 mm) Skew Decapole (a5)
MCDXF : MULTIPOLE, Lrad := l.MCDXF, Kmax := Kmax_MCDXF, Kmin := Kmin_MCDXF, Calib := Kmax_MCDXF / Imax_MCDXF;                     ! Single Aperture (150 mm) Decapole (b5) MCDXF
MCOSXF : MULTIPOLE, Lrad := l.MCOSXF, Kmax := Kmax_MCOSXF, Kmin := Kmin_MCOSXF, Calib := Kmax_MCOSXF / Imax_MCOSXF;               ! Single Aperture (150 mm) Skew Octupole (a4) MCOSXF
MCOXF : MULTIPOLE, Lrad := l.MCOXF, Kmax := Kmax_MCOXF, Kmin := Kmin_MCOXF, Calib := Kmax_MCOXF / Imax_MCOXF;                     ! Single Aperture (150 mm) Octupole (b4) MCOXF
MCSSXF : MULTIPOLE, Lrad := l.MCSSXF, Kmax := Kmax_MCSSXF, Kmin := Kmin_MCSSXF, Calib := Kmax_MCSSXF / Imax_MCSSXF;               ! Single Aperture (150 mm) Skew Sextupole (a3)
MCSXF : MULTIPOLE, Lrad := l.MCSXF, Kmax := Kmax_MCSXF, Kmin := Kmin_MCSXF, Calib := Kmax_MCSXF / Imax_MCSXF;                     ! Single Aperture (150 mm) Sextupole (b3) MCSXF
MCTSXF : MULTIPOLE, Lrad := l.MCTSXF, Kmax := Kmax_MCTSXF, Kmin := Kmin_MCTSXF, Calib := Kmax_MCTSXF / Imax_MCTSXF;               ! Single Aperture (150 mm) Skew Dodecapole (a6)
MCTXF : MULTIPOLE, Lrad := l.MCTXF, Kmax := Kmax_MCTXF, Kmin := Kmin_MCTXF, Calib := Kmax_MCTXF / Imax_MCTXF;                     ! Single Aperture (150 mm) Dodecapole (b6)
//---------------------- QUADRUPOLE     ---------------------------------------------
MQSXF : QUADRUPOLE, L := l.MQSXF, Kmax := Kmax_MQSXF, Kmin := Kmin_MQSXF, Calib := Kmax_MQSXF / Imax_MQSXF;                       ! Single Aperture (150 mm) Skew Quadrupole (a2)
MQXFA : QUADRUPOLE, L := l.MQXFA, Kmax := Kmax_MQXFA, Kmin := Kmin_MQXFA, Calib := Kmax_MQXFA / Imax_MQXFA;                       ! 150mm Single Aperture Nb3Sn Magnets (Q1,Q3)
MQXFB : QUADRUPOLE, L := l.MQXFB, Kmax := Kmax_MQXFB, Kmin := Kmin_MQXFB, Calib := Kmax_MQXFB / Imax_MQXFB;                       ! 150mm Single Aperture Nb3Sn Series Long Magnet (Q2)
//---------------------- RBEND          ---------------------------------------------
MBXF : RBEND, L := l.MBXF, Kmax := Kmax_MBXF, Kmin := Kmin_MBXF, Calib := Kmax_MBXF / Imax_MBXF;                                  ! Single Aperture (150mm) Separation Dipole (D1)
//---------------------- VCORRECTOR     ---------------------------------------------
MCBXFAV : VCORRECTOR, Lrad := l.MCBXFAV, Kmax := Kmax_MCBXFAV, Kmin := Kmin_MCBXFAV, Calib := Kmax_MCBXFAV / Imax_MCBXFAV;        ! Single Aperture (150mm) Vertical Orbit Corrector Associated to MQXFA (Q1-Q3) - 2.2m length
MCBXFBV : VCORRECTOR, Lrad := l.MCBXFBV, Kmax := Kmax_MCBXFBV, Kmin := Kmin_MCBXFBV, Calib := Kmax_MCBXFBV / Imax_MCBXFBV;        ! Single Aperture (150mm) Vertical Orbit Corrector Associated to MQXFB (Q2) - 1.2m length

/************************************************************************************/
/*                       STRING SEQUENCE                                            */
/************************************************************************************/

STRINGB1 : SEQUENCE, refer = centre, L = pIP5;
  MBXF.4SF:MBXF,                   at= -77.534+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952103, assembly_id= 56952100,            from= IP5SF;
  BPMQSTZB.4SF:BPMQSTZB,           at= -73.723+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952101, assembly_id= 56952100,            from= IP5SF;
  MCSSXF.3SF:MCSSXF,               at= -72.851+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952403, assembly_id= 56952396,            from= IP5SF;
  MCSXF.3SF:MCSXF,                 at= -72.543+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952404, assembly_id= 56952396,            from= IP5SF;
  MCOSXF.3SF:MCOSXF,               at= -72.2435+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952401, assembly_id= 56952396,           from= IP5SF;
  MCOXF.3SF:MCOXF,                 at= -71.9535+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952402, assembly_id= 56952396,           from= IP5SF;
  MCDSXF.3SF:MCDSXF,               at= -71.6635+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952399, assembly_id= 56952396,           from= IP5SF;
  MCDXF.3SF:MCDXF,                 at= -71.3735+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952400, assembly_id= 56952396,           from= IP5SF;
  MCTSXF.3SF:MCTSXF,               at= -71.1035+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952405, assembly_id= 56952396,           from= IP5SF;
  MCTXF.3SF:MCTXF,                 at= -70.6655+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952406, assembly_id= 56952396,           from= IP5SF;
  MQSXF.3SF:MQSXF,                 at= -70.0255+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952407, assembly_id= 56952396,           from= IP5SF;
  MCBXFAH.3SF:MCBXFAH,             at= -68.416+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952413, assembly_id= 56952396,            from= IP5SF;
  MCBXFAV.3SF:MCBXFAV,             at= -68.416+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952414, assembly_id= 56952396,            from= IP5SF;
  BPMQSTZB.B3SF:BPMQSTZB,          at= -65.77+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952408, assembly_id= 56952396,             from= IP5SF;
  MQXFA.B3SF:MQXFA,                at= -62.475+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952394, assembly_id= 56952391,            from= IP5SF;
  MQXFA.A3SF:MQXFA,                at= -57.711+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952395, assembly_id= 56952391,            from= IP5SF;
  BPMQSTZB.A3SF:BPMQSTZB,          at= -54.679+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56952392, assembly_id= 56952391,            from= IP5SF;
  MCBXFBH.B2SF:MCBXFBH,            at= -53.028+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953065, assembly_id= 56953055,            from= IP5SF;
  MCBXFBV.B2SF:MCBXFBV,            at= -53.028+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953066, assembly_id= 56953055,            from= IP5SF;
  MQXFB.B2SF:MQXFB,                at= -48.401+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953059, assembly_id= 56953055,            from= IP5SF;
  BPMQSTZB.B2SF:BPMQSTZB,          at= -43.893+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953056, assembly_id= 56953055,            from= IP5SF;
  MQXFB.A2SF:MQXFB,                at= -39.115+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953053, assembly_id= 56953049,            from= IP5SF;
  MCBXFBH.A2SF:MCBXFBH,            at= -34.488+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953063, assembly_id= 56953049,            from= IP5SF;
  MCBXFBV.A2SF:MCBXFBV,            at= -34.488+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953064, assembly_id= 56953049,            from= IP5SF;
  BPMQSTZB.A2SF:BPMQSTZB,          at= -33.108+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953050, assembly_id= 56953049,            from= IP5SF;
  MQXFA.B1SF:MQXFA,                at= -29.805+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953514, assembly_id= 56953508,            from= IP5SF;
  MQXFA.A1SF:MQXFA,                at= -25.041+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953513, assembly_id= 56953508,            from= IP5SF;
  BPMQSTZA.1SF:BPMQSTZA,           at= -21.871+(-308-IP5OFS.B1)*DS, mech_sep= 0, slot_id= 56953511, assembly_id= 56953508,            from= IP5SF;
IP5SF:OMK,                         at= pIP5+IP5OFS.B1*DS;
ENDSEQUENCE;

STRINGB2 : SEQUENCE, refer = centre, L = pIP5;
  MBXF.4SF:MBXF,                   at= -77.534+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952103, assembly_id= 56952100,             from= IP5SF;
  BPMQSTZB.4SF:BPMQSTZB,           at= -73.723+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952101, assembly_id= 56952100,             from= IP5SF;
  MCSSXF.3SF:MCSSXF,               at= -72.851+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952403, assembly_id= 56952396,             from= IP5SF;
  MCSXF.3SF:MCSXF,                 at= -72.543+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952404, assembly_id= 56952396,             from= IP5SF;
  MCOSXF.3SF:MCOSXF,               at= -72.2435+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952401, assembly_id= 56952396,            from= IP5SF;
  MCOXF.3SF:MCOXF,                 at= -71.9535+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952402, assembly_id= 56952396,            from= IP5SF;
  MCDSXF.3SF:MCDSXF,               at= -71.6635+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952399, assembly_id= 56952396,            from= IP5SF;
  MCDXF.3SF:MCDXF,                 at= -71.3735+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952400, assembly_id= 56952396,            from= IP5SF;
  MCTSXF.3SF:MCTSXF,               at= -71.1035+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952405, assembly_id= 56952396,            from= IP5SF;
  MCTXF.3SF:MCTXF,                 at= -70.6655+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952406, assembly_id= 56952396,            from= IP5SF;
  MQSXF.3SF:MQSXF,                 at= -70.0255+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952407, assembly_id= 56952396,            from= IP5SF;
  MCBXFAH.3SF:MCBXFAH,             at= -68.416+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952413, assembly_id= 56952396,             from= IP5SF;
  MCBXFAV.3SF:MCBXFAV,             at= -68.416+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952414, assembly_id= 56952396,             from= IP5SF;
  BPMQSTZB.B3SF:BPMQSTZB,          at= -65.77+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952408, assembly_id= 56952396,              from= IP5SF;
  MQXFA.B3SF:MQXFA,                at= -62.475+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952394, assembly_id= 56952391,             from= IP5SF;
  MQXFA.A3SF:MQXFA,                at= -57.711+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952395, assembly_id= 56952391,             from= IP5SF;
  BPMQSTZB.A3SF:BPMQSTZB,          at= -54.679+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56952392, assembly_id= 56952391,             from= IP5SF;
  MCBXFBH.B2SF:MCBXFBH,            at= -53.028+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953065, assembly_id= 56953055,             from= IP5SF;
  MCBXFBV.B2SF:MCBXFBV,            at= -53.028+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953066, assembly_id= 56953055,             from= IP5SF;
  MQXFB.B2SF:MQXFB,                at= -48.401+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953059, assembly_id= 56953055,             from= IP5SF;
  BPMQSTZB.B2SF:BPMQSTZB,          at= -43.893+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953056, assembly_id= 56953055,             from= IP5SF;
  MQXFB.A2SF:MQXFB,                at= -39.115+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953053, assembly_id= 56953049,             from= IP5SF;
  MCBXFBH.A2SF:MCBXFBH,            at= -34.488+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953063, assembly_id= 56953049,             from= IP5SF;
  MCBXFBV.A2SF:MCBXFBV,            at= -34.488+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953064, assembly_id= 56953049,             from= IP5SF;
  BPMQSTZB.A2SF:BPMQSTZB,          at= -33.108+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953050, assembly_id= 56953049,             from= IP5SF;
  MQXFA.B1SF:MQXFA,                at= -29.805+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953514, assembly_id= 56953508,             from= IP5SF;
  MQXFA.A1SF:MQXFA,                at= -25.041+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953513, assembly_id= 56953508,             from= IP5SF;
  BPMQSTZA.1SF:BPMQSTZA,           at= -21.871+(308-IP5OFS.B2)*DS, mech_sep= 0, slot_id= 56953511, assembly_id= 56953508,             from= IP5SF;
IP5SF:OMK,                         at= pIP5+IP5OFS.B2*DS;
ENDSEQUENCE;

return;