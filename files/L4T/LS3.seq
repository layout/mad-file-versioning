

  /**********************************************************************************
  *
  * L4T version (draft) LS3 in MAD X SEQUENCE format
  * Generated the 26-OCT-2024 02:23:32 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.L4T_ACDB                     := 0;
l.L4T_BCT__112                 := 0;
l.L4T_BCT__182                 := 0;
l.L4T_BEL__001                 := 0;
l.L4T_BEP__001                 := 0;
l.L4T_BPLFS                    := 0;
l.L4T_BPLFS002                 := 0;
l.L4T_BPUSE                    := 0;
l.L4T_BSGHV001                 := 0;
l.L4T_BWS__002                 := 0;
l.L4T_MBH                      := 0.876041170758117;
l.L4T_MBV                      := 0.872317453179938;
l.L4T_MCHAFCAP                 := 0;
l.L4T_MCHV_001                 := 0;
l.L4T_MQD__105                 := 0.105;
l.L4T_MQD__300                 := 0.2921;
l.L4T_MQF__105                 := 0.105;
l.L4T_MQF__300                 := 0.2921;
l.L4T_OMK                      := 0;
l.L4T_TDISA                    := 0;
l.L4T_TKSTR                    := 0;

//---------------------- COLLIMATOR     ---------------------------------------------
L4T_TDISA                : COLLIMATOR  , L := l.L4T_TDISA;         ! Beam Dump, Internal, with beam Stopper functionality, Assembly
//---------------------- INSTRUMENT     ---------------------------------------------
L4T_BWS__002             : INSTRUMENT  , L := l.L4T_BWS__002;      ! Wire Scanners Profile Monitors
L4T_TKSTR                : INSTRUMENT  , L := l.L4T_TKSTR;         ! PSB Kickers - Targets:  Stripping Foil Assembly
//---------------------- KICKER         ---------------------------------------------
L4T_MCHAFCAP             : KICKER      , L := l.L4T_MCHAFCAP;      ! Corrector magnet, horizontal, LINAC4 laser electron monitor
L4T_MCHV_001             : KICKER      , L := l.L4T_MCHV_001;      ! Corrector magnet, H+V steering, 120mm, L4 transfer line or Linac4 - Length:.12
//---------------------- MARKER         ---------------------------------------------
L4T_OMK                  : MARKER      , L := l.L4T_OMK;           ! L4T markers
//---------------------- MATRIX         ---------------------------------------------
L4T_ACDB                 : MATRIX      , L := l.L4T_ACDB;          ! Debunching cavity
//---------------------- MONITOR        ---------------------------------------------
L4T_BCT__112             : MONITOR     , L := l.L4T_BCT__112;      ! Beam current transformer, L4L
L4T_BCT__182             : MONITOR     , L := l.L4T_BCT__182;      ! Beam current transformer, L4T
L4T_BEL__001             : MONITOR     , L := l.L4T_BEL__001;      ! Beam Emittance - Laser Station
L4T_BEP__001             : MONITOR     , L := l.L4T_BEP__001;      ! Beam Emittance Profile - Electron Detector
L4T_BPLFS                : MONITOR     , L := l.L4T_BPLFS;         ! Longitudinal Bunch Profile monitor Feshenko
L4T_BPLFS002             : MONITOR     , L := l.L4T_BPLFS002;      ! Longitudinal Bunch Profile monitor Feshenko
L4T_BPUSE                : MONITOR     , L := l.L4T_BPUSE;         ! Beam Position Pick-up, Stripline, type E
L4T_BSGHV001             : MONITOR     , L := l.L4T_BSGHV001;      ! SEM Grid (secondary emission monitor) Horizontal and Vertical
//---------------------- QUADRUPOLE     ---------------------------------------------
L4T_MQD__105             : QUADRUPOLE  , L := l.L4T_MQD__105;      ! Quadrupole Magnet - Defocusing - Length:.105
L4T_MQD__300             : QUADRUPOLE  , L := l.L4T_MQD__300;      ! Quadrupole Magnet - Defocusing - Length:.3
L4T_MQF__105             : QUADRUPOLE  , L := l.L4T_MQF__105;      ! Quadrupole Magnet - Focusing - Magnetic Length:.105
L4T_MQF__300             : QUADRUPOLE  , L := l.L4T_MQF__300;      ! Quadrupole Magnet - Focusing - Magnetic Length:.3
//---------------------- SBEND          ---------------------------------------------
L4T_MBH                  : SBEND       , L := l.L4T_MBH;           ! Horizontal Bending Magnets
L4T_MBV                  : SBEND       , L := l.L4T_MBV;           ! Vertical Bending Magnets


/************************************************************************************/
/*                       L4T SEQUENCE                                                */
/************************************************************************************/

L4T : SEQUENCE, refer = centre,         L = 70.12612;
 LT.BHZ20OUT                   : L4T_OMK                , at = 0            , slot_id = 3805487;
 L4T.MCHV.0105                 : L4T_MCHV_001           , at = .38          , slot_id = 5528968;
 L4T.BCT.0107                  : L4T_BCT__112           , at = .725         , slot_id = 5528967;
 L4T.MQD.0110                  : L4T_MQD__105           , at = 1.1          , slot_id = 5528969;
 L4T.BEL.0112                  : L4T_BEL__001           , at = 1.5834       , slot_id = 42550988;
 L4T.BEP.0113                  : L4T_BEP__001           , at = 1.7812       , slot_id = 42550995, assembly_id= 42551145;
 L4T.MCH.0113                  : L4T_MCHAFCAP           , at = 1.7812       , slot_id = 42550998, assembly_id= 42551145;
 L4T.MCHV.0115                 : L4T_MCHV_001           , at = 2.1          , slot_id = 5528970, assembly_id= 42551145;
 L4T.TKSTR.0124                : L4T_TKSTR              , at = 2.565        , slot_id = 14302971;
 L4T.MCHV.0135                 : L4T_MCHV_001           , at = 3.1          , slot_id = 5528971;
 L4T.MQF.0210                  : L4T_MQF__105           , at = 4.1          , slot_id = 5528972, assembly_id= 34467433;
 L4T.BSGHV.0223                : L4T_BSGHV001           , at = 4.299        , slot_id = 5528973, assembly_id= 34467433;
 L4T.BWS.0223                  : L4T_BWS__002           , at = 4.342        , slot_id = 36269851, assembly_id= 34467433;
 L4T.BPM.0227                  : L4T_BPUSE              , at = 4.7323       , slot_id = 5528974;
 L4T.BPLFS.0233                : L4T_BPLFS              , at = 5.55         , slot_id = 5528975;
 L4T.BPM.0237                  : L4T_BPUSE              , at = 6.2323       , slot_id = 5528976, assembly_id= 34467434;
 L4T.BSGHV.0243                : L4T_BSGHV001           , at = 6.609        , slot_id = 5528977, assembly_id= 34467434;
 L4T.BWS.0243                  : L4T_BWS__002           , at = 6.652        , slot_id = 36269854, assembly_id= 34467434;
 L4T.MBH.0250IN                : L4T_OMK                , at = 7.031348     , slot_id = 3805487;
 L4T.MBH.0250                  : L4T_MBH                , at = 7.46936      , slot_id = 5528978;
 L4T.MBH.0250OUT               : L4T_OMK                , at = 7.919701     , slot_id = 3805487;
 L4T.MQD.0310                  : L4T_MQD__300           , at = 8.96321      , slot_id = 5528979;
 L4T.MCHV.0315                 : L4T_MCHV_001           , at = 9.51321      , slot_id = 5528980;
 L4T.MQF.0410                  : L4T_MQF__300           , at = 10.06321     , slot_id = 5528981;
 L4T.BEL.0422                  : L4T_BEL__001           , at = 10.40041     , slot_id = 42551205;
 L4T.BEP.0428                  : L4T_BEP__001           , at = 10.59821     , slot_id = 42551310;
 L4T.MCH.0428                  : L4T_MCHAFCAP           , at = 10.59821     , slot_id = 42551307;
 L4T.MBH.0450IN                : L4T_OMK                , at = 11.131348    , slot_id = 3805487;
 L4T.MBH.0450                  : L4T_MBH                , at = 11.55705     , slot_id = 5528982;
 L4T.MBH.0450OUT               : L4T_OMK                , at = 12.019701    , slot_id = 3805487;
 L4T.MQF.0510                  : L4T_MQF__300           , at = 13.0509      , slot_id = 5528983;
 L4T.BSGHV.0523                : L4T_BSGHV001           , at = 13.5799      , slot_id = 5528984;
 L4T.BWS.0523                  : L4T_BWS__002           , at = 13.6229      , slot_id = 36269855;
 L4T.MQD.0610                  : L4T_MQD__300           , at = 14.1509      , slot_id = 5528985;
 L4T.MBH.0650IN                : L4T_OMK                , at = 15.231348    , slot_id = 3805487;
 L4T.MBH.0650                  : L4T_MBH                , at = 15.64474     , slot_id = 5528986;
 L4T.MBH.0650OUT               : L4T_OMK                , at = 16.119701    , slot_id = 3805487;
 L4T.BCT.0673                  : L4T_BCT__182           , at = 17.02859     , slot_id = 5528987;
 L4T.MQF.0710                  : L4T_MQF__300           , at = 17.63859     , slot_id = 5528988;
 L4T.MCHV.0715                 : L4T_MCHV_001           , at = 18.33859     , slot_id = 5528989;
 L4T.MCHV.0735                 : L4T_MCHV_001           , at = 19.33859     , slot_id = 5528990;
 L4T.TDISA.0740                : L4T_TDISA              , at = 21.26159     , slot_id = 5528992;
 L4T.MQD.0810                  : L4T_MQD__300           , at = 22.93859     , slot_id = 5528993;
 L4T.BPM.0827                  : L4T_BPUSE              , at = 25.57094     , slot_id = 5528994;
 L4T.BPM.0837                  : L4T_BPUSE              , at = 27.07094     , slot_id = 5528995;
 L4T.MQF.0910                  : L4T_MQF__300           , at = 28.23859     , slot_id = 5528996;
 L4T.MCHV.0915                 : L4T_MCHV_001           , at = 28.93859     , slot_id = 5528997;
 L4T.MCHV.0935                 : L4T_MCHV_001           , at = 29.93859     , slot_id = 5528998;
 L4T.MQD.1010                  : L4T_MQD__300           , at = 33.53859     , slot_id = 5528999;
 L4T.BPM.1027                  : L4T_BPUSE              , at = 36.47094     , slot_id = 5529001;
 L4T.BPM.1037                  : L4T_BPUSE              , at = 37.97094     , slot_id = 5529002;
 L4T.BCT.1043                  : L4T_BCT__112           , at = 40.36859     , slot_id = 5529003;
 L4T.ACDB.1075                 : L4T_ACDB               , at = 41.53859     , slot_id = 5529004;
 L4T.MCHV.1085                 : L4T_MCHV_001           , at = 43.53859     , slot_id = 5529006;
 L4T.MCHV.1095                 : L4T_MCHV_001           , at = 44.53859     , slot_id = 5529007;
 L4T.MQD.1110                  : L4T_MQD__300           , at = 45.23859     , slot_id = 5529008;
 L4T.MQF.1210                  : L4T_MQF__300           , at = 46.48859     , slot_id = 5529009;
 L4T.BPM.1227                  : L4T_BPUSE              , at = 47.07094     , slot_id = 5529010;
 L4T.BCT.1243                  : L4T_BCT__182           , at = 48.29859     , slot_id = 5529015, assembly_id= 34467435;
 L4T.BPM.1245                  : L4T_BPUSE              , at = 48.57094     , slot_id = 5529016, assembly_id= 34467435;
 L4T.BSGHV.1247                : L4T_BSGHV001           , at = 49.2226      , slot_id = 5529017;
 L4T.BWS.1247                  : L4T_BWS__002           , at = 49.2656      , slot_id = 36269856;
 L4T.MBV.1250IN                : L4T_OMK                , at = 50.087031    , slot_id = 3805487;
 L4T.MBV.1250                  : L4T_MBV                , at = 50.48625     , slot_id = 5529018;
 L4T.MBV.1250OUT               : L4T_OMK                , at = 50.964017    , slot_id = 3805487;
 L4T.MQD.1310                  : L4T_MQD__300           , at = 52.98392     , slot_id = 5529019;
 L4T.MCHV.1315                 : L4T_MCHV_001           , at = 54.23392     , slot_id = 5529020;
 L4T.MQF.1410                  : L4T_MQF__300           , at = 55.48392     , slot_id = 5529021;
 L4T.MCHV.1415                 : L4T_MCHV_001           , at = 56.73392     , slot_id = 5529022;
 L4T.MQD.1510                  : L4T_MQD__300           , at = 57.98392     , slot_id = 5529023;
 L4T.MBV.1550IN                : L4T_OMK                , at = 60.087031    , slot_id = 3805487;
 L4T.MBV.1550                  : L4T_MBV                , at = 60.48158     , slot_id = 5529025;
 L4T.MBV.1550OUT               : L4T_OMK                , at = 60.964017    , slot_id = 3805487;
 L4T.BCT.1553                  : L4T_BCT__182           , at = 61.62925     , slot_id = 5529026, assembly_id= 34467436;
 L4T.BPM.1557                  : L4T_BPUSE              , at = 61.9216      , slot_id = 5529027, assembly_id= 34467436;
 L4T.MCHV.1605                 : L4T_MCHV_001           , at = 62.33123     , slot_id = 5529030, assembly_id= 34467437;
 L4T.MQD.1610                  : L4T_MQD__300           , at = 62.67925     , slot_id = 5529028, assembly_id= 34467437;
 L4T.BPM.1627                  : L4T_BPUSE              , at = 62.9616      , slot_id = 5529029, assembly_id= 34467437;
 L4T.BPLFS.1663                : L4T_BPLFS002           , at = 63.46925     , slot_id = 6611797;
 L4T.MQF.1710                  : L4T_MQF__300           , at = 63.92925     , slot_id = 5529031;
 LT.BHZ20IN                    : L4T_OMK                , at = 70.172393    , slot_id = 3805487;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTH CONSTANTS                                         */
/************************************************************************************/

angle.L4T.MBH.0250             := 0.407243448374;
angle.L4T.MBH.0450             := 0.407243448374;
angle.L4T.MBH.0650             := 0.407243448374;
angle.L4T.MBV.1250             := 0.2526079601;
angle.L4T.MBV.1550             := -0.2526079601;
e1.L4T.MBH.0250                := 0.203621724187;
e1.L4T.MBH.0450                := 0.203621724187;
e1.L4T.MBH.0650                := 0.203621724187;
e1.L4T.MBV.1250                := 0.12630398005;
e1.L4T.MBV.1550                := -0.12630398005;
e2.L4T.MBH.0250                := 0.203621724187;
e2.L4T.MBH.0450                := 0.203621724187;
e2.L4T.MBH.0650                := 0.203621724187;
e2.L4T.MBV.1250                := 0.12630398005;
e2.L4T.MBV.1550                := -0.12630398005;
fint.L4T.MBH.0250              := 0.7;
fint.L4T.MBH.0450              := 0.7;
fint.L4T.MBH.0650              := 0.7;
fint.L4T.MBV.1250              := 0;
fint.L4T.MBV.1550              := 0;
hgap.L4T.MBH.0250              := 0.025;
hgap.L4T.MBH.0450              := 0.025;
hgap.L4T.MBH.0650              := 0.025;
hgap.L4T.MBV.1250              := 0.025;
hgap.L4T.MBV.1550              := 0.025;
l.L4T.MBH.0250                 := 0.876041170758117;
l.L4T.MBH.0450                 := 0.876041170758117;
l.L4T.MBH.0650                 := 0.876041170758117;
l.L4T.MBV.1250                 := 0.872317453179938;
l.L4T.MBV.1550                 := 0.872317453179938;
tilt.L4T.MBV.1250              := -pi/2;
tilt.L4T.MBV.1550              := -pi/2;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/
  L4T.MBH.0250,              L := l.L4T.MBH.0250, ANGLE := angle.L4T.MBH.0250, E1 := e1.L4T.MBH.0250, E2 := e2.L4T.MBH.0250, FINT := fint.L4T.MBH.0250, HGAP := hgap.L4T.MBH.0250;
  L4T.MBH.0450,              L := l.L4T.MBH.0450, ANGLE := angle.L4T.MBH.0450, E1 := e1.L4T.MBH.0450, E2 := e2.L4T.MBH.0450, FINT := fint.L4T.MBH.0450, HGAP := hgap.L4T.MBH.0450;
  L4T.MBH.0650,              L := l.L4T.MBH.0650, ANGLE := angle.L4T.MBH.0650, E1 := e1.L4T.MBH.0650, E2 := e2.L4T.MBH.0650, FINT := fint.L4T.MBH.0650, HGAP := hgap.L4T.MBH.0650;
  L4T.MBV.1250,              L := l.L4T.MBV.1250, ANGLE := angle.L4T.MBV.1250, TILT := tilt.L4T.MBV.1250, E1 := e1.L4T.MBV.1250, E2 := e2.L4T.MBV.1250, FINT := fint.L4T.MBV.1250, HGAP := hgap.L4T.MBV.1250;
  L4T.MBV.1550,              L := l.L4T.MBV.1550, ANGLE := angle.L4T.MBV.1550, TILT := tilt.L4T.MBV.1550, E1 := e1.L4T.MBV.1550, E2 := e2.L4T.MBV.1550, FINT := fint.L4T.MBV.1550, HGAP := hgap.L4T.MBV.1550;
return;