/************************************************************************************************************************************************************************
*
* T10 version YETS 2021-2022 in MAD X SEQUENCE format
* Generated the 16-NOV-2024 03:39:33 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.T10_BXBPFS01                 := 0.152;
l.T10_BXSCI                    := 0.2;
l.T10_MBXGFHWP                 := 1;
l.T10_MBXHFHWP                 := 2;
l.T10_MCXCFHWP                 := 0.4;
l.T10_MQNDCTWP                 := 0.82;
l.T10_MQNEGTWP                 := 0.8;
l.T10_MQNEL8WP                 := 1.2;
l.T10_MQNEVTWP                 := 1;
l.T10_MQNFKTWP                 := 2;
l.T10_OMK                      := 0;
l.T10_TMMTV                    := 0.2;
l.T10_XCETWAA001               := 0.0014;
l.T10_XCETWAAP01               := 0.0014;
l.T10_XCETWAM003               := 0.0004;
l.T10_XCETWAMP03               := 0.0004;
l.T10_XCET_003                 := 0.34;
l.T10_XCET_004                 := 0.34;
l.T10_XCHV_001                 := 1;
l.T10_XCSH_001                 := 1;
l.T10_XCSV_001                 := 1;
l.T10_XVWAD001                 := 0.0001;
l.T10_XVWAT004                 := 0.0002;
l.T10_XVWAU002                 := 0.0001;

//---------------------- COLLIMATOR     ---------------------------------------------
T10_XCHV_001   : COLLIMATOR  , L := l.T10_XCHV_001;      ! Collimator 4 blocks, horizontal and vertical (design 1970)
T10_XCSH_001   : COLLIMATOR  , L := l.T10_XCSH_001;      ! Collimator 2 blocks horizontal (design 1970)
T10_XCSV_001   : COLLIMATOR  , L := l.T10_XCSV_001;      ! Collimator 2 blocks vertical (design 1970)
//---------------------- HKICKER        ---------------------------------------------
T10_MCXCFHWP   : HKICKER     , L := l.T10_MCXCFHWP;      ! Corrector magnet, H or V, type MDX laminated aperture 150mm
//---------------------- INSTRUMENT     ---------------------------------------------
T10_TMMTV      : INSTRUMENT  , L := l.T10_TMMTV;         ! Target, Mobile, Multiple, with TV screen
T10_XCETWAA001 : INSTRUMENT  , L := l.T10_XCETWAA001;    ! XCET Beam Window DN159 - Aluminium - 1.4 mm thick
T10_XCETWAAP01 : INSTRUMENT  , L := l.T10_XCETWAAP01;    ! XCET Beam Window DN159 - Aluminium - 1.4 mm thick - with Purge
T10_XCETWAM003 : INSTRUMENT  , L := l.T10_XCETWAM003;    ! XCET Beam Window DN159 - Mylar/Polyethylene  - 0.25/0.15 mm thick
T10_XCETWAMP03 : INSTRUMENT  , L := l.T10_XCETWAMP03;    ! XCET Beam Window DN159 - Mylar/Polyethylene  - 0.25/0.15 mm thick - with Purge
T10_XCET_003   : INSTRUMENT  , L := l.T10_XCET_003;      ! Cherenkov Counter DN150 - 2975 mm - Fenetre Al
T10_XCET_004   : INSTRUMENT  , L := l.T10_XCET_004;      ! Cherenkov Counter DN150 - 2595 mm - Fenetre Mylar
T10_XVWAD001   : INSTRUMENT  , L := l.T10_XVWAD001;      ! X Vacuum Window Aluminium DN159X0.1, aperture 120 mm
T10_XVWAT004   : INSTRUMENT  , L := l.T10_XVWAT004;      ! X Vacuum Window Mylar + pumping port DN159X99x0.2
T10_XVWAU002   : INSTRUMENT  , L := l.T10_XVWAU002;      ! X Vacuum Window Mylar DN219X65x0.2
//---------------------- MARKER         ---------------------------------------------
T10_OMK        : MARKER      , L := l.T10_OMK;           ! T10 markers
//---------------------- MONITOR        ---------------------------------------------
T10_BXBPFS01   : MONITOR     , L := l.T10_BXBPFS01;      ! Scintillating fibre beam profile monitor - Small
T10_BXSCI      : MONITOR     , L := l.T10_BXSCI;         ! Scintillator Counter Detector (Intensity Monitor)
//---------------------- QUADRUPOLE     ---------------------------------------------
T10_MQNDCTWP   : QUADRUPOLE  , L := l.T10_MQNDCTWP;      ! Quadrupole magnet, ISR, type QDS, 0.82m
T10_MQNEGTWP   : QUADRUPOLE  , L := l.T10_MQNEGTWP;      ! Quadrupole magnet, type QFS, 0.8m
T10_MQNEL8WP   : QUADRUPOLE  , L := l.T10_MQNEL8WP;      ! Quadrupole magnet, type Q120 laminated, 1.2 m
T10_MQNEVTWP   : QUADRUPOLE  , L := l.T10_MQNEVTWP;      ! Quadrupole magnet, type Q100 laminated
T10_MQNFKTWP   : QUADRUPOLE  , L := l.T10_MQNFKTWP;      ! Quadrupole magnet, type Q200 laminated
//---------------------- RBEND          ---------------------------------------------
T10_MBXGFHWP   : RBEND       , L := l.T10_MBXGFHWP;      ! Bending magnet, type M100 laminated
T10_MBXHFHWP   : RBEND       , L := l.T10_MBXHFHWP;      ! Bending magnet, type M200 laminated

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kT10.BHZ017                    := -0.086;
kT10.BHZ027                    := -0.086;
kT10.BVT032                    := 0.035;
kT10.DHZ040                    := 0;
tilt.T10.BHZ017                := 0.00150654406078094;
tilt.T10.BHZ027                := 0.004519632182630779;
tilt.T10.BVT032                := 1.5768225030385963;
tilt.T10.DHZ040                := 0.006026176243699678;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 T10.XBPF045                                       : T10_BXBPFS01;
 T10.XSCI036                                       : T10_BXSCI;
 T10.XSCI044                                       : T10_BXSCI;
 T10.BVT032                                        : T10_MBXGFHWP    , APERTYPE=RECTANGLE, APERTURE={.26,.055,.26,.055}, ANGLE := kT10.BVT032, TILT := tilt.T10.BVT032;
 T10.BHZ017                                        : T10_MBXHFHWP    , APERTYPE=RECTANGLE, APERTURE={.26,.07,.26,.07}, ANGLE := kT10.BHZ017, TILT := tilt.T10.BHZ017;
 T10.BHZ027                                        : T10_MBXHFHWP    , APERTYPE=RECTANGLE, APERTURE={.26,.07,.26,.07}, ANGLE := kT10.BHZ027, TILT := tilt.T10.BHZ027;
 T10.DHZ040                                        : T10_MCXCFHWP    , APERTYPE=RECTANGLE, APERTURE={.1,.075,.1,.075}, TILT := tilt.T10.DHZ040, HKICK := kT10.DHZ040;
 T10.QFN009                                        : T10_MQNDCTWP    , APERTYPE=CIRCLE, APERTURE={.0455,.0455,.0455,.0455}, K1 := kT10.QFN009;
 T10.QFN014                                        : T10_MQNDCTWP    , APERTYPE=CIRCLE, APERTURE={.0455,.0455,.0455,.0455}, K1 := kT10.QFN014;
 T10.QFN020                                        : T10_MQNEGTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kT10.QFN020;
 T10.QFN023                                        : T10_MQNEGTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kT10.QFN023;
 T10.QDN012                                        : T10_MQNEL8WP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kT10.QDN012;
 T10.QDN030                                        : T10_MQNEVTWP    , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1}, K1 := kT10.QDN030;
 T10.QDN038                                        : T10_MQNEVTWP    , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1}, K1 := kT10.QDN038;
 T10.QFN035                                        : T10_MQNFKTWP    , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1}, K1 := kT10.QFN035;
 T10.TBS019                                        : T10_OMK;
 T10.EXP01                                         : T10_OMK;
 T10.EXP02                                         : T10_OMK;
 T10.EXP03                                         : T10_OMK;
 T10.EXP04                                         : T10_OMK;
 T10.TDE052                                        : T10_OMK;
 F63.TMMTV009                                      : T10_TMMTV;
 T10.XCET040                                       : T10_XCET_003;
 T10.XCET043                                       : T10_XCET_004;
 T10.XCHV022                                       : T10_XCHV_001;
 T10.XCSH013                                       : T10_XCSH_001;
 T10.XCSV010                                       : T10_XCSV_001;
 VXW                                               : T10_XVWAD001    , APERTYPE=CIRCLE, APERTURE={.078,.078,.078,.078};
 VXWP                                              : T10_XVWAT004    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXWP                                              : T10_XVWAT004    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXW                                               : T10_XVWAU002    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

T10 : SEQUENCE, refer = centre,         L = 52.653;
 F63.TMMTV009                  : F63.TMMTV009                , at = .1           , slot_id = 53151725;
 T10.XVW005                    : VXW                         , at = 5.232        , slot_id = 57954674;
 T10.JSAAC007                  : T10_OMK                     , at = 6.88641      , slot_id = 58195118;
 T10.MQNDC009                  : T10.QFN009                  , at = 8.97         , slot_id = 53609595;
 T10.XCSV010                   : T10.XCSV010                 , at = 10.144       , slot_id = 53609626;
 T10.MQNEL012                  : T10.QDN012                  , at = 11.77        , slot_id = 53609649;
 T10.XCSH013                   : T10.XCSH013                 , at = 13.145       , slot_id = 53609827;
 T10.MQNDC014                  : T10.QFN014                  , at = 14.32        , slot_id = 53611293;
 T10.MBXHF017                  : T10.BHZ017                  , at = 17.07        , slot_id = 53611322;
 T10.TBS019                    : T10.TBS019                  , at = 19.388       , slot_id = 53611352;
 T10.MQNEG020                  : T10.QFN020                  , at = 20.47        , slot_id = 53611375;
 T10.XVW021                    : VXWP                        , at = 21.12309     , slot_id = 57955161;
 T10.XVW022                    : VXWP                        , at = 21.47009     , slot_id = 57955244;
 T10.XCHV022                   : T10.XCHV022                 , at = 22.094       , slot_id = 53611415;
 T10.MQNEG023                  : T10.QFN023                  , at = 23.27        , slot_id = 53611444;
 T10.MBXHF027                  : T10.BHZ027                  , at = 26.67        , slot_id = 53611473;
 T10.MQNEV030                  : T10.QDN030                  , at = 30.189       , slot_id = 53611502;
 T10.MBXGF032                  : T10.BVT032                  , at = 32.07        , slot_id = 53611532;
 T10.MQNFK035                  : T10.QFN035                  , at = 34.501       , slot_id = 53611562;
 T10.XVW036                    : VXW                         , at = 35.8146      , slot_id = 57955484;
 T10.XSCI036                   : T10.XSCI036                 , at = 35.921       , slot_id = 53611592;
 T10.XCETW037                  : T10_XCETWAA001              , at = 36.11515     , slot_id = 60081853;
 T10.MQNEV038                  : T10.QDN038                  , at = 37.52        , slot_id = 53611627;
 T10.XCET040                   : T10.XCET040                 , at = 39.454       , slot_id = 53302587;
 T10.XCETW040                  : T10_XCETWAAP01              , at = 39.66385     , slot_id = 60081877;
 T10.MCXCF040                  : T10.DHZ040                  , at = 40.217       , slot_id = 53611657;
 T10.XCETW041                  : T10_XCETWAM003              , at = 40.6778      , slot_id = 60081974;
 T10.XCET043                   : T10.XCET043                 , at = 43.64        , slot_id = 53302602;
 T10.XCETW044                  : T10_XCETWAMP03              , at = 43.8492      , slot_id = 60082044;
 T10.XSCI044                   : T10.XSCI044                 , at = 44.09        , slot_id = 53611694;
 T10.XBPF045                   : T10.XBPF045                 , at = 44.566       , slot_id = 53611723;
 T10.EXP045                    : T10.EXP01                   , at = 44.9305      , slot_id = 57343247;
 T10.EXP047                    : T10.EXP02                   , at = 46.9305      , slot_id = 57343270;
 T10.EXP049                    : T10.EXP03                   , at = 48.9305      , slot_id = 57343293;
 T10.EXP051                    : T10.EXP04                   , at = 50.9305      , slot_id = 57343316;
 T10.TDE052                    : T10.TDE052                  , at = 52.6535      , slot_id = 57123591;
ENDSEQUENCE;

return;