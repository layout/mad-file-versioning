/************************************************************************************************************************************************************************
*
* F61 version LS2 in MAD X SEQUENCE format
* Generated the 11-FEB-2025 02:10:22 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.F61_BCGAA                    := 0.06;
l.F61_BCTFV                    := 0;
l.F61_BTVMC100                 := 0.306;
l.F61_MBXHDCWP                 := 2.5;
l.F61_MCXCEHWP                 := 0.2;
l.F61_MQNCL8WP                 := 0.74;
l.F61_MQNEFTWP                 := 1.2;
l.F61_MQNEGTWP                 := 0.8;
l.F61_MQNEL8WP                 := 1.2;
l.F61_OMK                      := 0;
l.F61_XSEC_L01                 := 0.24;
l.F61_XVWAR001                 := 0.0001;
l.F61_XVWAS003                 := 0.0002;
l.F61_XVWTB001                 := 0.0001;

//---------------------- CORRECTOR      ---------------------------------------------
F61_MCXCEHWP   : CORRECTOR   , L := l.F61_MCXCEHWP;      ! Corrector magnet CR200
//---------------------- INSTRUMENT     ---------------------------------------------
F61_XVWAR001   : INSTRUMENT  , L := l.F61_XVWAR001;      ! X Vacuum Window Aluminium with transition + PP DN40, DN195/159, DN139x102X0.2
F61_XVWAS003   : INSTRUMENT  , L := l.F61_XVWAS003;      ! X Vacuum Window Aluminium + pumping port DN40, DN159x102X0.2
F61_XVWTB001   : INSTRUMENT  , L := l.F61_XVWTB001;      ! X Vacuum Window Titanium [th=0.1], Tube DE 219, CF 266, aperture 215, [L=XX]
//---------------------- MARKER         ---------------------------------------------
F61_OMK        : MARKER      , L := l.F61_OMK;           ! F61 markers
//---------------------- MONITOR        ---------------------------------------------
F61_BCGAA      : MONITOR     , L := l.F61_BCGAA;         ! Beam Current Measurement, Gas, type AA
F61_BCTFV      : MONITOR     , L := l.F61_BCTFV;         ! Fast Beam Current Transformer, Type V
F61_BTVMC100   : MONITOR     , L := l.F61_BTVMC100;      ! Beam observation TV, with Magnetic Coupling, screen 100
F61_XSEC_L01   : MONITOR     , L := l.F61_XSEC_L01;      ! Ensemble: Secondary Emission Chamber et Support HXBEA - Long Version
//---------------------- QUADRUPOLE     ---------------------------------------------
F61_MQNCL8WP   : QUADRUPOLE  , L := l.F61_MQNCL8WP;      ! Quadrupole magnet, type Q74 laminé
F61_MQNEFTWP   : QUADRUPOLE  , L := l.F61_MQNEFTWP;      ! Quadrupole magnet, type QFL, 1.2m
F61_MQNEGTWP   : QUADRUPOLE  , L := l.F61_MQNEGTWP;      ! Quadrupole magnet, type QFS, 0.8m
F61_MQNEL8WP   : QUADRUPOLE  , L := l.F61_MQNEL8WP;      ! Quadrupole magnet, type Q120 laminated, 1.2 m
//---------------------- SBEND          ---------------------------------------------
F61_MBXHDCWP   : SBEND       , L := l.F61_MBXHDCWP;      ! Bending Magnet, H or V, type HB2, 2.5m gap 80mm

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kF61.BHZ025                    := -0.044108;
kF61.BHZ033                    := -0;
kF61.DHZ013                    := 0;
tilt.F61.BHZ025                := -0;
tilt.F61.BHZ033                := -0;
tilt.F61.DHZ013                := -0;
tilt.F61.DVT015                := -0;
kF61.DVT015                    := 0;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 F61.BCGAA023                                      : F61_BCGAA;
 F61.BCTF022                                       : F61_BCTFV;
 F61.BTV012                                        : F61_BTVMC100;
 F61.BHZ025                                        : F61_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kF61.BHZ025, TILT := tilt.F61.BHZ025;
 F61.BHZ033                                        : F61_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kF61.BHZ033, TILT := tilt.F61.BHZ033;
 F61.DHZ013                                        : F61_MCXCEHWP    , TILT := tilt.F61.DHZ013, HKICK := kF61.DHZ013;
 F61.DVT015                                        : F61_MCXCEHWP    , TILT := tilt.F61.DVT015, VKICK := kF61.DVT015;
 F61.QFN007                                        : F61_MQNCL8WP    , K1 := KF61.QFN007;
 F61.QFN021                                        : F61_MQNEFTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kF61.QFN021;
 F61.QDN030                                        : F61_MQNEGTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kF61.QDN030;
 F61.QDN014                                        : F61_MQNEL8WP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kF61.QDN014;
 F61.TBS016                                        : F61_OMK;
 F61.TBS017                                        : F61_OMK;
 F61.TBS018                                        : F61_OMK;
 F61.TBS019                                        : F61_OMK;
 F61.TBS020                                        : F61_OMK;
 F61.XSEC023                                       : F61_XSEC_L01;
 VXWP                                              : F61_XVWAR001    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXWP                                              : F61_XVWAS003    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXWT                                              : F61_XVWTB001;

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

F61 : SEQUENCE, refer = centre,         L = 33.762;
 F61.MQNCL007                  : F61.QFN007                  , at = 6.81016      , slot_id = 52943159;
 F61.XVW012                    : VXWT                        , at = 12.10071     , slot_id = 57951623;
 F61.BTV012                    : F61.BTV012                  , at = 12.26021     , slot_id = 53123002;
 F61.MCXCE013                  : F61.DHZ013                  , at = 12.71321     , slot_id = 52943238;
 F61.MQNEL014                  : F61.QDN014                  , at = 13.94821     , slot_id = 53000241;
 F61.MCXCE015                  : F61.DVT015                  , at = 15.18321     , slot_id = 53000260;
 F61.TBS016                    : F61.TBS016                  , at = 16.53351     , slot_id = 53000276;
 F61.TBS017                    : F61.TBS017                  , at = 17.30061     , slot_id = 53000292;
 F61.TBS018                    : F61.TBS018                  , at = 18.06771     , slot_id = 53000307;
 F61.TBS019                    : F61.TBS019                  , at = 18.83481     , slot_id = 53000322;
 F61.TBS020                    : F61.TBS020                  , at = 19.60191     , slot_id = 53000337;
 F61.MQNEF021                  : F61.QFN021                  , at = 20.89721     , slot_id = 53000352;
 F61.BCTFV022                  : F61.BCTF022                 , at = 22.08721     , slot_id = 53000546;
 F61.XVW022                    : VXWP                        , at = 22.35131     , slot_id = 57951880;
 F61.BCGAA023                  : F61.BCGAA023                , at = 22.54221     , slot_id = 53123033;
 F61.XSEC023                   : F61.XSEC023                 , at = 22.73221     , slot_id = 53123049;
 F61.MBXHD025                  : F61.BHZ025                  , at = 24.52221     , slot_id = 53123070;
 F61.XVW026                    : VXWP                        , at = 26.17771     , slot_id = 57951919;
 F61.MQNEG030                  : F61.QDN030                  , at = 30.2619      , slot_id = 53123092;
 F61.MBXHD033                  : F61.BHZ033                  , at = 32.51191     , slot_id = 53123107;
ENDSEQUENCE;

return;