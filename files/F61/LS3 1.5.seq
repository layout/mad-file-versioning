

  /**********************************************************************************
  *
  * F61 version (draft) LS3 1.5 in MAD X SEQUENCE format
  * Generated the 21-FEB-2023 18:16:49 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.F61_BCGAA                    := 0.06;
l.F61_BCTF                     := 0;
l.F61_BTVMC100                 := 0.306;
l.F61_MBXHDCWP                 := 2.5;
l.F61_MCXCEHWP                 := 0.2;
l.F61_MQNCL8WP                 := 0.74;
l.F61_MQNEFTWP                 := 1.2;
l.F61_MQNEGTWP                 := 0.8;
l.F61_MQNEL8WP                 := 1.2;
l.F61_OMK                      := 0;
l.F61_XSEC_L01                 := 0.24;

//---------------------- CORRECTOR      ---------------------------------------------
F61_MCXCEHWP   : CORRECTOR   , L := l.F61_MCXCEHWP;      ! Corrector magnet CR200
//---------------------- MARKER         ---------------------------------------------
F61_OMK        : MARKER      , L := l.F61_OMK;           ! F61 markers
//---------------------- MONITOR        ---------------------------------------------
F61_BCGAA      : MONITOR     , L := l.F61_BCGAA;         ! Beam Current Measurement, Gas, type AA
F61_BCTF       : MONITOR     , L := l.F61_BCTF;          ! Fast Beam Current Transformers
F61_BTVMC100   : MONITOR     , L := l.F61_BTVMC100;      ! Beam observation TV, with Magnetic Coupling, screen 100
F61_XSEC_L01   : MONITOR     , L := l.F61_XSEC_L01;      ! Ensemble: Secondary Emission Chamber et Support HXBEA - Long Version
//---------------------- QUADRUPOLE     ---------------------------------------------
F61_MQNCL8WP   : QUADRUPOLE  , L := l.F61_MQNCL8WP;      ! Quadrupole magnet, type Q74 laminé
F61_MQNEFTWP   : QUADRUPOLE  , L := l.F61_MQNEFTWP;      ! Quadrupole magnet, type QFL, 1.2m
F61_MQNEGTWP   : QUADRUPOLE  , L := l.F61_MQNEGTWP;      ! Quadrupole magnet, type QFS, 0.8m
F61_MQNEL8WP   : QUADRUPOLE  , L := l.F61_MQNEL8WP;      ! Quadrupole magnet, type Q120 laminated, 1.2 m
//---------------------- SBEND          ---------------------------------------------
F61_MBXHDCWP   : SBEND       , L := l.F61_MBXHDCWP;      ! Bending Magnet, H or V, type HB2, 2.5m gap 80mm


/************************************************************************************/
/*                       F61 SEQUENCE                                               */
/************************************************************************************/

F61 : SEQUENCE, refer = centre,         L = 33.762;
 F61.MQNCL007                  : F61_MQNCL8WP    , at = 6.81016      , slot_id = 52943159;
 F61.BTV012                    : F61_BTVMC100    , at = 12.26021     , slot_id = 53123002;
 F61.MCXCE013                  : F61_MCXCEHWP    , at = 12.71321     , slot_id = 52943238;
 F61.MQNEL014                  : F61_MQNEL8WP    , at = 13.94821     , slot_id = 53000241;
 F61.MCXCE015                  : F61_MCXCEHWP    , at = 15.18321     , slot_id = 53000260;
 F61.TBS016                    : F61_OMK         , at = 16.53351     , slot_id = 53000276;
 F61.TBS017                    : F61_OMK         , at = 17.30061     , slot_id = 53000292;
 F61.TBS018                    : F61_OMK         , at = 18.06771     , slot_id = 53000307;
 F61.TBS019                    : F61_OMK         , at = 18.83481     , slot_id = 53000322;
 F61.TBS020                    : F61_OMK         , at = 19.60191     , slot_id = 53000337;
 F61.MQNEF021                  : F61_MQNEFTWP    , at = 20.89721     , slot_id = 53000352;
 F61.BCTF022                   : F61_BCTF        , at = 22.1196      , slot_id = 53000546;
 F61.BCGAA023                  : F61_BCGAA       , at = 22.54221     , slot_id = 53123033;
 F61.XSEC023                   : F61_XSEC_L01    , at = 22.73221     , slot_id = 53123049;
 F61.MBXHD025                  : F61_MBXHDCWP    , at = 24.52221     , slot_id = 53123070;
 F61.MQNEG030                  : F61_MQNEGTWP    , at = 30.2619      , slot_id = 53123092;
 F61.MBXHD033                  : F61_MBXHDCWP    , at = 32.51191     , slot_id = 53123107;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTH CONSTANTS                                         */
/************************************************************************************/

kF61.BHZ025                    := -0.044108;
kF61.BHZ033                    := -0;
kF61.DHZ013                    := 0;
tilt.F61.BHZ025                := -0;
tilt.F61.BHZ033                := -0;
tilt.F61.DHZ013                := -0;
tilt.F61.DVT015                := -0;
kF61.DVT015                    := 0;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/
  F61.MQNCL007,              K1 := KF61.QFN007;
  F61.MCXCE013,              TILT := tilt.F61.DHZ013, HKICK := kF61.DHZ013;
  F61.MQNEL014,              K1 := kF61.QDN014;
  F61.MCXCE015,              TILT := tilt.F61.DVT015, VKICK := kF61.DVT015;
  F61.MQNEF021,              K1 := kF61.QFN021;
  F61.MBXHD025,              ANGLE := kF61.BHZ025, TILT := tilt.F61.BHZ025;
  F61.MQNEG030,              K1 := kF61.QDN030;
  F61.MBXHD033,              ANGLE := kF61.BHZ033, TILT := tilt.F61.BHZ033;
return;