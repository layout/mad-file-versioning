/************************************************************************************************************************************************************************
*
* H8 version EYETS 2023-2024 in MAD X SEQUENCE format
* Generated the 11-MAR-2025 02:15:52 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.H8_MBMORISC                  := 3.5;
l.H8_MBNV_HWP                  := 5;
l.H8_MBXHCCWP                  := 2.5;
l.H8_MCXCAHWC                  := 0.4;
l.H8_MQNFBTWC                  := 2;
l.H8_MTN__HWP                  := 3.6;
l.H8_OMK                       := 0;
l.H8_QNL__8WP                  := 2.99;
l.H8_QSL__8WP                  := 3;
l.H8_QWL__8WP                  := 3;
l.H8_TBID                      := 0.25;
l.H8_TCMAA                     := 0.4;
l.H8_XCETWAM003                := 0.0004;
l.H8_XCET_001                  := 0;
l.H8_XCHV_001                  := 1;
l.H8_XCIOB002                  := 1;
l.H8_XCON_001                  := 0.3;
l.H8_XCON_P01                  := 0.1;
l.H8_XCRH                      := 0.45;
l.H8_XCRV                      := 0.45;
l.H8_XCSH_001                  := 1;
l.H8_XCSV_001                  := 1;
l.H8_XDWC                      := 0.06;
l.H8_XEMC_001                  := 0.8;
l.H8_XFF__001                  := 0.276;
l.H8_XSCI                      := 0.1;
l.H8_XTAX_005                  := 1.615;
l.H8_XTAX_006                  := 1.615;
l.H8_XTDV_001                  := 1.6;
l.H8_XVVSA001                  := 0.32;
l.H8_XVVSC001                  := 0.32;
l.H8_XVWAA001                  := 0.0001;
l.H8_XVWAB001                  := 0.0001;
l.H8_XVWAD001                  := 0.0001;
l.H8_XVWAF001                  := 0.0001;
l.H8_XVWAT004                  := 0.0002;
l.H8_XVWMF001                  := 0.00012;
l.H8_XWCA                      := 0.06;

//---------------------- COLLIMATOR     ---------------------------------------------
H8_TCMAA       : COLLIMATOR  , L := l.H8_TCMAA;          ! Collimation mask type A
H8_XCHV_001    : COLLIMATOR  , L := l.H8_XCHV_001;       ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
H8_XCIOB002    : COLLIMATOR  , L := l.H8_XCIOB002;       ! Converter IN OUT Block - Steel 1m
H8_XCON_001    : COLLIMATOR  , L := l.H8_XCON_001;       ! Converter H8 130 - Cu300-Pb3-Pb6 - Secondary Target 6 mm + 400 mm Lead + Aluminium, 12 mm Lead, 300 mm Copper
H8_XCON_P01    : COLLIMATOR  , L := l.H8_XCON_P01;       ! Converter Pb 4-8-18 mm
H8_XCRH        : COLLIMATOR  , L := l.H8_XCRH;           ! Micro-collimator Horizontal
H8_XCRV        : COLLIMATOR  , L := l.H8_XCRV;           ! Micro-collimator Vertical
H8_XCSH_001    : COLLIMATOR  , L := l.H8_XCSH_001;       ! SPS Collimator a fente horizontal (design 1970)
H8_XCSV_001    : COLLIMATOR  , L := l.H8_XCSV_001;       ! SPS Collimator a fente vertical (design 1970)
H8_XTAX_005    : COLLIMATOR  , L := l.H8_XTAX_005;       ! Target Absorber Type 005
H8_XTAX_006    : COLLIMATOR  , L := l.H8_XTAX_006;       ! Target Absorber Type 006
H8_XTDV_001    : COLLIMATOR  , L := l.H8_XTDV_001;       ! Mobile Dump Vertical (Hydraulic)
//---------------------- INSTRUMENT     ---------------------------------------------
H8_XCETWAM003  : INSTRUMENT  , L := l.H8_XCETWAM003;     ! XCET Beam Window DN159 - Mylar/Polyethylene  - 0.25/0.15 mm thick
H8_XCET_001    : INSTRUMENT  , L := l.H8_XCET_001;       ! Cherenkov Counter DN159 - Mylar Window
H8_XEMC_001    : INSTRUMENT  , L := l.H8_XEMC_001;       ! ElectroMagnetic Calorimeter
H8_XVVSA001    : INSTRUMENT  , L := l.H8_XVVSA001;       ! X Vacuum Sector ElectroValve DN159x320mm (remote control)
H8_XVVSC001    : INSTRUMENT  , L := l.H8_XVVSC001;       ! X Vacuum Sector Valve DN159x320mm (manual)
H8_XVWAA001    : INSTRUMENT  , L := l.H8_XVWAA001;       ! X Vacuum Window, Aluminum [th=0.1], Tube DE 159, Flat Flange 192, aperture 120 + pumping port DN40, [L=100]
H8_XVWAB001    : INSTRUMENT  , L := l.H8_XVWAB001;       ! X Vacuum Window Aluminium [th=0.2], Flat Flanges 390, aperture EL350x60, [L=60] (MT)
H8_XVWAD001    : INSTRUMENT  , L := l.H8_XVWAD001;       ! X Vacuum Window Aluminum [th=0.1], Tube DE 159, Flat Flange 192, aperture 120, [L=70] (VXW)
H8_XVWAF001    : INSTRUMENT  , L := l.H8_XVWAF001;       ! X Vacuum Window Aluminium EL900X60X0.2 + pumping port
H8_XVWAT004    : INSTRUMENT  , L := l.H8_XVWAT004;       ! X Vacuum Window Mylar + pumping port DN159X99x0.2
H8_XVWMF001    : INSTRUMENT  , L := l.H8_XVWMF001;       ! X Vacuum Window, Mylar [th=0.125], Tube DE 159, Flat Flange 192, aperture 120 + pumping port DN40, [L=100]
//---------------------- KICKER         ---------------------------------------------
H8_MCXCAHWC    : KICKER      , L := l.H8_MCXCAHWC;       ! Corrector magnet, H or V, type MDX
//---------------------- MARKER         ---------------------------------------------
H8_OMK         : MARKER      , L := l.H8_OMK;            ! H8 markers
//---------------------- MONITOR        ---------------------------------------------
H8_TBID        : MONITOR     , L := l.H8_TBID;           ! target beam instrumentation, downstream
H8_XDWC        : MONITOR     , L := l.H8_XDWC;           ! Delay Wire Chamber (Beam Profile Monitors)
H8_XFF__001    : MONITOR     , L := l.H8_XFF__001;       ! Filament Scintillator Profile Monitor
H8_XSCI        : MONITOR     , L := l.H8_XSCI;           ! Ensemble: Cadre et Scintillateur BXSCI
H8_XWCA        : MONITOR     , L := l.H8_XWCA;           ! Multi Wire Proportional Chamber Assembly
//---------------------- QUADRUPOLE     ---------------------------------------------
H8_MQNFBTWC    : QUADRUPOLE  , L := l.H8_MQNFBTWC;       ! Quadrupole magnet, type Q200, 2m
H8_QNL__8WP    : QUADRUPOLE  , L := l.H8_QNL__8WP;       ! Quadrupole, secondary beams, type north area - dimensions according to drawing EDMS 1973665
H8_QSL__8WP    : QUADRUPOLE  , L := l.H8_QSL__8WP;       ! Quadrupole, slim, long, - Same magnet type SPQSLD_
H8_QWL__8WP    : QUADRUPOLE  , L := l.H8_QWL__8WP;       ! Quadrupole, Secondary Beams, West Area Type - dimensions according to drawing EDMS 350768 
//---------------------- RBEND          ---------------------------------------------
H8_MBMORISC    : RBEND       , L := l.H8_MBMORISC;       ! MORPURGO, sector coil
H8_MBNV_HWP    : RBEND       , L := l.H8_MBNV_HWP;       ! Bending magnet, secondary beams, vertical, north area - Mechanical dimensions from NORMA
H8_MBXHCCWP    : RBEND       , L := l.H8_MBXHCCWP;       ! Bending Magnet, H or V, type HB1, 2.5m gap 80mm
H8_MTN__HWP    : RBEND       , L := l.H8_MTN__HWP;       ! Bending magnet, Target N

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kMBH.X0400003_BEND3T           := -0.007;
kMBH.X0400007_BEND3T           := -0.007;
kMBH.X0420412_BEND5H           := -0.0018;
kMBH.X0420416_BEND6H           := -0.0018;
kMBV.X0420049_BEND1V           := -0.0068356;
kMBV.X0420055_BEND2V           := -0.0068356;
kMBV.X0420061_BEND2V           := -0.0068356;
kMBV.X0420067_BEND1V           := -0.0068356;
kMBV.X0420073_BEND1V           := -0.0068356;
kMBV.X0420079_BEND2V           := -0.0068356;
kMBV.X0420325_BEND3V           := 0.0068482;
kMBV.X0420330_BEND4V           := 0.0068482;
kMBV.X0420336_BEND4V           := 0.0068482;
kMBV.X0420343_BEND3V           := 0.0068482;
kMBV.X0420354_BEND4V           := 0.0068482;
tilt.MBH.X0400003_BEND3T       := -0.0002617993877991494;
tilt.MBH.X0400007_BEND3T       := -0.00026432578132272927;
tilt.MBH.X0420412_BEND5H       := -0.0002668648932781924;
tilt.MBH.X0420416_BEND6H       := -0.00026765596229654616;
tilt.MBV.X0420049_BEND1V       := 1.5705294618538286;
tilt.MBV.X0420055_BEND2V       := 1.5705294673195003;
tilt.MBV.X0420061_BEND2V       := 1.5705294850190086;
tilt.MBV.X0420067_BEND1V       := 1.5705295149545797;
tilt.MBV.X0420073_BEND1V       := 1.570529557129584;
tilt.MBV.X0420079_BEND2V       := 1.5705296115485379;
tilt.MBV.X0420325_BEND3V       := 1.570529678217105;
tilt.MBV.X0420330_BEND4V       := 1.570529611453975;
tilt.MBV.X0420336_BEND4V       := 1.5705295569799604;
tilt.MBV.X0420343_BEND3V       := 1.570529514790524;
tilt.MBV.X0420354_BEND4V       := 1.5705294672530068;
tilt.MCBH.X0420094_TRIM11H     := -4.84918999251519e-06;
tilt.MCBH.X0420133_TRIM1H      := -0.0002666485777916646;
tilt.MCBH.X0420203_TRIM3H      := -0.0002666485777916646;
tilt.MCBH.X0420318_TRIM5H      := -0.0002666485777916646;
tilt.MCBH.X0420462_TRIM7H      := -0.00026844787827568574;
tilt.MCBH.X0420528_TRIM9H      := -0.00026844787827568574;
tilt.MCBV.X0420134_TRIM2V      := -0.0002666485777916646;
tilt.MCBV.X0420407_TRIM6V      := 1.5705294619016184;
tilt.MCBV.X0420463_TRIM8V      := 1.5705278789166208;
tilt.MCBV.X0420529_TRIM10V     := 1.5705278789166208;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 MBXH.X0420553_MORPURGO                            : H8_MBMORISC;
 MBV.X0420049_BEND1V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420049_BEND1V, TILT := tilt.MBV.X0420049_BEND1V;
 MBV.X0420055_BEND2V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420055_BEND2V, TILT := tilt.MBV.X0420055_BEND2V;
 MBV.X0420061_BEND2V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420061_BEND2V, TILT := tilt.MBV.X0420061_BEND2V;
 MBV.X0420067_BEND1V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420067_BEND1V, TILT := tilt.MBV.X0420067_BEND1V;
 MBV.X0420073_BEND1V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420073_BEND1V, TILT := tilt.MBV.X0420073_BEND1V;
 MBV.X0420079_BEND2V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420079_BEND2V, TILT := tilt.MBV.X0420079_BEND2V;
 MBV.X0420325_BEND3V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420325_BEND3V, TILT := tilt.MBV.X0420325_BEND3V;
 MBV.X0420330_BEND4V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420330_BEND4V, TILT := tilt.MBV.X0420330_BEND4V;
 MBV.X0420336_BEND4V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420336_BEND4V, TILT := tilt.MBV.X0420336_BEND4V;
 MBV.X0420343_BEND3V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420343_BEND3V, TILT := tilt.MBV.X0420343_BEND3V;
 MBV.X0420349_BEND3V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420349_BEND3V;
 MBV.X0420354_BEND4V                               : H8_MBNV_HWP     , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBV.X0420354_BEND4V, TILT := tilt.MBV.X0420354_BEND4V;
 MBH.X0420412_BEND5H                               : H8_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0420412_BEND5H, TILT := tilt.MBH.X0420412_BEND5H;
 MBH.X0420416_BEND6H                               : H8_MBXHCCWP     , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBH.X0420416_BEND6H, TILT := tilt.MBH.X0420416_BEND6H;
 MCBH.X0420094_TRIM11H                             : H8_MCXCAHWC     , TILT := tilt.MCBH.X0420094_TRIM11H, HKICK := kMCBH.X0420094_TRIM11H;
 MCBH.X0420133_TRIM1H                              : H8_MCXCAHWC     , TILT := tilt.MCBH.X0420133_TRIM1H, HKICK := kMCBH.X0420133_TRIM1H;
 MCBV.X0420134_TRIM2V                              : H8_MCXCAHWC     , TILT := tilt.MCBV.X0420134_TRIM2V, VKICK := kMCBV.X0420134_TRIM2V;
 MCBH.X0420203_TRIM3H                              : H8_MCXCAHWC     , TILT := tilt.MCBH.X0420203_TRIM3H, HKICK := kMCBH.X0420203_TRIM3H;
 MCBH.X0420318_TRIM5H                              : H8_MCXCAHWC     , TILT := tilt.MCBH.X0420318_TRIM5H, HKICK := kMCBH.X0420318_TRIM5H;
 MCBV.X0420407_TRIM6V                              : H8_MCXCAHWC     , TILT := tilt.MCBV.X0420407_TRIM6V, VKICK := kMCBV.X0420407_TRIM6V;
 MCBH.X0420462_TRIM7H                              : H8_MCXCAHWC     , TILT := tilt.MCBH.X0420462_TRIM7H, HKICK := kMCBH.X0420462_TRIM7H;
 MCBV.X0420463_TRIM8V                              : H8_MCXCAHWC     , TILT := tilt.MCBV.X0420463_TRIM8V, VKICK := kMCBV.X0420463_TRIM8V;
 MCBH.X0420528_TRIM9H                              : H8_MCXCAHWC     , TILT := tilt.MCBH.X0420528_TRIM9H, HKICK := kMCBH.X0420528_TRIM9H;
 MCBV.X0420529_TRIM10V                             : H8_MCXCAHWC     , TILT := tilt.MCBV.X0420529_TRIM10V, VKICK := kMCBV.X0420529_TRIM10V;
 MQD.X0420125_QUAD7D                               : H8_MQNFBTWC     , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1};
 MQD.X0420141_QUAD21D                              : H8_MQNFBTWC     , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1};
 MQD.X0420263_QUAD21D                              : H8_MQNFBTWC     , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1};
 MQD.X0420279_QUAD21D                              : H8_MQNFBTWC     , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1};
 MBH.X0400003_BEND3T                               : H8_MTN__HWP     , APERTYPE=RECTANGLE, APERTURE={.175,.03,.175,.03}, ANGLE := kMBH.X0400003_BEND3T, TILT := tilt.MBH.X0400003_BEND3T;
 MBH.X0400007_BEND3T                               : H8_MTN__HWP     , APERTYPE=RECTANGLE, APERTURE={.175,.03,.175,.03}, ANGLE := kMBH.X0400007_BEND3T, TILT := tilt.MBH.X0400007_BEND3T;
 ATLAS_TILECAL.X0420559                            : H8_OMK;
 ATLAS_LARGAT.X0420578                             : H8_OMK;
 TOTEM.X0420602                                    : H8_OMK;
 DUMP.X0420615                                     : H8_OMK;
 MQD.X0420036_QUAD3D                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420036_QUAD3D;
 MQD.X0420045_QUAD4D                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420045_QUAD4D;
 MQD.X0420083_QUAD5D                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420083_QUAD5D;
 MQF.X0420096_QUAD6F                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420096_QUAD6F;
 MQF.X0420170_QUAD8F                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420170_QUAD8F;
 MQD.X0420182_QUAD9D                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420182_QUAD9D;
 MQD.X0420221_QUAD9D                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420221_QUAD9D;
 MQF.X0420234_QUAD8F                               : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420234_QUAD8F;
 MQF.X0420308_QUAD10F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420308_QUAD10F;
 MQD.X0420320_QUAD11D                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420320_QUAD11D;
 MQD.X0420359_QUAD12D                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420359_QUAD12D;
 MQF.X0420371_QUAD10F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420371_QUAD10F;
 MQF.X0420374_QUAD13F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420374_QUAD13F;
 MQF.X0420427_QUAD14F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420427_QUAD14F;
 MQF.X0420457_QUAD16F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420457_QUAD16F;
 MQF.X0420460_QUAD16F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04};
 MQF.X0420495_QUAD17F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420495_QUAD17F;
 MQF.X0420498_QUAD17F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420498_QUAD17F;
 MQD.X0420506_QUAD18D                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X0420506_QUAD18D;
 MQF.X0420526_QUAD19F                              : H8_QNL__8WP     , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X0420526_QUAD19F;
 MQF.X0420026_QUAD1F                               : H8_QSL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQF.X0420026_QUAD1F;
 MQF.X0420033_QUAD2F                               : H8_QSL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQF.X0420033_QUAD2F;
 MQD.X0420444_QUAD15D                              : H8_QWL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQD.X0420444_QUAD15D;
 MQD.X0420447_QUAD15D                              : H8_QWL__8WP     , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05};
 XCHV05.X0420202                                   : H8_XCHV_001     , APERTYPE=RECTANGLE, APERTURE={.020001,.020001,.02,.02};
 XCRH.X0420201_COLL14RH                            : H8_XCRH;
 XCRV.X0420201_COLL13RV                            : H8_XCRV;
 XCH01.X0420064                                    : H8_XCSH_001;
 XSCI ATLAS_1                                      : H8_XSCI;
 XSCI ATLAS_2                                      : H8_XSCI;
 XVSV.042.107                                      : H8_XVVSA001     , APERTYPE=CIRCLE, APERTURE={.1305,.1305,.1305,.1305};
 VXWT                                              : H8_XVWAB001     , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXWP                                              : H8_XVWAT004;
 VXWP                                              : H8_XVWAT004;

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

H8 : SEQUENCE, refer = centre,          L = 616.758;
 TBACA.X0400000                : H8_OMK                      , at = 0            , slot_id = 56964667;
 TBID.241150                   : H8_TBID                     , at = .325         , slot_id = 47534194;
 TCMAA.X0400001                : H8_TCMAA                    , at = .85          , slot_id = 56992277;
 XVW.X0400001                  : VXWT                        , at = 1.129        , slot_id = 57310366;
 MTN.X0400003                  : MBH.X0400003_BEND3T         , at = 3.15         , slot_id = 56992202;
 MTN.X0400007                  : MBH.X0400007_BEND3T         , at = 7.35         , slot_id = 56992225;
 XVW.X0420009                  : H8_XVWAF001                 , at = 9.371        , slot_id = 62309773;
 XTAX.X0420018                 : H8_XTAX_005                 , at = 18.0075      , slot_id = 57111690;
 XTAX.X0420020                 : H8_XTAX_006                 , at = 19.6325      , slot_id = 57111699;
 XVW.X0420021                  : H8_XVWAD001                 , at = 20.477       , slot_id = 57602794;
 QSL.X0420026                  : MQF.X0420026_QUAD1F         , at = 25.94        , slot_id = 57444892;
 QSL.X0420033                  : MQF.X0420033_QUAD2F         , at = 32.92        , slot_id = 57420233;
 QNL.X0420036                  : MQD.X0420036_QUAD3D         , at = 36.35        , slot_id = 56048707;
 QNL.X0420045                  : MQD.X0420045_QUAD4D         , at = 44.75        , slot_id = 56048716;
 MBNV.X0420049                 : MBV.X0420049_BEND1V         , at = 49.295       , slot_id = 56266418;
 MBNV.X0420055                 : MBV.X0420055_BEND2V         , at = 54.955       , slot_id = 56266445;
 MBNV.X0420061                 : MBV.X0420061_BEND2V         , at = 60.615       , slot_id = 56266454;
 XCSH.X0420064                 : XCH01.X0420064              , at = 64.02        , slot_id = 56051887;
 MBNV.X0420067                 : MBV.X0420067_BEND1V         , at = 67.425       , slot_id = 56266427;
 MBNV.X0420073                 : MBV.X0420073_BEND1V         , at = 73.085       , slot_id = 56266436;
 MBNV.X0420079                 : MBV.X0420079_BEND2V         , at = 78.745       , slot_id = 56266463;
 QNL.X0420083                  : MQD.X0420083_QUAD5D         , at = 83.29        , slot_id = 56048725;
 MCXCA.X0420094                : MCBH.X0420094_TRIM11H       , at = 93.625       , slot_id = 56048349;
 QNL.X0420096                  : MQF.X0420096_QUAD6F         , at = 95.775       , slot_id = 56048734;
 XVVS.X0420104                 : XVSV.042.107                , at = 103.786      , slot_id = 57596774;
 MQNFB.X0420125                : MQD.X0420125_QUAD7D         , at = 124.89       , slot_id = 56048322;
 XCSH.X0420128                 : H8_XCSH_001                 , at = 128.13       , slot_id = 56051860;
 XVW.X0420129                  : H8_XVWAA001                 , at = 128.753      , slot_id = 57602758;
 XCON.X0420130                 : H8_XCON_001                 , at = 129.817      , slot_id = 60070162;
 XVW.X0420131                  : H8_XVWAA001                 , at = 131.373      , slot_id = 57602767;
 XCSV.X0420132                 : H8_XCSV_001                 , at = 131.923      , slot_id = 57015430;
 MCXCA.X0420133                : MCBH.X0420133_TRIM1H        , at = 132.968      , slot_id = 56048331;
 MCXCA.X0420134                : MCBV.X0420134_TRIM2V        , at = 133.738      , slot_id = 56048358;
 MQNFB.X0420141                : MQD.X0420141_QUAD21D        , at = 140.89       , slot_id = 56048295;
 QNL.X0420170                  : MQF.X0420170_QUAD8F         , at = 170.005      , slot_id = 56048743;
 QNL.X0420182                  : MQD.X0420182_QUAD9D         , at = 182.49       , slot_id = 56048761;
 XCSV.X0420185                 : H8_XCSV_001                 , at = 184.78       , slot_id = 57015453;
 XFFV.X0420196                 : H8_XFF__001                 , at = 197.766      , slot_id = 57703376;
 XFFH.X0420197                 : H8_XFF__001                 , at = 198.042      , slot_id = 57703399;
 XVW.X0420198                  : H8_XVWAA001                 , at = 198.23       , slot_id = 57602776;
 XWCA.X0420198                 : H8_XWCA                     , at = 198.34       , slot_id = 57703422;
 XSCI.X0420198                 : H8_XSCI                     , at = 198.42       , slot_id = 57847859;
 XCIO.X0420200                 : H8_XCIOB002                 , at = 199.137      , slot_id = 57247368;
 XVW.X0420199                  : H8_XVWAA001                 , at = 199.6975     , slot_id = 57602785;
 XCRV.X0420201                 : XCRV.X0420201_COLL13RV      , at = 200.035      , slot_id = 57671003, assembly_id= 56262653;
 XCRH.X0420201                 : XCRH.X0420201_COLL14RH      , at = 200.61       , slot_id = 57670967, assembly_id= 56262653;
 XCHV.X0420202                 : XCHV05.X0420202             , at = 201.76       , slot_id = 56051710;
 MCXCA.X0420203                : MCBH.X0420203_TRIM3H        , at = 202.77       , slot_id = 56048367;
 XFFH.X0420217                 : H8_XFF__001                 , at = 217.892      , slot_id = 57703468;
 QNL.X0420221                  : MQD.X0420221_QUAD9D         , at = 221.03       , slot_id = 56048770;
 QNL.X0420234                  : MQF.X0420234_QUAD8F         , at = 233.515      , slot_id = 56048752;
 MQNFB.X0420263                : MQD.X0420263_QUAD21D        , at = 262.63       , slot_id = 56048304;
 XFFV.X0420270                 : H8_XFF__001                 , at = 270.492      , slot_id = 57703491;
 XFFH.X0420271                 : H8_XFF__001                 , at = 270.768      , slot_id = 57703514;
 MQNFB.X0420279                : MQD.X0420279_QUAD21D        , at = 278.63       , slot_id = 56048313;
 QNL.X0420308                  : MQF.X0420308_QUAD10F        , at = 307.745      , slot_id = 56048872;
 MCXCA.X0420318                : MCBH.X0420318_TRIM5H        , at = 318.08       , slot_id = 56048376;
 QNL.X0420320                  : MQD.X0420320_QUAD11D        , at = 320.23       , slot_id = 56048890;
 MBNV.X0420325                 : MBV.X0420325_BEND3V         , at = 324.775      , slot_id = 56266503;
 MBNV.X0420330                 : MBV.X0420330_BEND4V         , at = 330.435      , slot_id = 56266521;
 MBNV.X0420336                 : MBV.X0420336_BEND4V         , at = 336.095      , slot_id = 56266530;
 XFFV.X0420338                 : H8_XFF__001                 , at = 339.063      , slot_id = 57703543;
 XFFH.X0420339                 : H8_XFF__001                 , at = 339.339      , slot_id = 57716279;
 MBNV.X0420343                 : MBV.X0420343_BEND3V         , at = 342.905      , slot_id = 56266481;
 MBNV.X0420349                 : MBV.X0420349_BEND3V         , at = 348.565      , slot_id = 56266490;
 MBNV.X0420354                 : MBV.X0420354_BEND4V         , at = 354.225      , slot_id = 56266541;
 QNL.X0420359                  : MQD.X0420359_QUAD12D        , at = 358.77       , slot_id = 56048899;
 QNL.X0420371                  : MQF.X0420371_QUAD10F        , at = 371.255      , slot_id = 56048881;
 QNL.X0420374                  : MQF.X0420374_QUAD13F        , at = 374.685      , slot_id = 56048908;
 XVW.X0420403                  : H8_XVWMF001                 , at = 403.713      , slot_id = 57602830;
 XWCA.X0420403                 : H8_XWCA                     , at = 403.9        , slot_id = 57716303;
 XSCI.X0420403                 : H8_XSCI                     , at = 403.98       , slot_id = 57848378;
 XVW.X0420404                  : H8_XVWMF001                 , at = 404.185      , slot_id = 57602839;
 XCSV.X0420404                 : H8_XCSV_001                 , at = 404.735      , slot_id = 57015466;
 XCHV.X0420406                 : H8_XCHV_001                 , at = 406.02       , slot_id = 56051719;
 MCXCA.X0420407                : MCBV.X0420407_TRIM6V        , at = 407.03       , slot_id = 56048385;
 XVW.X0420409                  : H8_XVWMF001                 , at = 409.065      , slot_id = 57602848;
 XSCI.X0420410                 : H8_XSCI                     , at = 409.26       , slot_id = 57851160;
 XCON.X0420411                 : H8_XCON_P01                 , at = 409.53       , slot_id = 57585239;
 XVW.X0420410                  : H8_XVWMF001                 , at = 409.761      , slot_id = 57602857;
 MBXHC.X0420412                : MBH.X0420412_BEND5H         , at = 411.46       , slot_id = 56048277;
 MBXHC.X0420416                : MBH.X0420416_BEND6H         , at = 415.91       , slot_id = 56048286;
 XFFV.X0420418                 : H8_XFF__001                 , at = 419.218      , slot_id = 57716327;
 XFFH.X0420419                 : H8_XFF__001                 , at = 419.494      , slot_id = 57716358;
 XVW.X0420419                  : H8_XVWMF001                 , at = 419.692      , slot_id = 57602888;
 XSCI.X0420420                 : H8_XSCI                     , at = 419.792      , slot_id = 57851196;
 XEMC.X0420420                 : H8_XEMC_001                 , at = 420.272      , slot_id = 57576809;
 XVW.X0420420                  : H8_XVWMF001                 , at = 421.477      , slot_id = 57602906;
 XCSH.X0420424                 : H8_XCSH_001                 , at = 424.28       , slot_id = 56051869;
 QNL.X0420427                  : MQF.X0420427_QUAD14F        , at = 426.57       , slot_id = 56048917;
 XVW.X0420430                  : H8_XVWMF001                 , at = 430.365      , slot_id = 57602915;
 XVW.X0420432                  : H8_XVWMF001                 , at = 432.045      , slot_id = 57602924;
 XCSH.X0420440                 : H8_XCSH_001                 , at = 440.48       , slot_id = 56051878;
 XVW.X0420441                  : H8_XVWMF001                 , at = 441.103      , slot_id = 57602933;
 XVW.X0420442                  : H8_XVWMF001                 , at = 442.528      , slot_id = 62457948;
 QWL.X0420444                  : MQD.X0420444_QUAD15D        , at = 444.263      , slot_id = 57419993;
 QWL.X0420447                  : MQD.X0420447_QUAD15D        , at = 447.693      , slot_id = 57420029;
 QNL.X0420457                  : MQF.X0420457_QUAD16F        , at = 456.683      , slot_id = 56048818;
 QNL.X0420460                  : MQF.X0420460_QUAD16F        , at = 460.113      , slot_id = 56048827;
 MCXCA.X0420462                : MCBH.X0420462_TRIM7H        , at = 462.163      , slot_id = 56048394;
 MCXCA.X0420463                : MCBV.X0420463_TRIM8V        , at = 462.933      , slot_id = 56048429;
 XVW.X0420463                  : H8_XVWMF001                 , at = 463.368      , slot_id = 57602951;
 XSCI.X0420463                 : H8_XSCI                     , at = 463.548      , slot_id = 57851232;
 XCET.X0420474                 : H8_XCET_001                 , at = 469.5065     , slot_id = 56032614;
 XVW.X0420474                  : H8_XVWMF001                 , at = 474          , slot_id = 57602960;
 XDWC.X0420474                 : H8_XDWC                     , at = 475.645      , slot_id = 57716381;
 XSCI.X0420475                 : H8_XSCI                     , at = 475.725      , slot_id = 57851329;
 XVW.X0420475                  : H8_XVWMF001                 , at = 475.875072404, slot_id = 57602969;
 XFFV.X0420476                 : H8_XFF__001                 , at = 477.363      , slot_id = 57112730;
 XFFH.X0420477                 : H8_XFF__001                 , at = 477.639      , slot_id = 57112739;
 XVW.X0420477                  : H8_XVWMF001                 , at = 477.847072404, slot_id = 57602978;
 XVW.X0420486                  : H8_XVWMF001                 , at = 486.856072404, slot_id = 57602987;
 XVVS.X0420490                 : H8_XVVSC001                 , at = 487.16       , slot_id = 59170219;
 XFFV.X0420493                 : H8_XFF__001                 , at = 488.834      , slot_id = 57716411;
 XFFH.X0420494                 : H8_XFF__001                 , at = 489.11       , slot_id = 57716434;
 QNL.X0420495                  : MQF.X0420495_QUAD17F        , at = 490.963      , slot_id = 56048836;
 QNL.X0420498                  : MQF.X0420498_QUAD17F        , at = 494.393      , slot_id = 56048845;
 XVW.X0420499                  : H8_XVWMF001                 , at = 496.158      , slot_id = 57602996;
 XTDV.X0420500                 : H8_XTDV_001                 , at = 497.403      , slot_id = 57613224;
 XTDV.X0420501                 : H8_XTDV_001                 , at = 499.023      , slot_id = 58239254;
 XVW.X0420501                  : H8_XVWMF001                 , at = 501          , slot_id = 57603005;
 QNL.X0420506                  : MQD.X0420506_QUAD18D        , at = 505.813      , slot_id = 56048854;
 XVW.X0420512                  : H8_XVWMF001                 , at = 512          , slot_id = 57603023;
 XWCA.X0420514                 : H8_XWCA                     , at = 513.653      , slot_id = 57716496;
 XSCI.X0420514                 : H8_XSCI                     , at = 513.733      , slot_id = 57851927;
 XCET.X0420519                 : H8_XCET_001                 , at = 524.158132404, slot_id = 56032623;
 QNL.X0420526                  : MQF.X0420526_QUAD19F        , at = 526.303      , slot_id = 56048863;
 MCXCA.X0420528                : MCBH.X0420528_TRIM9H        , at = 528.453      , slot_id = 56048438;
 MCXCA.X0420529                : MCBV.X0420529_TRIM10V       , at = 529.223      , slot_id = 56048340;
 XVW.X0420531                  : VXWP                        , at = 531.53609    , slot_id = 58587514;
 XVW.X0420533                  : H8_XCETWAM003               , at = 531.620132404, slot_id = 57603050;
 XCET.X0420537                 : H8_XCET_001                 , at = 541.100132404, slot_id = 56032632;
 XVW.X0420538                  : H8_XCETWAM003               , at = 541.467332404, slot_id = 57603059;
 XSCI.X0420541                 : XSCI ATLAS_1                , at = 541.633132404, slot_id = 57716535;
 XSCI.X0420542                 : XSCI ATLAS_2                , at = 542.048132404, slot_id = 57716600;
 XSCI.X0420543                 : H8_XSCI                     , at = 542.63       , slot_id = 57851365;
 XDWC.X0420543                 : H8_XDWC                     , at = 542.85       , slot_id = 57716626;
 XVW.X0420543                  : H8_XVWMF001                 , at = 542.99       , slot_id = 57603068;
 MBMOR.X0420553                : MBXH.X0420553_MORPURGO      , at = 553.3        , slot_id = 57423607;
 XVW.X0420555                  : VXWP                        , at = 555.23709    , slot_id = 57602803;
 XDWC.X0420555                 : H8_XDWC                     , at = 555.469      , slot_id = 58587742;
 EXP.X0420559                  : ATLAS_TILECAL.X0420559      , at = 559.18       , slot_id = 62388560;
 XTDV.X0420572                 : H8_XTDV_001                 , at = 570.009      , slot_id = 57613260;
 XTDV.X0420573                 : H8_XTDV_001                 , at = 571.629      , slot_id = 58239484;
 XWCA.X0420574                 : H8_XWCA                     , at = 573.439      , slot_id = 57716673;
 XSCI.X0420574                 : H8_XSCI                     , at = 573.519      , slot_id = 57851401;
 EXP.X0420578                  : ATLAS_LARGAT.X0420578       , at = 577.807      , slot_id = 62388597;
 XDWC.X0420583                 : H8_XDWC                     , at = 595.539      , slot_id = 57716696;
 EXP.X0420602                  : TOTEM.X0420602              , at = 601.669      , slot_id = 62388628;
 TDE.X0420615                  : DUMP.X0420615               , at = 615.158      , slot_id = 62388670;
ENDSEQUENCE;

return;