#!/usr/bin/python

import sys
from pathlib import Path
import os
import io

from cpymad.madx import Madx
import numpy as np

stdout = io.StringIO()


def trycall(mad, filename):
    try:
        mad.call(str(filename))
    except RuntimeError as er:
        print(*stdout.getvalue().splitlines()[-3:], sep="\n")
        print(f"MAD-X error: Calling `{filename}` generated error")
        sys.exit(0)


def tryinput(mad, cmds):
    for cmd in cmds.split(";"):
        try:
            mad.input(cmd)
        except RuntimeError as er:
            print(*stdout.getvalue().splitlines()[-3:], sep="\n")
            print(f"MAD-X error: Executing `{cmd}` generated error")
            sys.exit(0)


def force_mid_beam(mad):
    mad.input("REAL CONST DS = 0.0;")
    for i in range(1, 9):
        mad.input(f"REAL CONST AIP{i} = 0.0;")


def fix_negative_drifts(mad):
    for vv in "l.mgmwh,l.mgmwv,l.mgmwh003,l.mgmwv003".split(","):
        mad.input(f"REAL CONST {vv} = 0.0;")


def increase_circumference(mad):
    mad.set(format="25.9f")
    mad.input("real const lhclength = 26658.8832+10;")


def correct_sepdip(mad):
    values = [
        85.913 - 0.000001854199165,
        63.295 - 0.000001848931778,
        26.493 - 0.000000331964355,
        71.901 - 0.000001945077305,
        85.913 - 0.000001854199165,
        0,
        39.7395 - 0.000000101847040,
        63.295 - 0.000001849496213,
    ]
    for ii, vv in enumerate(values):
        mad.input(f"REAL CONST dsep{ii+1} = {vv};")


def correct_sepdip_ls3(mad):
    values = [
        64.561 - 0.000001885913335,
        63.295 - 0.000001848931778,
        26.493 - 0.000000331964355,
        71.901 - 0.000001945077305,
        64.561 - 0.000001885913335,
        0,
        39.7395 - 0.000000101847040,
        63.295 - 0.000001849496213,
    ]
    for ii, vv in enumerate(values):
        mad.input(f"REAL CONST dsep{ii+1} = {vv};")


def correct_dcum(mad, beam="b1"):
    cmd = """\
     select,  flag=seqedit, clear;
     seqedit, sequence=lhcb1;
     ddsep1 = +0.000051051289532;
     ddsep2 = +0.000070627521911;
     ddsep3 = +0.000003582471000;
     ddsep4 = +0.000084905493726;
     ddsep5 = +0.000051051269282;
     ddsep7 = +0.000002627241134;
     ddsep8 = +0.000070627523437;
     select,  flag=seqedit, range=mbxw.f4l1  /#e;  move, element=selected, by=ddsep1;
     select,  flag=seqedit, range=mbrc.4r8.b1/#e;  move, element=selected, by=ddsep8;
     select,  flag=seqedit, range=mbx.4l8    /#e;  move, element=selected, by=ddsep8;
     select,  flag=seqedit, range=mbw.c6r7.b1/#e;  move, element=selected, by=ddsep7;
     select,  flag=seqedit, range=mbw.b6l7.b1/#e;  move, element=selected, by=ddsep7;
     select,  flag=seqedit, range=mbrc.4r5.b1/#e;  move, element=selected, by=ddsep5;
     select,  flag=seqedit, range=mbxw.f4l5  /#e;  move, element=selected, by=ddsep5;
     select,  flag=seqedit, range=mbrb.5r4.b1/#e;  move, element=selected, by=ddsep4;
     select,  flag=seqedit, range=mbrs.5l4.b1/#e;  move, element=selected, by=ddsep4;
     select,  flag=seqedit, range=mbw.d6r3.b1/#e;  move, element=selected, by=ddsep3;
     select,  flag=seqedit, range=mbw.c6l3.b1/#e;  move, element=selected, by=ddsep3;
     select,  flag=seqedit, range=mbrc.4r2.b1/#e;  move, element=selected, by=ddsep2;
     select,  flag=seqedit, range=mbx.4l2    /#e;  move, element=selected, by=ddsep2;
     select,  flag=seqedit, range=mbrc.4r1.b1/#e;  move, element=selected, by=ddsep1;
     endedit;""".replace(
        "b1", beam
    )
    mad.input(cmd)


def correct_dcum_ls3(mad, beam="b1"):
    cmd = """\
     select,  flag=seqedit, clear;
     seqedit, sequence=lhcb1;
     ddsep1 = +7.01106959e-05;
     ddsep2 = +7.11010429e-05;
     ddsep3 = +2.96669144e-06;
     ddsep4 = +8.53790183e-05;
     ddsep5 = +7.01106959e-05;
     ddsep7 = +2.68110263e-06;
     ddsep8 = +2.10517769e-05;
     select,  flag=seqedit, range=mbxf.4l1   /#e;  move, element=selected, by=ddsep1;
     select,  flag=seqedit, range=mbrc.4r8.b1/#e;  move, element=selected, by=ddsep8;
     select,  flag=seqedit, range=mbx.4l8    /#e;  move, element=selected, by=ddsep8;
     select,  flag=seqedit, range=mbw.c6r7.b1/#e;  move, element=selected, by=ddsep7;
     select,  flag=seqedit, range=mbw.b6l7.b1/#e;  move, element=selected, by=ddsep7;
     select,  flag=seqedit, range=mbrc.4r5.b1/#e;  move, element=selected, by=ddsep5;
     select,  flag=seqedit, range=mbxf.4l5  /#e;  move, element=selected, by=ddsep5;
     select,  flag=seqedit, range=mbrb.5r4.b1/#e;  move, element=selected, by=ddsep4;
     select,  flag=seqedit, range=mbrs.5l4.b1/#e;  move, element=selected, by=ddsep4;
     select,  flag=seqedit, range=mbw.d6r3.b1/#e;  move, element=selected, by=ddsep3;
     select,  flag=seqedit, range=mbw.c6l3.b1/#e;  move, element=selected, by=ddsep3;
     select,  flag=seqedit, range=mbrc.4r2.b1/#e;  move, element=selected, by=ddsep2;
     select,  flag=seqedit, range=mbx.4l2    /#e;  move, element=selected, by=ddsep2;
     select,  flag=seqedit, range=mbrd.4r1.b1/#e;  move, element=selected, by=ddsep1;
     endedit;""".replace(
        "b1", beam
    )
    mad.input(cmd)


def rename_markers_for_survey(mad, beam="b1"):
    cmd = """\
    seqedit, sequence=lhcb1;
    select,  flag=seqedit, clear;
    select,  flag=seqedit, pattern=^s\.;
    select,  flag=seqedit, pattern=^e\.;
    remove, element=selected;
    install, element=ip.1, class=marker, at=0, from=ip1;
    install, element=ip.2, class=marker, at=0, from=ip2;
    install, element=ip.3, class=marker, at=0, from=ip3;
    install, element=ip.4, class=marker, at=0, from=ip4;
    install, element=ip.5, class=marker, at=0, from=ip5;
    install, element=ip.6, class=marker, at=0, from=ip6;
    install, element=ip.7, class=marker, at=0, from=ip7;
    install, element=ip.8, class=marker, at=0, from=ip8;
    endedit;
    seqedit, sequence=lhcb1;
    flatten;
    remove, element=ip1;
    remove, element=ip2;
    remove, element=ip3;
    remove, element=ip4;
    remove, element=ip5;
    remove, element=ip6;
    remove, element=ip7;
    remove, element=ip8;
    endedit; """.replace(
        "b1", beam
    )
    mad.input(cmd)


def make_survey(mad, beam="b1", file="survey.tfs"):
    sequence = "lhc" + beam
    bv = 1 if beam == "b1" else -1
    mad.beam(sequence=sequence, bv=bv)
    mad.use(sequence)
    cols = "name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,tilt,mech_sep,assembly_id,slot_id".split(
        ","
    )
    mad.select(flag="survey", range="ip.1/ip1.l1", column=cols)
    mad.survey(
        sequence=sequence,
        file=file,
        x0=-2202.21027,
        z0=2710.63882,
        y0=2359.00656,
        theta0=-4.315508007,
        phi0=0.0124279564,
        psi0=-0.0065309236,
    )


def make_mid_beam(seqfile="lhc.seq", outdir="."):
    mad = Madx(stdout=stdout)
    force_mid_beam(mad)
    mad.call(str(seqfile))
    rename_markers_for_survey(mad, beam="b1")
    rename_markers_for_survey(mad, beam="b2")
    if not outdir.exists():
        outdir.mkdir()
    make_survey(mad, beam="b1", file=str(outdir / f"survey_lhcb1_mid.tfs"))
    make_survey(mad, beam="b2", file=str(outdir / f"survey_lhcb2_mid.tfs"))
    return mad


def make_circ_beam(seqfile="lhc.seq", outdir="."):
    mad = Madx(stdout=stdout)
    increase_circumference(mad)
    if "LS3" in str(seqfile).upper():
        correct_sepdip_ls3(mad)
    else:
        correct_sepdip(mad)
    mad.call(str(seqfile))
    rename_markers_for_survey(mad, beam="b1")
    rename_markers_for_survey(mad, beam="b2")
    if "LS3" in str(seqfile).upper():
        correct_dcum_ls3(mad, beam="b1")
        correct_dcum_ls3(mad, beam="b2")
    else:
        correct_dcum(mad, beam="b1")
        correct_dcum(mad, beam="b2")
    outdir = Path(outdir)
    if not outdir.exists():
        outdir.mkdir()
    make_survey(mad, beam="b1", file=str(outdir / f"survey_lhcb1.tfs"))
    make_survey(mad, beam="b2", file=str(outdir / f"survey_lhcb2.tfs"))
    return mad


def copy_table(tt, cols):
    out = {}
    for cc in cols:
        out[cc] = tt[cc]
    return out


class LHCModel:
    arcs = ["12", "23", "34", "45", "56", "67", "78", "81"]

    def __init__(self, seqpath):
        self.seqpath = Path(seqpath)
        self.appath = self.seqpath.parent / f"APERTURE_{self.seqpath.stem}.seq"
        self.optpath = (
            self.seqpath.parent / "tests" / f"INJECTION_{self.seqpath.stem}.madx"
        )
        if not self.seqpath.exists():
            print(f"File {self.seqpath} not found")

        if not self.appath.exists():
            print(f"File {self.appath} not found")
        self.offsets = {}

    def make_twiss(self, mad, aperture=False):
        apcoll = ["aper_1", "aper_2", "aper_3", "aper_4", "apoff_1", "apoff_2"]
        cols = [
            "name",
            "s",
            "x",
            "y",
            "betx",
            "bety",
            "dx",
            "dy",
            "mux",
            "muy",
        ] + apcoll
        mad.select(
            flag="twiss",
            column=apcoll,
        )
        tt = []
        for ss in ["lhcb1", "lhcb2"]:
            if aperture:
                for nn, offset in self.offsets[ss].items():
                    _, _, xdiff = offset
                    mad.elements[nn].aper_offset = [xdiff]
            mad.use(ss)
            tt.append(copy_table(self.mad.twiss(), cols))
        return tt

    def check_optics(self, mad):
        tw = dict(zip("12", self.make_twiss(self.mad, aperture=False)))
        qt = {}
        q0 = {"x1": 62.31, "y1": 60.32, "x2": 62.31, "y2": 60.32}

        for b12 in "12":
            for xy in "xy":
                qt[xy + b12] = tw[b12]["mu" + xy][-1]

        for b12 in "12":
            for xy in "xy":
                diff = abs(qt[xy + b12] - q0[xy + b12])
                if diff > 1e-6:
                    print(
                        f"Error: Q{xy} Beam{b12} {qt[xy+b12]:18.15f} differs from nominal {q0[xy+b12]} by {diff}"
                    )
        return tw

    def load_sequence(self, mad, seqname):
        trycall(mad, seqname)

        if mad.globals.LHCLENGTH != 26658.8832:
            print(
                f"Error: LHC length {mad.globals.LHCLENGTH} m not correct, expecting 26658.8832 m."
            )

    def load_optics(self, mad, optname):
        trycall(mad, optname)

    def make_beam(self, mad):
        mad.input(
            """
            beam, sequence=lhcb1, bv= 1,
              particle=proton, charge=1, mass=0.938272046,
              energy= 450,   npart=1.2e11,kbunch=2556,
              ex=5.2126224777777785e-09,ey=5.2126224777777785e-09;
            beam, sequence=lhcb2, bv=-1,
              particle=proton, charge=1, mass=0.938272046,
              energy= 450,   npart=1.2e11,kbunch=2556,
              ex=5.2126224777777785e-09,ey=5.2126224777777785e-09;
            """
        )
        tryinput(mad, "use,sequence=lhcb1;")
        tryinput(mad, "use,sequence=lhcb2;")

    def make_thin(self, mad):
        mad.input(
            """
            select, flag=makethin, clear;
            select, flag=makethin, class=mb, slice=4;
            select, flag=makethin, class=mq, slice=4;
            select, flag=makethin, pattern=mqx,  slice=16;
            select, flag=makethin, pattern=mbx,  slice=4;
            select, flag=makethin, pattern=mbr,   slice=4;
            select, flag=makethin, pattern=mbr,   slice=4;
            select, flag=makethin, pattern=mqw,   slice=4;
            select, flag=makethin, pattern=mqy\.,    slice=4;
            select, flag=makethin, pattern=mqm\.,    slice=4;
            select, flag=makethin, pattern=mqmc\.,   slice=4;
            select, flag=makethin, pattern=mqml\.,   slice=4;
            select, flag=makethin, pattern=mqtlh\.,  slice=2;
            select, flag=makethin, pattern=mqtli\.,  slice=2;
            select, flag=makethin, pattern=mqt\.  ,  slice=2;
            """
        )
        tryinput(
            mad,
            "use,sequence=lhcb1; makethin,sequence=lhcb1,makedipedge=false,style=teapot,makeendmarkers=true;",
        )
        tryinput(
            mad,
            "use,sequence=lhcb2; makethin,sequence=lhcb2,makedipedge=false,style=teapot,makeendmarkers=true;",
        )

    def load_aperture(self, mad):
        trycall(mad, self.appath)

    def make_survey(
        self, mad, x0=0, y0=0, z0=0, theta0=0, phi0=0, psi0=0, file1=None, file2=None
    ):
        cols = ["name", "s", "x", "y", "z"]
        mad.use("lhcb1")
        mad.survey(x0=x0, y0=y0, z0=z0, theta0=z0, phi0=phi0, psi0=psi0, file=file1)
        s1 = copy_table(mad.table.survey, cols)

        mad.use("lhcb2")
        mad.survey(x0=x0, y0=y0, z0=z0, theta0=z0, phi0=phi0, psi0=psi0, file=file2)
        s2 = copy_table(mad.table.survey, cols)
        return s1, s2

    def compute_offsets(self, mad):
        """Allows to calculate X with respect to the mid beam
        but S is the same of the circulating beam
        """
        abn = [f"ab.a{aa}" for aa in self.arcs]
        angles = {}

        for ab in abn:
            angles[ab] = mad.globals[ab]
            mad.globals[ab] = 0

        self.thsu = self.make_survey(self.mad)
        self.assign_aper_offsets(
            mad.sequence.lhcb1, self.thsu[0]["name"], self.thsu[0]["x"]
        )
        self.assign_aper_offsets(
            mad.sequence.lhcb2, self.thsu[1]["name"], self.thsu[1]["x"]
        )

        for ab in abn:
            mad.globals[ab] = angles[ab]

    def assign_aper_offsets(self, seq, name, x):
        offsets = {}
        for nn, xx in zip(name, x):
            nn = nn.split(":")[0]  # real name
            if nn in seq.elements:
                el = seq.elements[nn]
                if el.aperture[0] != 0:
                    # position of center with respect to ref
                    xdiff = el.mech_sep / 2 - xx
                    # el.aper_offset = [xdiff, el.v_pos]
                    offsets[nn] = (xx, el.mech_sep / 2, xdiff)
        self.offsets[seq.name] = offsets

    def compare_beamsize_beam(self, su, tw):
        sx = su["x"]
        s = tw["s"]
        x = tw["x"]
        bx = tw["betx"]
        dx = tw["dx"]
        x = tw["x"]
        ax = tw["aper_1"]
        # ax = np.min([tw["aper_1"], tw["aper_3"]], axis=0)
        ox = tw["apoff_1"]
        idx = abs(ax > 0)
        sigx = 13 * np.sqrt(2.5e-6 / 450 * 0.938 * bx) + abs(dx) * 8e-4
        name = tw["name"]
        for en, ex, esx, esigx, eox, eax in zip(name, x, sx, sigx, ox, ax):
            if eax > 0:
                bp = ex + esigx
                ap = eox + eax
                bm = ex - esigx
                am = eox - eax
                ereal = en.split(":")[0].upper()
                if bp > ap or bm < am:
                    print(f"Horizontal beam losses in {ereal:15}:", end=" ")
                    print(f"Beam={ex+esx:6.3f}+-{esigx:5.3f}", end=" ")
                    print(f"Aperture={eox+esx:6.3f}+-{eax:5.3f}")

        return s, sx, x, sigx, ox, ax, idx

    def compare_beamsize(self):
        self.sizes = (
            self.compare_beamsize_beam(self.thsu[0], self.thtw[0]),
            self.compare_beamsize_beam(self.thsu[1], self.thtw[1]),
        )

    def plot_beam(self):
        import matplotlib.pyplot as plt

        for ii, cc in enumerate("br"):
            s, sx, x, sigx, ox, ax, idx = self.sizes[ii]

            plt.fill_between(
                s[idx], (ox - ax + sx)[idx], (ox + ax + sx)[idx], alpha=0.5, color="k"
            )
            plt.plot(s[idx], (ox - ax + sx)[idx], color="k")
            plt.plot(s[idx], (ox + ax + sx)[idx], color="k")
            plt.fill_between(s, x - sigx + sx, x + sigx + sx, alpha=0.5, color=cc)

    def run_tests(self):
        # run madx
        self.mad = Madx(stdout=stdout, stderr=sys.stdout)

        # load sequence for normal and mid beam
        self.load_sequence(self.mad, self.seqpath)

        # make beam
        self.make_beam(self.mad)

        if self.optpath.exists():
            self.load_optics(self.mad, self.optpath)
            self.tw = self.check_optics(self.mad)

        # make thin
        self.make_thin(self.mad)
        self.load_aperture(self.mad)
        self.compute_offsets(self.mad)
        if self.optpath.exists():
            self.thtw = self.make_twiss(self.mad,aperture=True)
            self.compare_beamsize()

    def close(self):
        self.mad.exit()


if __name__ == "__main__":
    # validation
    seqpath = Path(sys.argv[1])
    lhc = LHCModel(seqpath)
    lhc.run_tests()
    # lhc.plot_beam()
    lhc.close()

    # survey
    sudir = seqpath.parent / f"SURVEY_{seqpath.stem}"
    make_mid_beam(seqpath, sudir).exit()
    make_circ_beam(seqpath, sudir).exit()
