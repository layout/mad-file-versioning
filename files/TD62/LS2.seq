
/*************************************************************************************
*
* TD62 version (draft) LS2 in MAD X SEQUENCE format
* Generated the 22-DEC-2023 03:13:51 from Layout
*
*************************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TD62_BCTFD                   := 0;
l.TD62_BPMD                    := 0;
l.TD62_BPMSE                   := 0;
l.TD62_BPMSX004                := 0;
l.TD62_BPMYA                   := 0;
l.TD62_BPMYB                   := 0;
l.TD62_BTVD                    := 0;
l.TD62_BTVDD                   := 0;
l.TD62_BTVSE                   := 0;
l.TD62_MCBYA                   := 0.899;
l.TD62_MCBYV                   := 0.899;
l.TD62_MKBH                    := 1.931;
l.TD62_MKBV                    := 1.233;
l.TD62_MKD                     := 1.348;
l.TD62_MQY2                    := 1.7;
l.TD62_MQY__001                := 3.4;
l.TD62_MSDA                    := 4.088;
l.TD62_MSDB                    := 4.088;
l.TD62_MSDC                    := 4.088;
l.TD62_TCDSA                   := 3.012;
l.TD62_TCDSB                   := 3.012;
l.TD62_TDE                     := 0;

//---------------------- COLLIMATOR     ---------------------------------------------
TD62_TCDSA               : COLLIMATOR  , L := l.TD62_TCDSA;        ! Collimator absorber block for MSD Protection (IR6) - upstream - There is no 3D available for that equipment - info Benoit Riffaud
TD62_TCDSB               : COLLIMATOR  , L := l.TD62_TCDSB;        ! Collimator absorber block for MSD Protection (IR6) - downstream
//---------------------- CORRECTOR      ---------------------------------------------
TD62_MCBYA               : CORRECTOR   , L := l.TD62_MCBYA;        ! Wide Aperture Orbit Corrector Associated to MQY. External Aperture Corrected Horizontally and Internal Aperture Verticallly
//---------------------- HIDDEN         ---------------------------------------------
TD62_TDE                 : HIDDEN      , L := l.TD62_TDE;          ! Dump for Ejected Beam, External
//---------------------- HKICKER        ---------------------------------------------
TD62_MKBH                : HKICKER     , L := l.TD62_MKBH;         ! Diluter Dump Kicker, Horizontal
//---------------------- INSTRUMENT     ---------------------------------------------
TD62_BTVSE               : INSTRUMENT  , L := l.TD62_BTVSE;        ! TV monitor located upstream the TCDS collimator to visualize the extracted beam at the entrance of the extraction septum.
//---------------------- MONITOR        ---------------------------------------------
TD62_BCTFD               : MONITOR     , L := l.TD62_BCTFD;        ! Fast Beam Current Transformer for the Dump Lines
TD62_BPMD                : MONITOR     , L := l.TD62_BPMD;         ! Beam Position Monitor (4 Strip Lines, WARM) for the Dump Lines
TD62_BPMSE               : MONITOR     , L := l.TD62_BPMSE;        ! Beam Position Monitor (4 Strip Lines, WARM) upstream the TCDS3
TD62_BPMSX004            : MONITOR     , L := l.TD62_BPMSX004;     ! Beam Position Monitor (4 Strip Lines, Warm) behind D1 - Single Beam
TD62_BPMYA               : MONITOR     , L := l.TD62_BPMYA;        ! Beam Position Monitor, Wide Aperture, Normal Beam Screen
TD62_BPMYB               : MONITOR     , L := l.TD62_BPMYB;        ! Beam Position Monitor, Wide Aperture, Rotated Beam Screen
TD62_BTVD                : MONITOR     , L := l.TD62_BTVD;         ! Beam Observation TV Monitors based on Screens for the Dump Lines
TD62_BTVDD               : MONITOR     , L := l.TD62_BTVDD;        ! Beam Observation TV Monitor based on Screens in front of the Dump Block
//---------------------- QUADRUPOLE     ---------------------------------------------
TD62_MQY2                : QUADRUPOLE  , L := l.TD62_MQY2;         ! Virtual Half MQY (Magnet coil for Wide aperture quadrupole in the insertions, twin aperture) for TD62 TD68
//---------------------- RBEND          ---------------------------------------------
TD62_MKD                 : RBEND       , L := l.TD62_MKD;          ! Ejection dump kicker
TD62_MQY__001            : RBEND       , L := l.TD62_MQY__001;     ! Magnet coil for Wide aperture quadrupole in the insertions, twin aperture
TD62_MSDA                : RBEND       , L := l.TD62_MSDA;         ! Ejection dump septum, Module A
TD62_MSDB                : RBEND       , L := l.TD62_MSDB;         ! Ejection dump septum, Module B
TD62_MSDC                : RBEND       , L := l.TD62_MSDC;         ! Ejection dump septum, Module C
//---------------------- VCORRECTOR     ---------------------------------------------
TD62_MCBYV               : VCORRECTOR  , L := l.TD62_MCBYV;        ! Orbit Corrector in MCBYA(B)
//---------------------- VKICKER        ---------------------------------------------
TD62_MKBV                : VKICKER     , L := l.TD62_MKBV;         ! Diluter Dump Kicker, Vertical


/************************************************************************************/
/*                      TD62 SEQUENCE                                               */
/************************************************************************************/

TD62EXTRA : SEQUENCE, refer = centre,   L = 975.9;
 MQY.5R6.B1                    : TD62_MQY__001          , at = 0            , slot_id = 2302997, assembly_id= 103287;
 MQY2.5R6.B2                   : TD62_MQY2              , at = .85          , slot_id = 54987155, assembly_id= 103287;
 BPMYA.5R6.B2                  : TD62_BPMYA             , at = 2.718        , slot_id = 246611, assembly_id= 103287;
 MKD.O5R6.B2                   : TD62_MKD               , at = 5.2685       , slot_id = 134637;
 MKD.N5R6.B2                   : TD62_MKD               , at = 6.9715       , slot_id = 134636;
 MKD.M5R6.B2                   : TD62_MKD               , at = 8.9545       , slot_id = 134635;
 MKD.L5R6.B2                   : TD62_MKD               , at = 10.6575      , slot_id = 134634;
 MKD.K5R6.B2                   : TD62_MKD               , at = 12.6405      , slot_id = 134633;
 MKD.J5R6.B2                   : TD62_MKD               , at = 14.4985      , slot_id = 134632;
 MKD.I5R6.B2                   : TD62_MKD               , at = 16.4815      , slot_id = 134631;
 MKD.H5R6.B2                   : TD62_MKD               , at = 18.1845      , slot_id = 134630;
 MKD.G5R6.B2                   : TD62_MKD               , at = 19.8875      , slot_id = 134629;
 MKD.F5R6.B2                   : TD62_MKD               , at = 21.8705      , slot_id = 134628;
 MKD.E5R6.B2                   : TD62_MKD               , at = 23.7285      , slot_id = 134627;
 MKD.D5R6.B2                   : TD62_MKD               , at = 25.7115      , slot_id = 134626;
 MKD.C5R6.B2                   : TD62_MKD               , at = 27.4145      , slot_id = 134625;
 MKD.B5R6.B2                   : TD62_MKD               , at = 29.3975      , slot_id = 134624;
 MKD.A5R6.B2                   : TD62_MKD               , at = 31.1005      , slot_id = 134623;
 MCBYA.4R6                     : TD62_MCBYA             , at = 34.022       , slot_id = 246609, assembly_id= 103285;
 MCBYV.4R6.B2                  : TD62_MCBYV             , at = 34.053       , slot_id = 252140, assembly_id= 103285;
 MQY.4R6.B1                    : TD62_MQY__001          , at = 36.4         , slot_id = 2303141, assembly_id= 103285;
 MQY.4R6.B2                    : TD62_MQY__001          , at = 36.4         , slot_id = 2303140, assembly_id= 103285;
 BPMYB.4R6.B2                  : TD62_BPMYB             , at = 39.118       , slot_id = 298235, assembly_id= 103285;
 BPMSX.B4R6.B2                 : TD62_BPMSX004          , at = 65.6725      , slot_id = 377635;
 BPMSX.A4R6.B2                 : TD62_BPMSX004          , at = 66.2575      , slot_id = 377634;
 BPMSE.4R6.B2                  : TD62_BPMSE             , at = 162.9325     , slot_id = 181628;
 BTVSE.A4R6.B2                 : TD62_BTVSE             , at = 163.625      , slot_id = 181627;
 TCDSA.4R6.B2                  : TD62_TCDSA             , at = 165.825      , slot_id = 631490, assembly_id= 616849;
 TCDSB.4R6.B2                  : TD62_TCDSB             , at = 169.375      , slot_id = 631494, assembly_id= 103281;
 MSDA.E4R6.B2                  : TD62_MSDA              , at = 173.83       , slot_id = 134619;
 MSDA.D4R6.B2                  : TD62_MSDA              , at = 178.74       , slot_id = 134618;
 MSDA.C4R6.B2                  : TD62_MSDA              , at = 183.65       , slot_id = 134616;
 MSDA.B4R6.B2                  : TD62_MSDA              , at = 188.56       , slot_id = 134614;
 MSDA.A4R6.B2                  : TD62_MSDA              , at = 193.47       , slot_id = 134611;
 MSDB.B4R6.B2                  : TD62_MSDB              , at = 198.38       , slot_id = 134609;
 MSDB.A4R6.B2                  : TD62_MSDB              , at = 203.29       , slot_id = 134607;
 MSDB.A4L6.B2                  : TD62_MSDB              , at = 208.2        , slot_id = 134606;
 MSDB.B4L6.B2                  : TD62_MSDB              , at = 213.11       , slot_id = 134603;
 MSDB.C4L6.B2                  : TD62_MSDB              , at = 218.02       , slot_id = 134602;
 MSDC.A4L6.B2                  : TD62_MSDC              , at = 222.93       , slot_id = 134599;
 MSDC.B4L6.B2                  : TD62_MSDC              , at = 227.84       , slot_id = 134598;
 MSDC.C4L6.B2                  : TD62_MSDC              , at = 232.75       , slot_id = 134596;
 MSDC.D4L6.B2                  : TD62_MSDC              , at = 237.66       , slot_id = 134594;
 MSDC.E4L6.B2                  : TD62_MSDC              , at = 242.57       , slot_id = 134592;
 BCTFD.623130.B2               : TD62_BCTFD             , at = 313.39       , slot_id = 631894;
 BCTFD.623139.B2               : TD62_BCTFD             , at = 314.31       , slot_id = 631001;
 MKBH.623185.B2                : TD62_MKBH              , at = 319.4405     , slot_id = 632059, assembly_id= 631900;
 MKBH.623206.B2                : TD62_MKBH              , at = 321.5415     , slot_id = 632060, assembly_id= 631900;
 MKBH.623233.B2                : TD62_MKBH              , at = 324.2025     , slot_id = 632063, assembly_id= 631901;
 MKBH.623254.B2                : TD62_MKBH              , at = 326.3035     , slot_id = 632064, assembly_id= 631901;
 MKBV.623280.B2                : TD62_MKBV              , at = 328.607      , slot_id = 632067, assembly_id= 631902;
 MKBV.623303.B2                : TD62_MKBV              , at = 330.917      , slot_id = 632068, assembly_id= 631902;
 MKBV.623323.B2                : TD62_MKBV              , at = 332.863      , slot_id = 632071, assembly_id= 631903;
 MKBV.623346.B2                : TD62_MKBV              , at = 335.173      , slot_id = 632072, assembly_id= 631903;
 MKBV.623365.B2                : TD62_MKBV              , at = 337.119      , slot_id = 632075, assembly_id= 631904;
 MKBV.623388.B2                : TD62_MKBV              , at = 339.429      , slot_id = 632076, assembly_id= 631904;
 BPMD.623451.B2                : TD62_BPMD              , at = 345.2345     , slot_id = 631910;
 BTVD.623458.B2                : TD62_BTVD              , at = 346.077      , slot_id = 631912;
 BTVDD.629339.B2               : TD62_BTVDD             , at = 934.95       , slot_id = 632046;
 TDE.629691.B2                 : TD62_TDE               , at = 973.271      , slot_id = 1988535;
ENDSEQUENCE;

/************************************************************************************/
/*                       STRENGTH CONSTANTS                                         */
/************************************************************************************/

tilt.MSD                       := -pi/2;

/************************************************************************************/
/*                       STRENGTHS                                                  */
/************************************************************************************/
  MQY2.5R6.B2,               ANGLE := a.MQY2, K1 := k1.MQY2;
  MKD.O5R6.B2,               ANGLE := a.MKD;
  MKD.N5R6.B2,               ANGLE := a.MKD;
  MKD.M5R6.B2,               ANGLE := a.MKD;
  MKD.L5R6.B2,               ANGLE := a.MKD;
  MKD.K5R6.B2,               ANGLE := a.MKD;
  MKD.J5R6.B2,               ANGLE := a.MKD;
  MKD.I5R6.B2,               ANGLE := a.MKD;
  MKD.H5R6.B2,               ANGLE := a.MKD;
  MKD.G5R6.B2,               ANGLE := a.MKD;
  MKD.F5R6.B2,               ANGLE := a.MKD;
  MKD.E5R6.B2,               ANGLE := a.MKD;
  MKD.D5R6.B2,               ANGLE := a.MKD;
  MKD.C5R6.B2,               ANGLE := a.MKD;
  MKD.B5R6.B2,               ANGLE := a.MKD;
  MKD.A5R6.B2,               ANGLE := a.MKD;
  MQY.4R6.B2,                ANGLE := a.MQY, K1 := k1.MQY;
  MSDA.E4R6.B2,              ANGLE := a.MSDA, TILT := tilt.MSD;
  MSDA.D4R6.B2,              ANGLE := a.MSDA, TILT := tilt.MSD;
  MSDA.C4R6.B2,              ANGLE := a.MSDA, TILT := tilt.MSD;
  MSDA.B4R6.B2,              ANGLE := a.MSDA, TILT := tilt.MSD;
  MSDA.A4R6.B2,              ANGLE := a.MSDA, TILT := tilt.MSD;
  MSDB.B4R6.B2,              ANGLE := a.MSDB, TILT := tilt.MSD;
  MSDB.A4R6.B2,              ANGLE := a.MSDB, TILT := tilt.MSD;
  MSDB.A4L6.B2,              ANGLE := a.MSDB, TILT := tilt.MSD;
  MSDB.B4L6.B2,              ANGLE := a.MSDB, TILT := tilt.MSD;
  MSDB.C4L6.B2,              ANGLE := a.MSDB, TILT := tilt.MSD;
  MSDC.A4L6.B2,              ANGLE := a.MSDC, TILT := tilt.MSD;
  MSDC.B4L6.B2,              ANGLE := a.MSDC, TILT := tilt.MSD;
  MSDC.C4L6.B2,              ANGLE := a.MSDC, TILT := tilt.MSD;
  MSDC.D4L6.B2,              ANGLE := a.MSDC, TILT := tilt.MSD;
  MSDC.E4L6.B2,              ANGLE := a.MSDC, TILT := tilt.MSD;
  MKBH.623185.B2,            KICK := a.MKBH;
  MKBH.623206.B2,            KICK := a.MKBH;
  MKBH.623233.B2,            KICK := a.MKBH;
  MKBH.623254.B2,            KICK := a.MKBH;
return;