

  /**********************************************************************************
  *
  * ER (LEIR Ring) version (draft) YETS 2021-2022 in MAD X SEQUENCE format
  * Generated the 22-DEC-2023 02:10:59 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.ER_BIPM                      := 0;
l.ER_BPUAC                     := 0;
l.ER_BPUAE                     := 0;
l.ER_BTVLP006                  := 0;
l.ER_DEH                       := 0;
l.ER_DEV                       := 0;
l.ER_DWHV                      := 0;
l.ER_EC0                       := 0;
l.ER_EC1                       := 0;
l.ER_EC2                       := 0;
l.ER_EC3                       := 0;
l.ER_EC4                       := 0;
l.ER_EC5H                      := 0;
l.ER_ECDH                      := 0;
l.ER_ECQ                       := 0;
l.ER_KDHV                      := 0;
l.ER_KQF                       := 0;
l.ER_LMKFH                     := 0;
l.ER_MCCARWIP                  := 0;
l.ER_MKDFH                     := 0;
l.ER_MKKFH001                  := 0;
l.ER_ML_BA_WC                  := 0.427;
l.ER_MQNEK002                  := 0.5122;
l.ER_MQNEK003                  := 0.5072;
l.ER_MQNEK004                  := 0.5122;
l.ER_MQNEK005                  := 0.5172;
l.ER_MQNEK006                  := 0.1741;
l.ER_MQNEK007                  := 0.3431;
l.ER_MQNEK008                  := 0.2586;
l.ER_MQNEKFWP                  := 0.5172;
l.ER_MQSBANWP                  := 0.32;
l.ER_MU2HA002                  := 0;
l.ER_MU2HA003                  := 0;
l.ER_MU2HA004                  := 0;
l.ER_MU2HA005                  := 0;
l.ER_MU2HA006                  := 0;
l.ER_MU2HA007                  := 0;
l.ER_MU2HACWP                  := 0;
l.ER_MXNACIIP                  := 0.33535;
l.ER_MXSAAIIP                  := 0.33535;
l.ER_OMK                       := 0;
l.ER_UCH                       := 0;
l.ER_UCV                       := 0;
l.ER_UCV__001                  := 0;
l.ER_UDHV                      := 0;
l.ER_UEH                       := 0;
l.ER_UEH__001                  := 0;
l.ER_UEH__002                  := 0;
l.ER_UEV                       := 0;
l.ER_UEV__001                  := 0;
l.ER_UEV__002                  := 0;
l.ER_UWB__001                  := 0;
l.ER_XFW                       := 0;

//---------------------- HKICKER        ---------------------------------------------
ER_ECDH                  : HKICKER     , L := l.ER_ECDH;           ! LEIR electron cooler slot, kicker
ER_KQF                   : HKICKER     , L := l.ER_KQF;            ! Tune Kicker, LEIR
ER_LMKFH                 : HKICKER     , L := l.ER_LMKFH;          ! Assembly of fast horizontal kicker magnet
ER_MKDFH                 : HKICKER     , L := l.ER_MKDFH;          ! Kicker Magnet - Dipole Fast Horizontal
ER_MKKFH001              : HKICKER     , L := l.ER_MKKFH001;       ! Kicker, Fast Horizontal, LEIR
//---------------------- HMONITOR       ---------------------------------------------
ER_UEH                   : HMONITOR    , L := l.ER_UEH;            ! Beam pick-up, horizontal, circular dia 182mm, LEIR
ER_UEH__001              : HMONITOR    , L := l.ER_UEH__001;       ! Beam pick up, horizontal, rectangular shape for main unit bending magnets, LEIR
ER_UEH__002              : HMONITOR    , L := l.ER_UEH__002;       ! Beam pick up, horizontal, circular dia 140mm, inside electron cooler, LEIR
//---------------------- INSTRUMENT     ---------------------------------------------
ER_BPUAE                 : INSTRUMENT  , L := l.ER_BPUAE;          ! Beam Position Pick-up Axial type E
//---------------------- KICKER         ---------------------------------------------
ER_DEH                   : KICKER      , L := l.ER_DEH;            ! Corrector magnet horizontal function, electron cooler, LEIR
ER_DEV                   : KICKER      , L := l.ER_DEV;            ! Corrector magnet vertical function, electron cooler, LEIR
ER_DWHV                  : KICKER      , L := l.ER_DWHV;           ! Corrector in bending arc, LEIR
ER_KDHV                  : KICKER      , L := l.ER_KDHV;           ! Kicker
ER_MCCARWIP              : KICKER      , L := l.ER_MCCARWIP;       ! Corrector magnet, H+V, type DHV/DHN, LEIR
//---------------------- MARKER         ---------------------------------------------
ER_OMK                   : MARKER      , L := l.ER_OMK;            ! ER markers
//---------------------- MONITOR        ---------------------------------------------
ER_BIPM                  : MONITOR     , L := l.ER_BIPM;           ! Beam Ionisation Profile Monitor (AD, LEIR)
ER_BPUAC                 : MONITOR     , L := l.ER_BPUAC;          ! Beam Position Pick-up Axial type C
ER_BTVLP006              : MONITOR     , L := l.ER_BTVLP006;       ! Beam TV LEIR Type, Pneumatic, variant 006
ER_UCH                   : MONITOR     , L := l.ER_UCH;            ! Schottky Pick-up, horizontal, LEIR
ER_UCV                   : MONITOR     , L := l.ER_UCV;            ! Schottky Pick-up, vertical, LEIR
ER_UCV__001              : MONITOR     , L := l.ER_UCV__001;       ! Schottky pick up, vertical, LEIR
ER_UDHV                  : MONITOR     , L := l.ER_UDHV;           ! Pickup for transverse damper
ER_UWB__001              : MONITOR     , L := l.ER_UWB__001;       ! Wide band pick up, LEIR
//---------------------- MULTIPOLE      ---------------------------------------------
ER_ECQ                   : MULTIPOLE   , L := l.ER_ECQ;            ! LEIR electron cooler slot, multipole
ER_MU2HACWP              : MULTIPOLE   , L := l.ER_MU2HACWP;       ! Main unit, multipole, LEIR
ER_XFW                   : MULTIPOLE   , L := l.ER_XFW;            ! Pole face winding
//---------------------- QUADRUPOLE     ---------------------------------------------
ER_MQNEK002              : QUADRUPOLE  , L := l.ER_MQNEK002;       ! Quadrupole magnet, type QN LEIR
ER_MQNEK003              : QUADRUPOLE  , L := l.ER_MQNEK003;       ! Quadrupole magnet, type QN LEIR
ER_MQNEK004              : QUADRUPOLE  , L := l.ER_MQNEK004;       ! Quadrupole magnet, type QN, LEIR
ER_MQNEK005              : QUADRUPOLE  , L := l.ER_MQNEK005;       ! Quadrupole magnet, type QN, LEIR
ER_MQNEK006              : QUADRUPOLE  , L := l.ER_MQNEK006;       ! Quadrupole magnet, type QN, LEIR.
ER_MQNEK007              : QUADRUPOLE  , L := l.ER_MQNEK007;       ! Quadrupole magnet, type QN, LEIR
ER_MQNEK008              : QUADRUPOLE  , L := l.ER_MQNEK008;       ! Quadrupole magnet, type QN, LEIR
ER_MQNEKFWP              : QUADRUPOLE  , L := l.ER_MQNEKFWP;       ! Quadrupole magnet, type QN LEIR
ER_MQSBANWP              : QUADRUPOLE  , L := l.ER_MQSBANWP;       ! Quadrupole magnet, skew, type QSK
//---------------------- SBEND          ---------------------------------------------
ER_MU2HA002              : SBEND       , L := l.ER_MU2HA002;       ! Artificial point where magnet BAH has been split into two to add the pole face winding elements
ER_MU2HA003              : SBEND       , L := l.ER_MU2HA003;       ! Artificial point. The centre magnet is split up into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet, part of the main unit multipole BHN
ER_MU2HA004              : SBEND       , L := l.ER_MU2HA004;       ! Artificial point. The centre magnet is split up into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet, part of the main unit multipole BHN
ER_MU2HA005              : SBEND       , L := l.ER_MU2HA005;       ! Artificial point. The centre magnet is split up into BI1PM, BI1PO and BI1PI in order to put in monitors. In reality it is only one magnet, part of the main unit multipole BHN
ER_MU2HA006              : SBEND       , L := l.ER_MU2HA006;       ! Artificial point. Magnet BI1PI is separated into BI1PII and BI1PIO to give space for an Ionisation Profile Monitor. The IPM is inside BI1PI, which is really only one magnet (together with BI1PM and BI1PO).
ER_MU2HA007              : SBEND       , L := l.ER_MU2HA007;       ! Artificial point. Magnet BI1PI is separated into BI1PII and BI1PIO to give space for an Ionisation Profile Monitor. The IPM is inside BI1PI, which is really only one magnet (together with BI1PM and BI1PO).
//---------------------- SEXTUPOLE      ---------------------------------------------
ER_MXNACIIP              : SEXTUPOLE   , L := l.ER_MXNACIIP;       ! Sextupole Magnet, type XN, LEIR
ER_MXSAAIIP              : SEXTUPOLE   , L := l.ER_MXSAAIIP;       ! Sextupole Magnet, type skew  XLS, LEIR
//---------------------- SOLENOID       ---------------------------------------------
ER_EC0                   : SOLENOID    , L := l.ER_EC0;            ! LEIR electron cooler slot, solenoid
ER_EC1                   : SOLENOID    , L := l.ER_EC1;            ! LEIR electron cooler slot, solenoid
ER_EC2                   : SOLENOID    , L := l.ER_EC2;            ! LEIR electron cooler slot, solenoid
ER_EC3                   : SOLENOID    , L := l.ER_EC3;            ! LEIR electron cooler slot, solenoid
ER_EC4                   : SOLENOID    , L := l.ER_EC4;            ! LEIR electron cooler slot, solenoid
ER_EC5H                  : SOLENOID    , L := l.ER_EC5H;           ! LEIR electron cooler slot, solenoid
ER_ML_BA_WC              : SOLENOID    , L := l.ER_ML_BA_WC;       ! Solenoid magnet, type 13, LEIR
//---------------------- VMONITOR       ---------------------------------------------
ER_UEV                   : VMONITOR    , L := l.ER_UEV;            ! Beam pick-up, vertical, circular dia 182mm, LEIR
ER_UEV__001              : VMONITOR    , L := l.ER_UEV__001;       ! Beam pick up, vertical, rectangular shape for main unit bending magnets, LEIR
ER_UEV__002              : VMONITOR    , L := l.ER_UEV__002;       ! Beam pick up, vertical, circular dia 140mm, inside electron cooler, LEIR


/************************************************************************************/
/*                       LEIR Ring SEQUENCE                                         */
/************************************************************************************/

ER : SEQUENCE, refer = centre,          L = 78.5437;
 ER.UQF11                      : ER_BPUAC               , at = .36134       , slot_id = 5980302;
 ER.DFH11                      : ER_MKDFH               , at = .70434       , slot_id = 5980059;
 ER.QDN11                      : ER_MQNEK004            , at = 1.309843     , slot_id = 5979754;
 ER.XDN11                      : ER_MXNACIIP            , at = 1.812343     , slot_id = 5979855;
 ER.QFN11                      : ER_MQNEK003            , at = 2.312343     , slot_id = 5979864;
 ER.XFN11                      : ER_MXNACIIP            , at = 2.842343     , slot_id = 5979867;
 ER.UEH11                      : ER_UEH                 , at = 3.572843     , slot_id = 5986360, assembly_id= 6030166;
 ER.UEV11                      : ER_UEV                 , at = 3.662843     , slot_id = 5986361, assembly_id= 6030166;
 ER.SMH11                      : ER_OMK                 , at = 4.623343     , slot_id = 5987644;
 ER.SEH10                      : ER_OMK                 , at = 6.476843     , slot_id = 5987642;
 ER.DHV12                      : ER_MCCARWIP            , at = 7.222843     , slot_id = 5980261;
 ER.UEH12                      : ER_UEH                 , at = 7.846343     , slot_id = 5987578;
 ER.UEV12                      : ER_UEV                 , at = 7.846343     , slot_id = 5987581;
 ER.MSIEVE12                   : ER_OMK                 , at = 8.036343     , slot_id = 5980263;
 ER.MTV12                      : ER_BTVLP006            , at = 8.266343     , slot_id = 5980262;
 ER.MTR12                      : ER_OMK                 , at = 8.879843     , slot_id = 5979938;
 ER.MTRF12                     : ER_OMK                 , at = 9.413843     , slot_id = 5979971;
 ER.XFN12                      : ER_MXNACIIP            , at = 10.111343    , slot_id = 5979868;
 ER.QFN12                      : ER_MQNEK003            , at = 10.641343    , slot_id = 5979865;
 ER.XDN12                      : ER_MXNACIIP            , at = 11.141343    , slot_id = 5979869;
 ER.QDN12                      : ER_MQNEK002            , at = 11.643843    , slot_id = 5979866;
 ER.DFH12                      : ER_MKDFH               , at = 12.249343    , slot_id = 5980060;
 ER.KQF12                      : ER_BPUAE               , at = 12.647343    , slot_id = 6029672;
 ER.BA1HO10                    : ER_MU2HA002            , at = 13.232896    , slot_id = 5986738, assembly_id= 5979962;
 ER.DWHV11                     : ER_DWHV                , at = 13.512106    , slot_id = 6029882, assembly_id= 5979962;
 ER.XFW11                      : ER_XFW                 , at = 13.512106    , slot_id = 5987585, assembly_id= 5979962;
 ER.BA1HI10                    : ER_MU2HA002            , at = 13.791316    , slot_id = 5986739, assembly_id= 5979962;
 ER.BI1PO10                    : ER_MU2HA003            , at = 14.284146    , slot_id = 5986743, assembly_id= 5979962;
 ER.UEV13                      : ER_UEV__001            , at = 14.482016    , slot_id = 5986366, assembly_id= 5979962;
 ER.BI1PM10                    : ER_MU2HA004            , at = 14.613016    , slot_id = 5986746, assembly_id= 5979962;
 ER.UEH13                      : ER_UEH__001            , at = 14.744016    , slot_id = 5986367, assembly_id= 5979962;
 ER.BI1PI10                    : ER_MU2HA005            , at = 16.162704    , slot_id = 5986749, assembly_id= 5979962;
 ER.BHN10                      : ER_MU2HACWP            , at = 16.294806    , slot_id = 5979962;
 ER.UCH10                      : ER_UCH                 , at = 16.294806    , slot_id = 6029904, assembly_id= 5979962;
 ER.BI2PI10                    : ER_MU2HA005            , at = 17.106696    , slot_id = 5986750, assembly_id= 5979962;
 ER.UEH14                      : ER_UEH__001            , at = 17.845596    , slot_id = 5986368, assembly_id= 5979962;
 ER.BI2PM10                    : ER_MU2HA004            , at = 17.976596    , slot_id = 5986747, assembly_id= 5979962;
 ER.UEV14                      : ER_UEV__001            , at = 18.107596    , slot_id = 5986369, assembly_id= 5979962;
 ER.BI2PO10                    : ER_MU2HA003            , at = 18.305466    , slot_id = 5986744, assembly_id= 5979962;
 ER.BA2HI10                    : ER_MU2HA002            , at = 18.798296    , slot_id = 5986740, assembly_id= 5979962;
 ER.DWHV12                     : ER_DWHV                , at = 19.077506    , slot_id = 6029883, assembly_id= 5979962;
 ER.XFW12                      : ER_XFW                 , at = 19.077506    , slot_id = 5987586, assembly_id= 5979962;
 ER.BA2HO10                    : ER_MU2HA002            , at = 19.356716    , slot_id = 5986741, assembly_id= 5979962;
 ER.KDHV21                     : ER_KDHV                , at = 19.954269    , slot_id = 6029679;
 ER.QFN21                      : ER_MQNEKFWP            , at = 20.438269    , slot_id = 5979871;
 ER.DFH21                      : ER_MKDFH               , at = 20.898269    , slot_id = 5980061;
 ER.QDN21                      : ER_MQNEK004            , at = 21.355769    , slot_id = 5979872;
 ER.QSK21                      : ER_MQSBANWP            , at = 21.862768    , slot_id = 5979933;
 ER.QFN23                      : ER_MQNEK002            , at = 22.360769    , slot_id = 5979873;
 ER.SOL21                      : ER_ML_BA_WC            , at = 22.988768    , slot_id = 5979968;
 ER.DEH21                      : ER_DEH                 , at = 23.553769    , slot_id = 56023376, assembly_id= 6030224;
 ER.DEV21                      : ER_DEV                 , at = 23.553769    , slot_id = 56023346, assembly_id= 6030224;
 ER.EC021                      : ER_EC0                 , at = 23.647704    , slot_id = 5986707;
 ER.ECQSI1                     : ER_ECQ                 , at = 23.741639    , slot_id = 5986720;
 ER.EC121                      : ER_EC1                 , at = 23.983894    , slot_id = 5986709;
 ER.ECDH1                      : ER_ECDH                , at = 24.226149    , slot_id = 5986725;
 ER.EC221                      : ER_EC2                 , at = 24.304039    , slot_id = 5986711;
 ER.ECQSI2                     : ER_ECQ                 , at = 24.381929    , slot_id = 5986721;
 ER.EC321                      : ER_EC3                 , at = 24.647849    , slot_id = 5986713;
 ER.UEV21                      : ER_UEV__002            , at = 24.913769    , slot_id = 5986370;
 ER.EC421                      : ER_EC4                 , at = 24.968769    , slot_id = 5986715;
 ER.UEH21                      : ER_UEH__002            , at = 25.023769    , slot_id = 5986371;
 ER.EC5H21                     : ER_EC5H                , at = 25.568269    , slot_id = 5986717;
 ER.EC5H22                     : ER_EC5H                , at = 26.657269    , slot_id = 5986718;
 ER.UEH22                      : ER_UEH__002            , at = 27.201769    , slot_id = 5986372;
 ER.EC422                      : ER_EC4                 , at = 27.256769    , slot_id = 5986716;
 ER.UEV22                      : ER_UEV__002            , at = 27.311769    , slot_id = 5986373;
 ER.EC322                      : ER_EC3                 , at = 27.577689    , slot_id = 5986714;
 ER.ECQS02                     : ER_ECQ                 , at = 27.843609    , slot_id = 5986722;
 ER.EC222                      : ER_EC2                 , at = 27.921499    , slot_id = 5986712;
 ER.ECDH2                      : ER_ECDH                , at = 27.999389    , slot_id = 5986726;
 ER.EC122                      : ER_EC1                 , at = 28.241644    , slot_id = 5986710;
 ER.ECQS01                     : ER_ECQ                 , at = 28.483899    , slot_id = 5986723;
 ER.EC022                      : ER_EC0                 , at = 28.577834    , slot_id = 5986708;
 ER.DEH22                      : ER_DEH                 , at = 28.671769    , slot_id = 56023402, assembly_id= 6030225;
 ER.DEV22                      : ER_DEV                 , at = 28.671769    , slot_id = 56023427, assembly_id= 6030225;
 ER.SOL22                      : ER_ML_BA_WC            , at = 29.236768    , slot_id = 5979969;
 ER.QFN24                      : ER_MQNEK004            , at = 29.864769    , slot_id = 5979874;
 ER.QSK22                      : ER_MQSBANWP            , at = 30.362768    , slot_id = 5979934;
 ER.QDN22                      : ER_MQNEK002            , at = 30.869769    , slot_id = 5979875;
 ER.QFN22I                     : ER_MQNEK006            , at = 31.615719    , slot_id = 5986325, assembly_id= 5986322;
 ER.UCV22                      : ER_UCV                 , at = 31.702768    , slot_id = 5980387;
 ER.QFN22                      : ER_MQNEK005            , at = 31.787269    , slot_id = 5986322;
 ER.QFN22O                     : ER_MQNEK007            , at = 31.874319    , slot_id = 5986329, assembly_id= 5986322;
 ER.BA1HO20                    : ER_MU2HA002            , at = 32.868821    , slot_id = 5986966, assembly_id= 5979964;
 ER.DWHV21                     : ER_DWHV                , at = 33.148031    , slot_id = 6029884, assembly_id= 5979964;
 ER.XFW21                      : ER_XFW                 , at = 33.148031    , slot_id = 5987591, assembly_id= 5979964;
 ER.BA1HI20                    : ER_MU2HA002            , at = 33.427241    , slot_id = 5986967, assembly_id= 5979964;
 ER.BI1PO20                    : ER_MU2HA003            , at = 33.920071    , slot_id = 5986970, assembly_id= 5979964;
 ER.UEV23                      : ER_UEV__001            , at = 34.117941    , slot_id = 5986675, assembly_id= 5979964;
 ER.BI1PM20                    : ER_MU2HA004            , at = 34.248941    , slot_id = 5986972, assembly_id= 5979964;
 ER.UEH23                      : ER_UEH__001            , at = 34.379941    , slot_id = 5986683, assembly_id= 5979964;
 ER.BI1PI20                    : ER_MU2HA005            , at = 35.118841    , slot_id = 5986974, assembly_id= 5979964;
 ER.BHN20                      : ER_MU2HACWP            , at = 35.930731    , slot_id = 5979964;
 ER.BI2PI20                    : ER_MU2HA005            , at = 36.742621    , slot_id = 5986975, assembly_id= 5979964;
 ER.UEH24                      : ER_UEH__001            , at = 37.481521    , slot_id = 5986684, assembly_id= 5979964;
 ER.BI2PM20                    : ER_MU2HA004            , at = 37.612521    , slot_id = 5986973, assembly_id= 5979964;
 ER.UEV24                      : ER_UEV__001            , at = 37.743521    , slot_id = 5986676, assembly_id= 5979964;
 ER.BI2PO20                    : ER_MU2HA003            , at = 37.941391    , slot_id = 5986971, assembly_id= 5979964;
 ER.BA2HI20                    : ER_MU2HA002            , at = 38.434221    , slot_id = 5986968, assembly_id= 5979964;
 ER.DWHV22                     : ER_DWHV                , at = 38.713431    , slot_id = 6029885, assembly_id= 5979964;
 ER.XFW22                      : ER_XFW                 , at = 38.713431    , slot_id = 5987592, assembly_id= 5979964;
 ER.BA2HO20                    : ER_MU2HA002            , at = 38.992641    , slot_id = 5986969, assembly_id= 5979964;
 ER.UWB31                      : ER_UWB__001            , at = 39.710851    , slot_id = 6030227;
 ER.KQF31                      : ER_KQF                 , at = 40.001851    , slot_id = 6030229;
 ER.QDN31                      : ER_MQNEK004            , at = 40.581694    , slot_id = 5979921;
 ER.XDN31                      : ER_MXNACIIP            , at = 41.084194    , slot_id = 5979870;
 ER.QFN31                      : ER_MQNEK003            , at = 41.584194    , slot_id = 5979922;
 ER.XFN31                      : ER_MXNACIIP            , at = 41.924194    , slot_id = 5979929;
 ER.KFH31                      : ER_MKKFH001            , at = 42.752694    , slot_id = 5980086;
 ER.UEH31                      : ER_UEH                 , at = 43.175194    , slot_id = 5986362, assembly_id= 6030248;
 ER.UEV31                      : ER_UEV                 , at = 43.265194    , slot_id = 5986363, assembly_id= 6030248;
 ER.DHV31                      : ER_MCCARWIP            , at = 43.998694    , slot_id = 5979956;
 ER.UEH32                      : ER_UEH                 , at = 47.437194    , slot_id = 5986364, assembly_id= 6079970;
 ER.UEV32                      : ER_UEV                 , at = 47.527194    , slot_id = 5986365, assembly_id= 6079970;
 ER.KFH3234                    : ER_LMKFH               , at = 48.507194    , slot_id = 5987173;
 ER.XFN32                      : ER_MXNACIIP            , at = 49.383194    , slot_id = 5979930;
 ER.QFN32                      : ER_MQNEK003            , at = 49.913194    , slot_id = 5979923;
 ER.XDN32                      : ER_MXNACIIP            , at = 50.413194    , slot_id = 5979931;
 ER.QDN32                      : ER_MQNEK002            , at = 50.915694    , slot_id = 5979924;
 ER.UCV32                      : ER_UCV__001            , at = 51.707194    , slot_id = 6030239;
 ER.BA1HO30                    : ER_MU2HA002            , at = 52.504747    , slot_id = 5987207, assembly_id= 5979965;
 ER.DWHV31                     : ER_DWHV                , at = 52.783957    , slot_id = 6029886, assembly_id= 5979965;
 ER.XFW31                      : ER_XFW                 , at = 52.783957    , slot_id = 5987595, assembly_id= 5979965;
 ER.BA1HI30                    : ER_MU2HA002            , at = 53.063167    , slot_id = 5987208, assembly_id= 5979965;
 ER.BI1PO30                    : ER_MU2HA003            , at = 53.555997    , slot_id = 5987241, assembly_id= 5979965;
 ER.UEV33                      : ER_UEV__001            , at = 53.753867    , slot_id = 5986677, assembly_id= 5979965;
 ER.BI1PM30                    : ER_MU2HA004            , at = 53.884867    , slot_id = 5987251, assembly_id= 5979965;
 ER.UEH33                      : ER_UEH__001            , at = 54.015867    , slot_id = 5986685, assembly_id= 5979965;
 ER.BI1PI30                    : ER_MU2HA005            , at = 55.434555    , slot_id = 5987261, assembly_id= 5979965;
 ER.BHN30                      : ER_MU2HACWP            , at = 55.566657    , slot_id = 5979965;
 ER.BI2PI30                    : ER_MU2HA005            , at = 55.698759    , slot_id = 5987263, assembly_id= 5979965;
 ER.UEH34                      : ER_UEH__001            , at = 57.117447    , slot_id = 5986686, assembly_id= 5979965;
 ER.BI2PM30                    : ER_MU2HA004            , at = 57.248447    , slot_id = 5987253, assembly_id= 5979965;
 ER.UEV34                      : ER_UEV__001            , at = 57.379447    , slot_id = 5986678, assembly_id= 5979965;
 ER.BI2PO30                    : ER_MU2HA003            , at = 57.577317    , slot_id = 5987243, assembly_id= 5979965;
 ER.BA2HI30                    : ER_MU2HA002            , at = 58.070147    , slot_id = 5987209, assembly_id= 5979965;
 ER.DWHV32                     : ER_DWHV                , at = 58.349357    , slot_id = 6029887, assembly_id= 5979965;
 ER.XFW32                      : ER_XFW                 , at = 58.349357    , slot_id = 5987596, assembly_id= 5979965;
 ER.BA2HO30                    : ER_MU2HA002            , at = 58.628567    , slot_id = 5987210, assembly_id= 5979965;
 ER.KDHV41                     : ER_KDHV                , at = 59.067777    , slot_id = 6029680;
 ER.QFN41I                     : ER_MQNEK008            , at = 59.58082     , slot_id = 5986343, assembly_id= 5986344;
 ER.QFN41                      : ER_MQNEK005            , at = 59.71012     , slot_id = 5986344;
 ER.UDHV41                     : ER_UDHV                , at = 59.71012     , slot_id = 6030241;
 ER.QFN41O                     : ER_MQNEK008            , at = 59.83942     , slot_id = 5986347, assembly_id= 5986344;
 ER.QDN41                      : ER_MQNEK004            , at = 60.62762     , slot_id = 5979925;
 ER.XFLS41                     : ER_MXSAAIIP            , at = 61.13012     , slot_id = 5979959;
 ER.QFN43                      : ER_MQNEK002            , at = 61.63262     , slot_id = 5979926;
 ER.UEH41                      : ER_UEH                 , at = 62.26412     , slot_id = 5986687;
 ER.UEV41                      : ER_UEV                 , at = 62.35412     , slot_id = 5986679;
 ER.DHV41                      : ER_MCCARWIP            , at = 62.57612     , slot_id = 5979972;
 ER.CRF41                      : ER_OMK                 , at = 63.19812     , slot_id = 5980088;
 ER.CRF43                      : ER_OMK                 , at = 64.29812     , slot_id = 5980089;
 ER.DHV42                      : ER_MCCARWIP            , at = 67.12712     , slot_id = 5979957;
 ER.UEV42                      : ER_UEV                 , at = 67.34912     , slot_id = 5986680;
 ER.UEH42                      : ER_UEH                 , at = 67.43912     , slot_id = 5986688;
 ER.MPIV42                     : ER_BIPM                , at = 67.65112     , slot_id = 6029676;
 ER.VCT42                      : ER_OMK                 , at = 68.03912     , slot_id = 5987489;
 ER.QFN44                      : ER_MQNEK004            , at = 69.13662     , slot_id = 5979927;
 ER.XFLS42                     : ER_MXSAAIIP            , at = 69.63912     , slot_id = 5979960;
 ER.QDN42                      : ER_MQNEK002            , at = 70.14162     , slot_id = 5979928;
 ER.DFH42                      : ER_MKDFH               , at = 70.59912     , slot_id = 5980062;
 ER.QFN42I                     : ER_MQNEK008            , at = 70.929745    , slot_id = 5986350, assembly_id= 5986323;
 ER.QFN42                      : ER_MQNEK005            , at = 71.05912     , slot_id = 5986323;
 ER.UDHV42                     : ER_UDHV                , at = 71.05912     , slot_id = 6030242;
 ER.QFN42O                     : ER_MQNEK008            , at = 71.18842     , slot_id = 5986351, assembly_id= 5986323;
 ER.KDHV42                     : ER_KDHV                , at = 71.701463    , slot_id = 6029681;
 ER.BA1HO40                    : ER_MU2HA002            , at = 72.140673    , slot_id = 5987215, assembly_id= 5979966;
 ER.DWHV41                     : ER_DWHV                , at = 72.419883    , slot_id = 6029888, assembly_id= 5979966;
 ER.XFW41                      : ER_XFW                 , at = 72.419883    , slot_id = 5987599, assembly_id= 5979966;
 ER.BA1HI40                    : ER_MU2HA002            , at = 72.699093    , slot_id = 5987218, assembly_id= 5979966;
 ER.BI1PO40                    : ER_MU2HA003            , at = 73.191923    , slot_id = 5987245, assembly_id= 5979966;
 ER.UEV43                      : ER_UEV__001            , at = 73.389793    , slot_id = 5986681, assembly_id= 5979966;
 ER.BI1PM40                    : ER_MU2HA004            , at = 73.520793    , slot_id = 5987255, assembly_id= 5979966;
 ER.UEH43                      : ER_UEH__001            , at = 73.651793    , slot_id = 5986689, assembly_id= 5979966;
 ER.BI1PIO40                   : ER_MU2HA006            , at = 73.740693    , slot_id = 5987282, assembly_id= 5979966;
 ER.MPIH41                     : ER_BIPM                , at = 73.829593    , slot_id = 6029677, assembly_id= 5979966;
 ER.BI1PII40                   : ER_MU2HA007            , at = 74.479593    , slot_id = 5987286, assembly_id= 5979966;
 ER.BHN40                      : ER_MU2HACWP            , at = 75.202582    , slot_id = 5979966;
 ER.UCH40                      : ER_UCH                 , at = 75.202583    , slot_id = 6029903, assembly_id= 5979966;
 ER.BI2PI40                    : ER_MU2HA005            , at = 76.014473    , slot_id = 5987266, assembly_id= 5979966;
 ER.UEH44                      : ER_UEH__001            , at = 76.753373    , slot_id = 5986690, assembly_id= 5979966;
 ER.BI2PM40                    : ER_MU2HA004            , at = 76.884373    , slot_id = 5987258, assembly_id= 5979966;
 ER.UEV44                      : ER_UEV__001            , at = 77.015373    , slot_id = 5986682, assembly_id= 5979966;
 ER.BI2PO40                    : ER_MU2HA003            , at = 77.213243    , slot_id = 5987248, assembly_id= 5979966;
 ER.BA2HI40                    : ER_MU2HA002            , at = 77.706073    , slot_id = 5987221, assembly_id= 5979966;
 ER.DWHV42                     : ER_DWHV                , at = 77.985283    , slot_id = 6029889, assembly_id= 5979966;
 ER.XFW42                      : ER_XFW                 , at = 77.985283    , slot_id = 5987600, assembly_id= 5979966;
 ER.BA2HO40                    : ER_MU2HA002            , at = 78.264493    , slot_id = 5987224, assembly_id= 5979966;
ENDSEQUENCE;

return;