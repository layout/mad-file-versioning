#!/usr/bin/python

import sys
from pathlib import Path

from cpymad.madx import Madx

def trycall(mad, filename):
    try:
        mad.call(str(filename))
    except RuntimeError as er:
        print(f"MAD-X error: Calling `{filename}` generated error")
        sys.exit(0)


def tryinput(mad, cmds):
    for cmd in cmds.split(";"):
        try:
            mad.input(cmd)
        except RuntimeError as er:
            print(f"MAD-X error: Executing `{cmd}` generated error")
            sys.exit(0)

def copy_table(tt, cols):
    out = {}
    for cc in cols:
        out[cc] = tt[cc]
    return out


class PsModel:

    def __init__(self, seqpath):
        self.seqpath = Path(seqpath)
        self.strpath = (
            self.seqpath.parent / "tests" / "ps_pro_bare_machine.str"
        )
        if not self.seqpath.exists():
            print(f"File {self.seqpath} not found")

    def make_twiss(self, mad):
        cols = ["name","keyword","s", "x", "y", "betx", "bety", "dx", "dy","mux","muy"]# + apcoll
        mad.select(
            flag="twiss",
            column=cols,
        )
        mad.use("ps")
        t = copy_table(self.mad.twiss(), cols)
        return t

    def check_optics(self, mad):
        tw = dict(zip("12", self.make_twiss(self.mad)))
        qt = {}
        q0 = {"x1": 62.31, "y1": 60.32, "x2": 62.31, "y2": 60.32}

        for b12 in "12":
            for xy in "xy":
                qt[xy + b12] = tw[b12]["mu" + xy][-1]

        for b12 in "12":
            for xy in "xy":
                diff = abs(qt[xy + b12] - q0[xy + b12])
                if diff > 1e-6:
                    print(
                        f"Error: Q{xy} Beam{b12} {qt[xy+b12]:18.15f} differs from nominal {q0[xy+b12]} by {diff}"
                    )
        return tw

    def load_sequence(self, mad, seqname):
        trycall(mad, seqname)

        # if mad.globals.LHCLENGTH != 26658.8832:
        #     print(
        #         f"Error: LHC length {mad.globals.LHCLENGTH} m not correct, expecting 26658.8832 m."
        #     )

    def load_file(self, mad, filename):
        trycall(mad, filename)

    def make_beam(self, mad):
        mad.input(
            """
            BEAM, PARTICLE=PROTON, PC = 2.794987;
            BRHO      := BEAM->PC * 3.3356;
            """
        )
        tryinput(mad, "use,sequence=ps;")

    def make_survey(self, mad, x0=-2045.383636, y0=2433.66, z0=2008.722117, theta0=-4.256156465, phi0=0, psi0=0, file="survey.tfs"):
        '''Initial conditions according to https://issues.cern.ch/browse/E2A-74, extracted on 29.03.2023.'''

        sequence = "ps"
        mad.beam(sequence=sequence)
        mad.use(sequence)
        mad.input('set,  format="12.9f";')
        cols = "name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,tilt,mech_sep,assembly_id,slot_id".split(
            ","
        )
        mad.select(flag="survey", column=cols)
        mad.survey(
            sequence=sequence,
            file=file,
            x0=x0,
            z0=z0,
            y0=y0,
            theta0=theta0,
            phi0=phi0,
            psi0=psi0,
        )
        s = copy_table(mad.table.survey, cols)
        return s

    # def plot_beam(self):
    #     for ii, cc in enumerate("br"):
    #         s, sx, x, sigx, ox, ax, idx = self.sizes[ii]
    #         import matplotlib.pyplot as plt
    #
    #         plt.fill_between(
    #             s[idx], (ox - ax + sx)[idx], (ox + ax + sx)[idx], alpha=0.5, color="k"
    #         )
    #         plt.plot(s[idx], (ox - ax + sx)[idx], color="k")
    #         plt.plot(s[idx], (ox + ax + sx)[idx], color="k")
    #         plt.fill_between(s, x - sigx + sx, x + sigx + sx, alpha=0.5, color=cc)

    def make_seqedit(self):
        MUseqs = [k for k in self.mad.sequence.keys() if "pr.bh" in k]
        MUlen = self.mad.sequence.get("pr.bht01")._get_length_parameter().value
        angle = 0.03147300489 + 0.03135884818
        self.mad.input(f"PR.BH: SBEND, L = {MUlen}, ANGLE = {angle} ")

        for seq in MUseqs:
            elements = self.mad.sequence.get(seq).element_names()
            self.mad.input(f"seqedit, sequence = PR.BH{seq[-3:].upper()}")
            for element in elements:
                if  ("pr.mp" in element) | ("pr.dhz" in element) | (element.endswith(".f")) | (element.endswith(".d")):
                    self.mad.input(f"remove, element =  {element.upper()}")
                    assembly_id = self.mad.elements.get(element).get("assembly_id")
            self.mad.input(f"PR.BH{seq[-3:].upper()}: PR.BH, at = 0.0, assembly_id = {assembly_id}")
            self.mad.input(f"install, element = PR.BH{seq[-3:].upper()}, at = {MUlen/2}")
            self.mad.input("endedit")

    def run_tests(self):
        # run madx
        self.mad = Madx(stdout=False, stderr=sys.stdout)

        # make beam
        self.make_beam(self.mad)

        # load sequence of main unit and straight section elements
        self.load_sequence(self.mad, self.seqpath)

        if self.strpath.exists():
            self.load_file(self.mad, self.strpath)
            #TODO: add test based on Twiss output
            self.twiss = self.make_twiss(self.mad)

        sudir = self.seqpath.parent / f"SURVEY_{self.seqpath.stem}"

        if not sudir.exists():
            sudir.mkdir()
        self.make_seqedit()
        self.survey = self.make_survey(self.mad, file=str(sudir / f"ps_survey_{self.seqpath.stem}.tfs"))

    def close(self):
        self.mad.exit()


if __name__ == "__main__":
    seqpath = Path(sys.argv[1])
    ps = PsModel(seqpath)
    ps.run_tests()
    #ps.plot_beam()
    ps.close()
