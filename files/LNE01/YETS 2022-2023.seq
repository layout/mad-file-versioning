

  /**********************************************************************************
  *
  * LNE01 version (draft) YETS 2022-2023 in MAD X SEQUENCE format
  * Generated the 07-NOV-2024 03:05:14 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE01_BSGWAMEC001            := 0.3;
l.LNE01_BSGWAMFX001            := 0.17145;
l.LNE01_VVGAG                  := 0.08;
l.LNE01_VVGBF                  := 0.075;
l.LNE01_ZCH                    := 0.037;
l.LNE01_ZCV                    := 0.037;
l.LNE01_ZDFHR                  := 0.4;
l.LNE01_ZDFV                   := 0;
l.LNE01_ZDSHR002               := 0.347251;
l.LNE01_ZDSV                   := 0;
l.LNE01_ZQ                     := 0.1;
l.LNE01_ZQFD                   := 0.1;
l.LNE01_ZQFF                   := 0.1;
l.LNE01_ZQMD                   := 0.1;
l.LNE01_ZQMF                   := 0.1;
l.LNE01_ZQNA                   := 0.39;

//---------------------- HCORRECTOR     ---------------------------------------------
LNE01_ZCH                : HCORRECTOR  , L := l.LNE01_ZCH;         ! Electrostatic Corrector, Horizontal
LNE01_ZDFHR              : HCORRECTOR  , L := l.LNE01_ZDFHR;       ! Electrostatic Deflector, Fast pulsed, Horizontal, Right
LNE01_ZDSHR002           : HCORRECTOR  , L := l.LNE01_ZDSHR002;    ! Electrostatic Deflector, Slow pulsed, Horizontal, Right, 45.765 deg
//---------------------- INSTRUMENT     ---------------------------------------------
LNE01_BSGWAMEC001        : INSTRUMENT  , L := l.LNE01_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
LNE01_BSGWAMFX001        : INSTRUMENT  , L := l.LNE01_BSGWAMFX001; ! SEM Grid Fixed Mechanical Assembly
LNE01_VVGAG              : INSTRUMENT  , L := l.LNE01_VVGAG;       ! Vacuum valve gate elastomer metal body, electropneumatic controlled, DN200-CFF200 (VAT-S10)
LNE01_VVGBF              : INSTRUMENT  , L := l.LNE01_VVGBF;       ! Vacuum valve gate all metal, electropneumatic controlled, DN63-CFF63 (VAT-S48)
//---------------------- KICKER         ---------------------------------------------
LNE01_ZDFV               : KICKER      , L := l.LNE01_ZDFV;        ! Electrostatic Deflector, Fast pulsed, Vertical
LNE01_ZDSV               : KICKER      , L := l.LNE01_ZDSV;        ! Electrostatic Deflector, Slow pulsed, Vertical
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE01_ZQ                 : QUADRUPOLE  , L := l.LNE01_ZQ;          ! Electrostatic Quadrupoles
LNE01_ZQFD               : QUADRUPOLE  , L := l.LNE01_ZQFD;        ! Electrostatic Quadrupoles, powered in series, Defocussing
LNE01_ZQFF               : QUADRUPOLE  , L := l.LNE01_ZQFF;        ! Electrostatic Quadrupoles, powered in series, Focussing
LNE01_ZQMD               : QUADRUPOLE  , L := l.LNE01_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE01_ZQMF               : QUADRUPOLE  , L := l.LNE01_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE01_ZQNA               : QUADRUPOLE  , Lrad := l.LNE01_ZQNA;        ! Electrostatic Quadrupole, Normal, type A
//---------------------- VCORRECTOR     ---------------------------------------------
LNE01_ZCV                : VCORRECTOR  , L := l.LNE01_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE01 SEQUENCE                                              */
/************************************************************************************/

LNE01 : SEQUENCE, refer = centre,       L = 15.390137;
 LNE.ZQFF.0101                 : LNE01_ZQFF             , at = 1.388969     , slot_id = 52779615, assembly_id= 52779598;
 LNE.ZCV.0101                  : LNE01_ZCV              , at = 1.487469     , slot_id = 52779616, assembly_id= 52779598;
 LNE.ZQNA.0101                 : LNE01_ZQNA             , at = 1.511469     , slot_id = 52779598;
 LNE.ZCH.0101                  : LNE01_ZCH              , at = 1.534469     , slot_id = 52779617, assembly_id= 52779598;
 LNE.ZQ.0102                   : LNE01_ZQ               , at = 1.632969     , slot_id = 52779618, assembly_id= 52779598;
 LNE.ZQFD.0107                 : LNE01_ZQFD             , at = 3.206669     , slot_id = 52779696, assembly_id= 52779681;
 LNE.ZCV.0107                  : LNE01_ZCV              , at = 3.305169     , slot_id = 52779697, assembly_id= 52779681;
 LNE.ZQNA.0107                 : LNE01_ZQNA             , at = 3.329169     , slot_id = 52779681;
 LNE.ZCH.0107                  : LNE01_ZCH              , at = 3.352169     , slot_id = 52779698, assembly_id= 52779681;
 LNE.ZQ.0108                   : LNE01_ZQ               , at = 3.450669     , slot_id = 52779699, assembly_id= 52779681;
 LNE.BSGWA.0109                : LNE01_BSGWAMEC001      , at = 3.674169     , slot_id = 52779756;
 LNE.VV.0111                   : LNE01_VVGBF            , at = 3.946669     , slot_id = 52779780;
 LNE.ZQFF.0114                 : LNE01_ZQFF             , at = 4.566169     , slot_id = 52779806, assembly_id= 52779791;
 LNE.ZCV.0114                  : LNE01_ZCV              , at = 4.664669     , slot_id = 52779807, assembly_id= 52779791;
 LNE.ZQNA.0114                 : LNE01_ZQNA             , at = 4.688669     , slot_id = 52779791;
 LNE.ZCH.0114                  : LNE01_ZCH              , at = 4.711669     , slot_id = 52779808, assembly_id= 52779791;
 LNE.ZQ.0115                   : LNE01_ZQ               , at = 4.810169     , slot_id = 52779809, assembly_id= 52779791;
 LNE.BSGWA.0120                : LNE01_BSGWAMEC001      , at = 5.649669     , slot_id = 52779866;
 LNE.ZQ.0121                   : LNE01_ZQ               , at = 5.872169     , slot_id = 52779905, assembly_id= 52779890;
 LNE.ZCV.0121                  : LNE01_ZCV              , at = 5.970669     , slot_id = 52779906, assembly_id= 52779890;
 LNE.ZQNA.0121                 : LNE01_ZQNA             , at = 5.994669     , slot_id = 52779890;
 LNE.ZCH.0121                  : LNE01_ZCH              , at = 6.017669     , slot_id = 52779907, assembly_id= 52779890;
 LNE.ZQFD.0122                 : LNE01_ZQFD             , at = 6.116169     , slot_id = 52779908, assembly_id= 52779890;
 LNE.ZDFHR.0127                : LNE01_ZDFHR            , at = 6.562579     , slot_id = 52779991;
 LNE.ZDSHR.0132                : LNE01_ZDSHR002         , at = 7.1488135    , slot_id = 52780287;
 LNE.BSGWA.0137                : LNE01_BSGWAMEC001      , at = 7.728271     , slot_id = 52780339;
 LNE.ZQMF.0138                 : LNE01_ZQMF             , at = 7.950771     , slot_id = 52780388, assembly_id= 52780373;
 LNE.ZCV.0138                  : LNE01_ZCV              , at = 8.049271     , slot_id = 52780389, assembly_id= 52780373;
 LNE.ZQNA.0138                 : LNE01_ZQNA             , at = 8.073271     , slot_id = 52780373;
 LNE.ZCH.0138                  : LNE01_ZCH              , at = 8.096271     , slot_id = 52780390, assembly_id= 52780373;
 LNE.ZQMD.0139                 : LNE01_ZQMD             , at = 8.194771     , slot_id = 52780391, assembly_id= 52780373;
 LNE.VV.0141                   : LNE01_VVGBF            , at = 8.390771     , slot_id = 52780470;
 LNE.ZQMD.0145                 : LNE01_ZQMD             , at = 8.982371     , slot_id = 52780520, assembly_id= 52780505;
 LNE.ZCV.0145                  : LNE01_ZCV              , at = 9.080871     , slot_id = 52780521, assembly_id= 52780505;
 LNE.ZQNA.0145                 : LNE01_ZQNA             , at = 9.104871     , slot_id = 52780505;
 LNE.ZCH.0145                  : LNE01_ZCH              , at = 9.127871     , slot_id = 52780522, assembly_id= 52780505;
 LNE.ZQMF.0146                 : LNE01_ZQMF             , at = 9.226371     , slot_id = 52780523, assembly_id= 52780505;
 LNE.BSGWA.0151                : LNE01_BSGWAMEC001      , at = 9.940951     , slot_id = 52780580;
 LNE.ZQFD.0152                 : LNE01_ZQFD             , at = 10.163451    , slot_id = 52780619, assembly_id= 52780604;
 LNE.ZCV.0152                  : LNE01_ZCV              , at = 10.261951    , slot_id = 52780620, assembly_id= 52780604;
 LNE.ZQNA.0152                 : LNE01_ZQNA             , at = 10.285951    , slot_id = 52780604;
 LNE.ZCH.0152                  : LNE01_ZCH              , at = 10.308951    , slot_id = 52780621, assembly_id= 52780604;
 LNE.ZQ.0153                   : LNE01_ZQ               , at = 10.407451    , slot_id = 52780622, assembly_id= 52780604;
 LNE.BSGWA.0158                : LNE01_BSGWAMEC001      , at = 11.246951    , slot_id = 52780679;
 LNE.ZQ.0159                   : LNE01_ZQ               , at = 11.469451    , slot_id = 52780718, assembly_id= 52780703;
 LNE.ZCV.0159                  : LNE01_ZCV              , at = 11.567951    , slot_id = 52780719, assembly_id= 52780703;
 LNE.ZQNA.0159                 : LNE01_ZQNA             , at = 11.591951    , slot_id = 52780703;
 LNE.ZCH.0159                  : LNE01_ZCH              , at = 11.614951    , slot_id = 52780720, assembly_id= 52780703;
 LNE.ZQFF.0160                 : LNE01_ZQFF             , at = 11.713451    , slot_id = 52780721, assembly_id= 52780703;
 LNE.ZDFV.0165                 : LNE01_ZDFV             , at = 12.213222    , slot_id = 52780778;
 LNE.ZDSV.0170                 : LNE01_ZDSV             , at = 12.9602965   , slot_id = 52780793;
 LNE.ZQMF.0175                 : LNE01_ZQMF             , at = 13.606432    , slot_id = 52780823, assembly_id= 52780808;
 LNE.ZCV.0175                  : LNE01_ZCV              , at = 13.704932    , slot_id = 52780824, assembly_id= 52780808;
 LNE.ZQNA.0175                 : LNE01_ZQNA             , at = 13.728932    , slot_id = 52780808;
 LNE.ZCH.0175                  : LNE01_ZCH              , at = 13.751932    , slot_id = 52780825, assembly_id= 52780808;
 LNE.ZQMF.0176                 : LNE01_ZQMF             , at = 13.850432    , slot_id = 52780826, assembly_id= 52780808;
 LNE.BSGWA.0181                : LNE01_BSGWAMFX001      , at = 14.009657    , slot_id = 52780883;
 LNE.ZQMD.0182                 : LNE01_ZQMD             , at = 14.167882    , slot_id = 52780922, assembly_id= 52780907;
 LNE.ZCV.0182                  : LNE01_ZCV              , at = 14.266382    , slot_id = 52780923, assembly_id= 52780907;
 LNE.ZQNA.0182                 : LNE01_ZQNA             , at = 14.290382    , slot_id = 52780907;
 LNE.ZCH.0182                  : LNE01_ZCH              , at = 14.313382    , slot_id = 52780924, assembly_id= 52780907;
 LNE.ZQMF.0183                 : LNE01_ZQMF             , at = 14.411882    , slot_id = 52780925, assembly_id= 52780907;
 LNE.VV.0183                   : LNE01_VVGAG            , at = 14.525242    , slot_id = 52780982;
ENDSEQUENCE;

return;