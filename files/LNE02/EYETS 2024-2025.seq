

  /**********************************************************************************
  *
  * LNE02 version (draft) EYETS 2024-2025 in MAD X SEQUENCE format
  * Generated the 07-NOV-2024 03:06:12 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE02_BSGWAMEC001            := 0.3;
l.LNE02_BSGWAMFX001            := 0.17145;
l.LNE02_TBSLA001               := 0.15;
l.LNE02_VVGBH                  := 0.085;
l.LNE02_XXAEGIS                := 4.8;
l.LNE02_ZCH                    := 0.037;
l.LNE02_ZCV                    := 0.037;
l.LNE02_ZDSHR003               := 0;
l.LNE02_ZQ                     := 0.1;
l.LNE02_ZQFF                   := 0.1;
l.LNE02_ZQMD                   := 0.1;
l.LNE02_ZQMF                   := 0.1;
l.LNE02_ZQNA                   := 0.39;

//---------------------- COLLIMATOR     ---------------------------------------------
LNE02_TBSLA001           : COLLIMATOR  , L := l.LNE02_TBSLA001;    ! Beam Stopper, Low energy, type A
//---------------------- HCORRECTOR     ---------------------------------------------
LNE02_ZCH                : HCORRECTOR  , L := l.LNE02_ZCH;         ! Electrostatic Corrector, Horizontal
//---------------------- HKICKER        ---------------------------------------------
LNE02_ZDSHR003           : HKICKER     , L := l.LNE02_ZDSHR003;    ! Electrostatic Deflector, Slow pulsed, Horizontal, Right, 45.77 deg
//---------------------- INSTRUMENT     ---------------------------------------------
LNE02_BSGWAMEC001        : INSTRUMENT  , L := l.LNE02_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
LNE02_BSGWAMFX001        : INSTRUMENT  , L := l.LNE02_BSGWAMFX001; ! SEM Grid Fixed Mechanical Assembly
LNE02_VVGBH              : INSTRUMENT  , L := l.LNE02_VVGBH;       ! Vacuum valve gate all metal, electropneumatic controlled, DN100-CFF100 (VAT-S48)
LNE02_XXAEGIS            : INSTRUMENT  , L := l.LNE02_XXAEGIS;     ! ELENA Extraction - AEGIS Experiment
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE02_ZQ                 : QUADRUPOLE  , L := l.LNE02_ZQ;          ! Electrostatic Quadrupoles
LNE02_ZQFF               : QUADRUPOLE  , L := l.LNE02_ZQFF;        ! Electrostatic Quadrupoles, powered in series, Focussing
LNE02_ZQMD               : QUADRUPOLE  , L := l.LNE02_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE02_ZQMF               : QUADRUPOLE  , L := l.LNE02_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE02_ZQNA               : QUADRUPOLE  , Lrad := l.LNE02_ZQNA;        ! Electrostatic Quadrupole, Normal, type A
//---------------------- VCORRECTOR     ---------------------------------------------
LNE02_ZCV                : VCORRECTOR  , L := l.LNE02_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE02 SEQUENCE                                              */
/************************************************************************************/

LNE02 : SEQUENCE, refer = centre,       L = 7.653745;
 LNE.VV.0201                   : LNE02_VVGBH            , at = 1.2050903    , slot_id = 54948228;
 LNE.TBS.0203                  : LNE02_TBSLA001         , at = 1.4055903    , slot_id = 52767462;
 LNE.ZQFF.0205                 : LNE02_ZQFF             , at = 1.6380903    , slot_id = 52755612, assembly_id= 52755603;
 LNE.ZCV.0205                  : LNE02_ZCV              , at = 1.7365903    , slot_id = 52755613, assembly_id= 52755603;
 LNE.ZQNA.0205                 : LNE02_ZQNA             , at = 1.7605903    , slot_id = 52755603;
 LNE.ZCH.0205                  : LNE02_ZCH              , at = 1.7835903    , slot_id = 52755614, assembly_id= 52755603;
 LNE.ZQ.0206                   : LNE02_ZQ               , at = 1.8820903    , slot_id = 52755615, assembly_id= 52755603;
 LNE.BSGWA.0207                : LNE02_BSGWAMEC001      , at = 2.1055903    , slot_id = 52767435;
 LNE.ZQMD.0208                 : LNE02_ZQMD             , at = 3.78359      , slot_id = 52767893, assembly_id= 52767884;
 LNE.ZCV.0208                  : LNE02_ZCV              , at = 3.88209      , slot_id = 52767894, assembly_id= 52767884;
 LNE.ZQNA.0208                 : LNE02_ZQNA             , at = 3.90609      , slot_id = 52767884;
 LNE.ZCH.0208                  : LNE02_ZCH              , at = 3.92909      , slot_id = 52767895, assembly_id= 52767884;
 LNE.ZQMF.0209                 : LNE02_ZQMF             , at = 4.02759      , slot_id = 52767896, assembly_id= 52767884;
 LNE.ZQMD.0214                 : LNE02_ZQMD             , at = 4.29359      , slot_id = 52768095, assembly_id= 52768086;
 LNE.ZCV.0214                  : LNE02_ZCV              , at = 4.39209      , slot_id = 52768096, assembly_id= 52768086;
 LNE.ZQNA.0214                 : LNE02_ZQNA             , at = 4.41609      , slot_id = 52768086;
 LNE.ZCH.0214                  : LNE02_ZCH              , at = 4.43909      , slot_id = 52768097, assembly_id= 52768086;
 LNE.ZQMF.0215                 : LNE02_ZQMF             , at = 4.53759      , slot_id = 52768098, assembly_id= 52768086;
 LNE.ZDSHR.0220                : LNE02_ZDSHR003         , at = 5.071418     , slot_id = 52768162;
 LNE.BSGWA.0225                : LNE02_BSGWAMFX001      , at = 5.68266      , slot_id = 52768178;
 LNE.VV.0226                   : LNE02_VVGBH            , at = 5.97516      , slot_id = 52768202;
 LNE.AEGIS.0228                : LNE02_XXAEGIS          , at = 10.072746    , slot_id = 58612572;
ENDSEQUENCE;

return;