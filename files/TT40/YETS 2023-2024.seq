

  /**********************************************************************************
  *
  * TT40 version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 13-JUN-2023 03:11:54 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TT40_BCTFI                   := 0.482;
l.TT40_BPK                     := 0.45;
l.TT40_BTV                     := 0.45;
l.TT40_MBHA                    := 3.412002;
l.TT40_MBHC                    := 5.000004;
l.TT40_MDMH                    := 0.508;
l.TT40_MDMV                    := 0.508;
l.TT40_MDSV                    := 0.7;
l.TT40_QTLF                    := 2.99;
l.TT40_QTMD                    := 1.4;
l.TT40_QTRD                    := 2.99;
l.TT40_QTRF                    := 2.99;
l.TT40_TED                     := 4.3;
l.TT40_VVSA                    := 0.175;

//---------------------- HKICKER        ---------------------------------------------
TT40_MDMH      : HKICKER     , L := l.TT40_MDMH;         ! correcting dipole, BT line, horizontal steering
//---------------------- INSTRUMENT     ---------------------------------------------
TT40_BCTFI     : INSTRUMENT  , L := l.TT40_BCTFI;        ! Fast Beam Current Transformer for the Transfer Lines
TT40_BTV       : INSTRUMENT  , L := l.TT40_BTV;          ! light screen monitor
TT40_TED       : INSTRUMENT  , L := l.TT40_TED;          ! Beam Absorber for Injection, External
TT40_VVSA      : INSTRUMENT  , L := l.TT40_VVSA;         ! vacuum valve, sector, diameter 100 mm
//---------------------- MONITOR        ---------------------------------------------
TT40_BPK       : MONITOR     , L := l.TT40_BPK;          ! beam position, K type (?)
//---------------------- QUADRUPOLE     ---------------------------------------------
TT40_QTLF      : QUADRUPOLE  , L := l.TT40_QTLF;         ! quadrupole, BT line, long, focussing
TT40_QTMD      : QUADRUPOLE  , L := l.TT40_QTMD;         ! quadrupole, BT line, defocussing
TT40_QTRD      : QUADRUPOLE  , L := l.TT40_QTRD;         ! quadrupole, BT line, reduced aperture, defocussing
TT40_QTRF      : QUADRUPOLE  , L := l.TT40_QTRF;         ! quadrupole, BT line, reduced aperture, focussing
//---------------------- RBEND          ---------------------------------------------
TT40_MBHA      : RBEND       , L := l.TT40_MBHA;         ! bending magnet, BT line, horizontal
TT40_MBHC      : RBEND       , L := l.TT40_MBHC;         ! Bending magnet, BT line, type MBHC
//---------------------- VKICKER        ---------------------------------------------
TT40_MDMV      : VKICKER     , L := l.TT40_MDMV;         ! correcting dipole, BT line, vertical steering
TT40_MDSV      : VKICKER     , L := l.TT40_MDSV;         ! Correcting dipole, BT line, short, vertical deflection


/************************************************************************************/
/*                      TT40 SEQUENCE                                               */
/************************************************************************************/

TTT40 : SEQUENCE, refer = centre,       L = 111.472521;
 VVSA.400000                   : TT40_VVSA       , at = -2.313       , slot_id = 13716452;
 MDMV.400097                   : TT40_MDMV       , at = 1.8105       , slot_id = 1603826;
 BPK.400099                    : TT40_BPK        , at = 2.4095       , slot_id = 1603827;
 QTMD.400100                   : TT40_QTMD       , at = 3.6045       , slot_id = 1603828;
 MDMH.400104                   : TT40_MDMH       , at = 4.7785       , slot_id = 1603829;
 BTV.400105                    : TT40_BTV        , at = 5.3775       , slot_id = 1603830;
 MBHC.400107                   : TT40_MBHC       , at = 8.604502     , slot_id = 1603831;
 MBHC.400118                   : TT40_MBHC       , at = 14.104506    , slot_id = 1603832;
 MBHC.400129                   : TT40_MBHC       , at = 19.60451     , slot_id = 1603833;
 QTRF.400200                   : TT40_QTRF       , at = 26.356512    , slot_id = 1603834;
 BPK.400207                    : TT40_BPK        , at = 28.579512    , slot_id = 1603835;
 BTV.400222                    : TT40_BTV        , at = 36.029512    , slot_id = 1603836;
 MDSV.400293                   : TT40_MDSV       , at = 71.263512    , slot_id = 1603837;
 QTRD.400300                   : TT40_QTRD       , at = 73.656512    , slot_id = 1603838;
 BPK.400307                    : TT40_BPK        , at = 75.879512    , slot_id = 1603839;
 MBHA.400309                   : TT40_MBHA       , at = 78.325513    , slot_id = 1603840;
 MBHA.400318                   : TT40_MBHA       , at = 82.437515    , slot_id = 1603841;
 MBHA.400326                   : TT40_MBHA       , at = 86.549517    , slot_id = 1603842;
 MBHA.400334                   : TT40_MBHA       , at = 90.661519    , slot_id = 1603843;
 BTV.400343                    : TT40_BTV        , at = 93.53552     , slot_id = 1603844;
 BCTFI.400344                  : TT40_BCTFI      , at = 94.11452     , slot_id = 1603845;
 TED.400354                    : TT40_TED        , at = 100.98752    , slot_id = 2081550;
 QTLF.400400                   : TT40_QTLF       , at = 108.262521   , slot_id = 1603846;
 BPK.400407                    : TT40_BPK        , at = 110.372521   , slot_id = 1603847;
ENDSEQUENCE;

return;