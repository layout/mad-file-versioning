

  /**********************************************************************************
  *
  * TT66 version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 28-JUL-2023 14:55:48 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TT66_BCTFA021                := 0;
l.TT66_BCTFA022                := 0;
l.TT66_BPCK                    := 0.45;
l.TT66_BPKG                    := 0;
l.TT66_BPM                     := 0;
l.TT66_BSTL                    := 0.86;
l.TT66_BTV                     := 0.45;
l.TT66_BTVHE                   := 0;
l.TT66_BTV__002                := 0;
l.TT66_MBB                     := 6.26;
l.TT66_MBE                     := 6.26;
l.TT66_MBS                     := 3;
l.TT66_MDAV                    := 1.4;
l.TT66_MDLH                    := 1.4;
l.TT66_MDLV                    := 1.4;
l.TT66_MDSH                    := 0.7;
l.TT66_MDSV                    := 0.7;
l.TT66_MSE                     := 2.38;
l.TT66_OMK                     := 0;
l.TT66_QTLD                    := 2.99;
l.TT66_QTLF                    := 2.99;
l.TT66_TED                     := 4.3;
l.TT66_TED__004                := 5.367;
l.TT66_TPSG4                   := 3.098;
l.TT66_VVSB                    := 0.175;
l.TT66_XMTBA                   := 1.95;
l.TT66_XMTBB                   := 1.95;
l.TT66_XMTBC                   := 1.95;

//---------------------- COLLIMATOR     ---------------------------------------------
TT66_TPSG4     : COLLIMATOR  , L := l.TT66_TPSG4;        ! SPS LSS4 MSE PROTECTIF SHIELD
//---------------------- HKICKER        ---------------------------------------------
TT66_MDSH      : HKICKER     , L := l.TT66_MDSH;         ! Correcting dipole, BT line, short, horizontal deflection
TT66_MSE       : HKICKER     , L := l.TT66_MSE;          ! septum magnet, extractor
//---------------------- INSTRUMENT     ---------------------------------------------
TT66_BCTFA021  : INSTRUMENT  , L := l.TT66_BCTFA021;     ! FAST BEAM CURRENT TRANSFORMER SPS WITH BELLOW
TT66_BCTFA022  : INSTRUMENT  , L := l.TT66_BCTFA022;     ! FAST CURRENT TRANSFORMER SPS WITHOUT BELLOW
TT66_BPKG      : INSTRUMENT  , L := l.TT66_BPKG;         ! beam position for T40 target
TT66_BSTL      : INSTRUMENT  , L := l.TT66_BSTL;         ! Test monitor double BTV and QUATRO
TT66_BTV       : INSTRUMENT  , L := l.TT66_BTV;          ! light screen monitor
TT66_BTVHE     : INSTRUMENT  , L := l.TT66_BTVHE;        ! BTV HIRADMAT Experiment
TT66_BTV__002  : INSTRUMENT  , L := l.TT66_BTV__002;     ! Beam Observation TV Monitors based on Screens
TT66_TED       : INSTRUMENT  , L := l.TT66_TED;          ! Beam Absorber for Injection, External
TT66_TED__004  : INSTRUMENT  , L := l.TT66_TED__004;     ! Dump with inert gas core protection
TT66_VVSB      : INSTRUMENT  , L := l.TT66_VVSB;         ! vacuum valve, sector, diameter 150 mm
TT66_XMTBA     : INSTRUMENT  , L := l.TT66_XMTBA;        ! HiRadMat Experimental Table A
TT66_XMTBB     : INSTRUMENT  , L := l.TT66_XMTBB;        ! HiRadMat Experimental Table B
TT66_XMTBC     : INSTRUMENT  , L := l.TT66_XMTBC;        ! HiRadMat Experimental Table C
//---------------------- MARKER         ---------------------------------------------
TT66_OMK       : MARKER      , L := l.TT66_OMK;          ! TT66 markers
//---------------------- MONITOR        ---------------------------------------------
TT66_BPCK      : MONITOR     , L := l.TT66_BPCK;         ! beam position, directional coupler, K type (?)
TT66_BPM       : MONITOR     , L := l.TT66_BPM;          ! Beam position monitor | Beam position monitor
//---------------------- QUADRUPOLE     ---------------------------------------------
TT66_QTLD      : QUADRUPOLE  , L := l.TT66_QTLD;         ! quadrupole, BT line, long, defocussing
TT66_QTLF      : QUADRUPOLE  , L := l.TT66_QTLF;         ! quadrupole, BT line, long, focussing
//---------------------- RBEND          ---------------------------------------------
TT66_MBB       : RBEND       , L := l.TT66_MBB;          ! Bending Magnet, main, type B2
TT66_MBE       : RBEND       , L := l.TT66_MBE;          ! Bending magnet, modified MBB for vertical deflection
TT66_MBS       : RBEND       , L := l.TT66_MBS;          ! Bending magnet, switch, fast pulsed
TT66_MDAV      : RBEND       , L := l.TT66_MDAV;         ! Correcting dipole, BT line, enlarged aperture, vertical deflection
TT66_MDLH      : RBEND       , L := l.TT66_MDLH;         ! Correcting dipole, BT line, long, horizontal deflection
TT66_MDLV      : RBEND       , L := l.TT66_MDLV;         ! Correcting dipole, BT line, long, vertical deflection
//---------------------- VKICKER        ---------------------------------------------
TT66_MDSV      : VKICKER     , L := l.TT66_MDSV;         ! Correcting dipole, BT line, short, vertical deflection


/************************************************************************************/
/*                      TT66 SEQUENCE                                               */
/************************************************************************************/

TT66 : SEQUENCE, refer = centre,        L = 428;
 MDAV.610013                   : TT66_MDAV       , at = 7.9448       , slot_id = 1619547;
 BPCK.610015                   : TT66_BPCK       , at = 9.3798       , slot_id = 1619548;
 BTV.610018                    : TT66_BTV        , at = 11.2758      , slot_id = 1619549;
 QTLD.610100                   : TT66_QTLD       , at = 13.3058      , slot_id = 1619550;
 MDLH.610104                   : TT66_MDLH       , at = 16.1208      , slot_id = 1619551;
 QTLF.610200                   : TT66_QTLF       , at = 48.8116      , slot_id = 1619553;
 MDLH.610206                   : TT66_MDLH       , at = 53.2221      , slot_id = 1619554;
 BPCK.610211                   : TT66_BPCK       , at = 56.9451      , slot_id = 1619555;
 BCTFI.610225                  : TT66_BCTFA021   , at = 68.3411      , slot_id = 56476109;
 BSTL.610227                   : TT66_BSTL       , at = 69.7121      , slot_id = 1619558;
 BTV.610252                    : TT66_BTV        , at = 89.1701      , slot_id = 1619559;
 QTLD.610300                   : TT66_QTLD       , at = 93.1731      , slot_id = 1619560;
 MDLV.610304                   : TT66_MDLV       , at = 96.1011      , slot_id = 1619561;
 BPCK.610312                   : TT66_BPCK       , at = 101.8931     , slot_id = 1619562;
 BTV.610317                    : TT66_BTV        , at = 105.5331     , slot_id = 1619563;
 TED.610321                    : TT66_TED        , at = 111.7536     , slot_id = 1619564;
 MDLH.610337                   : TT66_MDLH       , at = 120.9101     , slot_id = 1619565;
 BPCK.610340                   : TT66_BPCK       , at = 124.2351     , slot_id = 1619566;
 QTLF.610400                   : TT66_QTLF       , at = 126.4001     , slot_id = 1619567;
 MBB.610405                    : TT66_MBB        , at = 132.424109   , slot_id = 1619568;
 MBB.610413                    : TT66_MBB        , at = 139.064127   , slot_id = 1619569;
 MBE.610422                    : TT66_MBE        , at = 145.841145   , slot_id = 1619570;
 MBE.610430                    : TT66_MBE        , at = 152.481163   , slot_id = 1619571;
 MBE.610438                    : TT66_MBE        , at = 159.121181   , slot_id = 1619572;
 QTLD.610500                   : TT66_QTLD       , at = 164.76119    , slot_id = 1619573;
 MBE.610506                    : TT66_MBE        , at = 171.621199   , slot_id = 1619574;
 MBE.610514                    : TT66_MBE        , at = 178.261217   , slot_id = 1619575;
 MBB.610523                    : TT66_MBB        , at = 185.038235   , slot_id = 1619576;
 MBB.610531                    : TT66_MBB        , at = 191.678253   , slot_id = 1619577;
 BPCK.610539                   : TT66_BPCK       , at = 195.292661   , slot_id = 1619578;
 QTLF.610600                   : TT66_QTLF       , at = 197.909662   , slot_id = 1619579;
 MBS.660004                    : TT66_MBS        , at = 201.574499   , slot_id = 4305911;
 MBS.660008                    : TT66_MBS        , at = 205.214499   , slot_id = 4305912;
 MBS.660011                    : TT66_MBS        , at = 208.854499   , slot_id = 4305913;
 VVSB.660015                   : TT66_VVSB       , at = 210.955      , slot_id = 4337673;
 MBS.660024                    : TT66_MBS        , at = 221.317499   , slot_id = 4305914;
 MBS.660028                    : TT66_MBS        , at = 224.957499   , slot_id = 4305915;
 MBS.660031                    : TT66_MBS        , at = 228.597499   , slot_id = 4305916;
 MBS.660035                    : TT66_MBS        , at = 232.237499   , slot_id = 4305917;
 MBS.660038                    : TT66_MBS        , at = 235.877499   , slot_id = 4305918;
 QTLF.660100                   : TT66_QTLF       , at = 239.4055     , slot_id = 4305919;
 VVSB.660104                   : TT66_VVSB       , at = 241.3785     , slot_id = 5332501;
 BPM.660104                    : TT66_BPM        , at = 242.019      , slot_id = 4305920;
 BTV.660105                    : TT66_BTV__002   , at = 242.802      , slot_id = 4305921;
 MDSV.660106                   : TT66_MDSV       , at = 243.878      , slot_id = 4305922;
 MBB.660107                    : TT66_MBB        , at = 248          , slot_id = 4305923;
 MBB.660115                    : TT66_MBB        , at = 255.5        , slot_id = 4305924;
 QTLD.660200                   : TT66_QTLD       , at = 261.2574     , slot_id = 4305925;
 BPM.660204                    : TT66_BPM        , at = 263.5        , slot_id = 4305926;
 MBB.660205                    : TT66_MBB        , at = 267.5        , slot_id = 4305927;
 MBB.660213                    : TT66_MBB        , at = 275          , slot_id = 4305928;
 BTV.660221                    : TT66_BTV__002   , at = 280          , slot_id = 4305929;
 MDSV.660224                   : TT66_MDSV       , at = 283.2724     , slot_id = 4305930;
 BCTFI.660298                  : TT66_BCTFA022   , at = 357.8825     , slot_id = 56475790;
 QTLF.660300                   : TT66_QTLF       , at = 360.0905     , slot_id = 4305931;
 BPM.660305                    : TT66_BPM        , at = 363.0905     , slot_id = 4305932;
 MDLH.660306                   : TT66_MDLH       , at = 365.0905     , slot_id = 4305933;
 QTLD.660400                   : TT66_QTLD       , at = 373.2245     , slot_id = 4305934;
 QTLD.660404                   : TT66_QTLD       , at = 376.8905     , slot_id = 4305935;
 BPM.660408                    : TT66_BPM        , at = 379.3905     , slot_id = 4305936;
 MDSV.660409                   : TT66_MDSV       , at = 380.8905     , slot_id = 4305937;
 QTLF.660500                   : TT66_QTLF       , at = 385.8905     , slot_id = 4305938;
 QTLF.660504                   : TT66_QTLF       , at = 389.5565     , slot_id = 4305939;
 BPM.660508                    : TT66_BPM        , at = 392.0565     , slot_id = 4305940;
 MDSH.660509                   : TT66_MDSH       , at = 393.5565     , slot_id = 4305941;
 MDLV.660512                   : TT66_MDLV       , at = 396.8635     , slot_id = 4305942;
 BPM.660517                    : TT66_BPM        , at = 401.006      , slot_id = 4305944;
 BTV.660518                    : TT66_BTV__002   , at = 402.38       , slot_id = 4305945;
 XMTBA.660520.FOCALPOINT       : TT66_OMK        , at = 404.28       , slot_id = 56503691;
 XMTBA.660520                  : TT66_XMTBA      , at = 405.054      , slot_id = 56503691;
 BPKG.660524                   : TT66_BPKG       , at = 405.274      , slot_id = 16091536;
 BTV.660524                    : TT66_BTVHE      , at = 405.813      , slot_id = 16091537;
 TPSG.660520                   : TT66_TPSG4      , at = 405.865515   , slot_id = 57867105;
 XMTBB.660523.FOCALPOINT       : TT66_OMK        , at = 406.78       , slot_id = 56503642;
 XMTBB.660523                  : TT66_XMTBB      , at = 407.549      , slot_id = 56503642;
 MSE.660524                    : TT66_MSE        , at = 408.822515   , slot_id = 57867144;
 XMTBC.660526.FOCALPOINT       : TT66_OMK        , at = 409.78       , slot_id = 56503667;
 XMTBC.660526                  : TT66_XMTBC      , at = 410.271      , slot_id = 56503667;
 BPKG.660529                   : TT66_BPKG       , at = 413.480034   , slot_id = 4305946;
 TED.660529.WINDOWUPSTREAM     : TT66_OMK        , at = 414.3027     , slot_id = 56421056;
 TED.660529.CORESTART          : TT66_OMK        , at = 415.35       , slot_id = 56421056;
 TED.660529                    : TT66_TED__004   , at = 416.9665     , slot_id = 56421056;
ENDSEQUENCE;

return;