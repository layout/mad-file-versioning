#!/usr/bin/python

import sys
from pathlib import Path
import os
import io

from cpymad.madx import Madx
import numpy as np

stdout = io.StringIO()


def trycall(mad, filename):
    try:
        mad.call(str(filename))
    except RuntimeError as er:
        print(*stdout.getvalue().splitlines()[-3:], sep="\n")
        print(f"MAD-X error: Calling `{filename}` generated error")
        sys.exit(0)


def tryinput(mad, cmds):
    for cmd in cmds.split(";"):
        try:
            mad.input(cmd)
        except RuntimeError as er:
            print(*stdout.getvalue().splitlines()[-3:], sep="\n")
            print(f"MAD-X error: Executing `{cmd}` generated error")
            sys.exit(0)


def increase_circumference(mad):
    mad.set(format="25.9f")
    mad.input("real const spslength = 6911.5038+10;")


def displace(mad, eps):

    mad.call(file="sps/strengths/lhc_q26.str")
    ds = 0

    mad.seqedit(sequence="sps")

    for el in list(mad.sequence.sps.elements):
        if el.name in eps:
            ds += eps[el.name]
        elif el.parent.name in eps:
            ds += eps[el.parent.name]
        if el.base_type.name in [
            "rbend",
            "quadrupole",
            "sextupole",
            "octupole",
            "rfcavity",
            "monitor",
            "hmonitor",
            "vmonitor",
            "hkicker",
            "vkicker",
            "tkicker",
        ]:  # collimator, instrument
            print("move", el.name, "by", ds)
            mad.move(element=el.name, by=ds)
        elif el.name.startswith("end"):
            mad.move(element=el.name, by=ds)
            print("move", el.name, "by", ds)

    mad.endedit()


dsmb = 19.34e-6
# dsmb=0
eps = {
    "sps_mba": dsmb,
    "sps_mbb": dsmb,
    "mdh.11607": -74e-6,
    "mdv.21507": -100e-6,  # 47
    "mba.22030": 13e-6 + dsmb,
    "mdv.31307": -90e-6,  # -90
    "mba.32030": 15e-6 + dsmb,
    "mdv.41107": -98e-6,  # -98
    "mba.42030": 15e-6 + dsmb + 10e-3 * 0,
    "mba.42050": +dsmb - 10e-3 * 0,
    "mdv.50907": -100e-6,
    "mba.52030": 15e-6 + dsmb + 10e-3 * 0,
    "mba.52050": +dsmb - 10e-3 * 0,
    "mdh.60607": -100e-6,
    "mba.62030": 34e-6,
}
x0c = [-0.500e-6, 2.500e-6, 2.700e-6]
dir0 = [0.321e-6, 0.012e-6, +0.031e-6]


def copy_table(tt, cols):
    out = {}
    for cc in cols:
        out[cc] = tt[cc]
    return out


class SPSModel:
    def __init__(self, seqpath):
        self.seqpath = Path(seqpath)
        self.appath = self.seqpath.parent / f"APERTURE_{self.seqpath.stem}.seq"
        self.optpath = (
            self.seqpath.parent / "tests" / f"INJECTION_{self.seqpath.stem}.madx"
        )
        if not self.seqpath.exists():
            print(f"File {self.seqpath} not found")

        if not self.appath.exists():
            print(f"File {self.appath} not found")

    def make_twiss(self, mad):
        cols = [
            "name",
            "s",
            "x",
            "y",
            "betx",
            "bety",
            "dx",
            "dy",
            "mux",
            "muy",
        ]  # + apcoll
        mad.select(
            flag="twiss",
            column=cols,
        )
        mad.use("sps")
        t = copy_table(self.mad.twiss(), cols)
        return t

    def check_optics(self, mad):
        tw = dict(zip("12", self.make_twiss(self.mad)))
        qt = {}
        q0 = {"x1": 62.31, "y1": 60.32, "x2": 62.31, "y2": 60.32}

        for b12 in "12":
            for xy in "xy":
                qt[xy + b12] = tw[b12]["mu" + xy][-1]

        for b12 in "12":
            for xy in "xy":
                diff = abs(qt[xy + b12] - q0[xy + b12])
                if diff > 1e-6:
                    print(
                        f"Error: Q{xy} Beam{b12} {qt[xy+b12]:18.15f} differs from nominal {q0[xy+b12]} by {diff}"
                    )
        return tw

    def load_sequence(self, mad, seqname):
        trycall(mad, seqname)

        # if mad.globals.SPSLENGTH != 6911.5038:
        #     print(
        #         f"Error: SPS length {mad.globals.SPSLENGTH} m not correct, expecting 6911.5038 m."
        #     )

    def load_optics(self, mad, optname):
        trycall(mad, optname)

    def make_beam(self, mad):
        mad.input(
            """
            BEAM, PARTICLE=PROTON, PC = 450;
            """
        )
        tryinput(mad, "use,sequence=sps;")

    def load_aperture(self, mad):
        trycall(mad, self.appath)

    def make_survey(
        self,
        mad,
        x0=-1559.44225668,
        y0=2402.175006,
        z0=2485.226688,
        theta0=-305.844696 / 400 * (2 * np.pi),
        phi0=-0.00007450,
        psi0=0.00022800,
        file="survey.tfs",
    ):
        sequence = "sps"
        mad.beam(sequence=sequence)
        mad.use(sequence)
        mad.input('set,  format="12.9f";')
        cols = "name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,tilt,mech_sep,assembly_id,slot_id".split(
            ","
        )
        mad.select(flag="survey", column=cols)

        x0n = x0 + x0c[0]
        y0n = y0 + x0c[1]
        z0n = z0 + x0c[2]
        theta0n = theta0 - dir0[0]
        phi0n = phi0 - dir0[1]
        psi0n = psi0 - dir0[2]

        mad.survey(
            x0=x0n, y0=y0n, z0=z0n, theta0=theta0n, phi0=phi0n, psi0=psi0n, file=file
        )
        s = copy_table(mad.table.survey, cols)
        return s

    def run_tests(self):
        # run madx
        self.mad = Madx(stdout=stdout, stderr=sys.stdout)

        # load sequence
        self.load_sequence(self.mad, self.seqpath)

        sudir = self.seqpath.parent / f"SURVEY_{self.seqpath.stem}"

        if not sudir.exists():
            sudir.mkdir()
        self.survey = self.make_survey(
            self.mad, file=str(sudir / f"SPS_survey_{self.seqpath.stem}.tfs")
        )

        # make beam
        self.make_beam(self.mad)

        if self.optpath.exists():
            self.load_optics(self.mad, self.optpath)
            self.tw = self.check_optics(self.mad)

        self.load_aperture(self.mad)
        if self.optpath.exists():
            self.thtw = self.make_twiss(self.mad)
            self.compare_beamsize()

    def close(self):
        self.mad.exit()


if __name__ == "__main__":
    # validation
    seqpath = Path(sys.argv[1])
    sps = SPSModel(seqpath)
    sps.run_tests()
    sps.close()
