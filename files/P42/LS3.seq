/************************************************************************************************************************************************************************
*
* P42 version LS3 in MAD X SEQUENCE format
* Generated the 01-FEB-2025 02:54:49 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.P42_BBS                      := 0.45;
l.P42_BSGV                     := 0.45;
l.P42_BSI                      := 0;
l.P42_BSM                      := 0.45;
l.P42_BSP                      := 0.45;
l.P42_MBNH_HWP                 := 5;
l.P42_MBNV_HWP                 := 5;
l.P42_MBW__HWP                 := 6.2;
l.P42_MBXGDCWP                 := 3;
l.P42_MCXCAHWC                 := 0.4;
l.P42_MDSH                     := 0.7;
l.P42_MSN                      := 3.2;
l.P42_MTN__HWP                 := 3.6;
l.P42_OMK                      := 0;
l.P42_QNL__8WP                 := 2.99;
l.P42_QSL__8WP                 := 3;
l.P42_TBID                     := 0.25;
l.P42_TBIU                     := 0;
l.P42_TCMAA                    := 0.4;
l.P42_TCMAC                    := 0.4;
l.P42_XCHV_001                 := 1;
l.P42_XCIOP001                 := 0.1;
l.P42_XFF__001                 := 0.276;
l.P42_XTAX_007                 := 1.615;
l.P42_XTAX_008                 := 1.615;
l.P42_XTCX_002                 := 2.4;
l.P42_XTCX_005                 := 1.6;
l.P42_XVVSB001                 := 0.32;
l.P42_XVWAB001                 := 0.0001;
l.P42_XVWAC001                 := 0.0001;
l.P42_XVWAD001                 := 0.0001;
l.P42_XVWTA001                 := 0.0001;

//---------------------- COLLIMATOR     ---------------------------------------------
P42_TCMAA      : COLLIMATOR  , L := l.P42_TCMAA;         ! Collimation mask type A
P42_TCMAC      : COLLIMATOR  , L := l.P42_TCMAC;         ! Collimation mask type C
P42_XCHV_001   : COLLIMATOR  , L := l.P42_XCHV_001;      ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
P42_XCIOP001   : COLLIMATOR  , L := l.P42_XCIOP001;      ! Converter IN OUT Plate - Lead
P42_XTAX_007   : COLLIMATOR  , L := l.P42_XTAX_007;      ! Target Absorber Type 007
P42_XTAX_008   : COLLIMATOR  , L := l.P42_XTAX_008;      ! Target Absorber Type 008
P42_XTCX_002   : COLLIMATOR  , L := l.P42_XTCX_002;      ! XTCX - Fixed collimator 2.4m water cooled, vac chamber
P42_XTCX_005   : COLLIMATOR  , L := l.P42_XTCX_005;      ! XTCX - Fixed Collimator 1.6 m, Vacuum, Cooling

//---------------------- HKICKER        ---------------------------------------------
P42_MDSH       : HKICKER     , L := l.P42_MDSH;          ! Correcting dipole, BT line, short, horizontal deflection
//---------------------- INSTRUMENT     ---------------------------------------------
P42_BBS        : INSTRUMENT  , L := l.P42_BBS;           ! Beam Box Scanner
P42_BSGV       : INSTRUMENT  , L := l.P42_BSGV;          ! SEM grid, vertical
P42_BSI        : INSTRUMENT  , L := l.P42_BSI;           ! SEM intensity
P42_BSM        : INSTRUMENT  , L := l.P42_BSM;           ! SEM position, moveable
P42_TBIU       : INSTRUMENT  , L := l.P42_TBIU;          ! target beam instrumentation, upstream
P42_XVVSB001   : INSTRUMENT  , L := l.P42_XVVSB001;      ! X Vacuum Sector ElectroValve DN159x320mm (local control) - VAT
P42_XVWAB001   : INSTRUMENT  , L := l.P42_XVWAB001;      ! X Vacuum Window Aluminium EL350x60x0.1
P42_XVWAC001   : INSTRUMENT  , L := l.P42_XVWAC001;      ! X Vacuum Window Aluminium DN159X0.1, conical flanges
P42_XVWAD001   : INSTRUMENT  , L := l.P42_XVWAD001;      ! X Vacuum Window Aluminium DN159X0.1, aperture 120 mm
P42_XVWTA001   : INSTRUMENT  , L := l.P42_XVWTA001;      ! X Vacuum Window Titanium DN159x0.1
//---------------------- KICKER         ---------------------------------------------
P42_MCXCAHWC   : KICKER      , L := l.P42_MCXCAHWC;      ! Corrector magnet, H or V, type MDX
//---------------------- MARKER         ---------------------------------------------
P42_OMK        : MARKER      , L := l.P42_OMK;           ! P42 markers
//---------------------- MONITOR        ---------------------------------------------
P42_BSP        : MONITOR     , L := l.P42_BSP;           ! SEM position, horizontal + vertical
P42_TBID       : MONITOR     , L := l.P42_TBID;          ! target beam instrumentation, downstream
P42_XFF__001   : MONITOR     , L := l.P42_XFF__001;      ! Filament Scintillator Profile Monitor
//---------------------- QUADRUPOLE     ---------------------------------------------
P42_QNL__8WP   : QUADRUPOLE  , L := l.P42_QNL__8WP;      ! Quadrupole, secondary beams, type north area
P42_QSL__8WP   : QUADRUPOLE  , L := l.P42_QSL__8WP;      ! Quadrupole, slim, long, - Same magnet type SPQSLD_
//---------------------- RBEND          ---------------------------------------------
P42_MBNH_HWP   : RBEND       , L := l.P42_MBNH_HWP;      ! Bending magnet, secondary beams, horizontal, north area
P42_MBNV_HWP   : RBEND       , L := l.P42_MBNV_HWP;      ! Bending magnet, secondary beams, vertical, north area - Mechanical dimensions from NORMA
P42_MBW__HWP   : RBEND       , L := l.P42_MBW__HWP;      ! Bending Magnet, main, type B2, 1000A
P42_MBXGDCWP   : RBEND       , L := l.P42_MBXGDCWP;      ! Bending Magnet, H or V, type MCW
P42_MSN        : RBEND       , L := l.P42_MSN;           ! Septum magnet, north area - offset mechanical and optical dimensions according to drawing 1766138 (not well indicated)
P42_MTN__HWP   : RBEND       , L := l.P42_MTN__HWP;      ! Bending magnet, Target N

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kMBH.X0400003_BEND3T           := -0.0007;
kMBH.X0400007_BEND3T           := -0.0007;
kMBNH.X0430520                 := -0.0059997;
kMBNH.X0430525                 := -0.0059997;
kMBNH.X0430531                 := -0.0059997;
kMBNH.X0430537                 := -0.0059997;
kMBNH.X0430542                 := -0.0059997;
kMBNH.X0430548                 := -0.0059997;
kMBNH.X0430554                 := -0.0059997;
kMBNH.X0430559                 := -0.0059997;
kMBNH.X0430565                 := -0.0059997;
kMBNH.X0430718                 := -0.0071455;
kMBNH.X0430724                 := -0.0071455;
kMBNH.X0430730                 := 0.0029436;
kMBNH.X0430735                 := 0.0029436;
kMBNV.X0450823                 := 0.0069631;
kMBNV.X0450829                 := 0.0060254;
kMBW.X0430069                  := 0.0065952;
kMBW.X0430076                  := 0.0065952;
kMBW.X0430082                  := 0.0065952;
kMBW.X0430089                  := 0.0065952;
kMBW.X0430096                  := 0.0065952;
kMBXGD.X0450834                := -0.0033351;
kMSN.X0430022                  := 0.0014;
kMSN.X0430029                  := 0.0028;
tilt.MBH.X0400003_BEND3T       := -0.0002617993877991494;
tilt.MBH.X0400007_BEND3T       := -0.000261799;
tilt.MBNH.X0430520             := -0.00039815233921242706;
tilt.MBNH.X0430525             := -0.000343169583121045;
tilt.MBNH.X0430531             := -0.0002881972412966894;
tilt.MBNH.X0430537             := -0.000233235313558402;
tilt.MBNH.X0430542             := -0.00017828379970740522;
tilt.MBNH.X0430548             := -0.00012334269956141007;
tilt.MBNH.X0430554             := -6.841201293791084e-05;
tilt.MBNH.X0430559             := -0.000013491739646258;
tilt.MBNH.X0430565             := 4.141812050438982e-05;
tilt.MBNH.X0430718             := 9.631756769629205e-05;
tilt.MBNH.X0430724             := 0.00016168786617705617;
tilt.MBNH.X0430730             := 0.00022704502762330343;
tilt.MBNH.X0430735             := 0.000200122040548116;
tilt.MBNV.X0450823             := 1.5709695255963894;
tilt.MBNV.X0450829             := 1.5709695152696603;
tilt.MBW.X0430069              := -0.369310662751648;
tilt.MBW.X0430076              := -0.3693157743395632;
tilt.MBW.X0430082              := -0.00028565810790937403;
tilt.MBW.X0430089              := -0.369364566576347;
tilt.MBW.X0430096              := -0.36939897325455306;
tilt.MBXGD.X0450834            := 1.5709695165778923;
tilt.MCXCA.X0430048            := 1.5705355343774487;
tilt.MCXCA.X0430100            := 1.570398174455684;
tilt.MCXCA.X0430171            := -0.0003981523392124264;
tilt.MCXCA.X0430228            := 1.570398174455684;
tilt.MCXCA.X0430458            := 1.570398174455684;
tilt.MCXCA.X0430573            := 1.5708926443625928;
tilt.MCXCA.X0430688            := 9.631756769629276e-05;
tilt.MCXCA.X0430715            := 1.5708926443625928;
tilt.MCXCA.X0450820            := 0.0001731988014928568;
tilt.MDSH.X0430401             := -0.00039815233921242674;
tilt.MSN.X0430022              := -0.00026230364426381447;
tilt.MSN.X0430029              := -0.000261799388206413;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 BSI 3                                             : P42_BSI;
 MBH.X0430520_BEND71H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430520, TILT := tilt.MBNH.X0430520;
 MBH.X0430525_BEND72H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430525, TILT := tilt.MBNH.X0430525;
 MBH.X0430531_BEND71H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430531, TILT := tilt.MBNH.X0430531;
 MBH.X0430537_BEND72H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430537, TILT := tilt.MBNH.X0430537;
 MBH.X0430542_BEND71H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430542, TILT := tilt.MBNH.X0430542;
 MBH.X0430548_BEND72H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430548, TILT := tilt.MBNH.X0430548;
 MBH.X0430554_BEND71H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430554, TILT := tilt.MBNH.X0430554;
 MBH.X0430559_BEND72H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430559, TILT := tilt.MBNH.X0430559;
 MBH.X0430565_BEND71H                              : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430565, TILT := tilt.MBNH.X0430565;
 MBH.X0430718_BEND8H                               : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430718, TILT := tilt.MBNH.X0430718;
 MBH.X0430724_BEND8H                               : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430724, TILT := tilt.MBNH.X0430724;
 MBH.X0430730_BEND9H                               : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430730, TILT := tilt.MBNH.X0430730;
 MBH.X0430735_BEND9H                               : P42_MBNH_HWP    , APERTYPE=RECTANGLE, APERTURE={.125,.03,.125,.03}, ANGLE := kMBNH.X0430735, TILT := tilt.MBNH.X0430735;
 MBV.X0430823_BEND10V                              : P42_MBNV_HWP    , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBNV.X0450823, TILT := tilt.MBNV.X0450823;
 MBV.X0430829_BEND11V                              : P42_MBNV_HWP    , APERTYPE=RACETRACK, APERTURE={.03,.125,.03,.125}, ANGLE := kMBNV.X0450829, TILT := tilt.MBNV.X0450829;
 MBT.X0430069_BEND4T                               : P42_MBW__HWP    , APERTYPE=UNDEFINED, APERTURE={.0645,.026,.0645,.026}, ANGLE := kMBW.X0430069, TILT := tilt.MBW.X0430069;
 MBT.X0430076_BEND4T                               : P42_MBW__HWP    , APERTYPE=UNDEFINED, APERTURE={.0645,.026,.0645,.026}, ANGLE := kMBW.X0430076, TILT := tilt.MBW.X0430076;
 MBH.X0430082_BEND5H                               : P42_MBW__HWP    , APERTYPE=UNDEFINED, APERTURE={.0645,.026,.0645,.026}, ANGLE := kMBW.X0430082, TILT := tilt.MBW.X0430082;
 MBT.X0430089_BEND4T                               : P42_MBW__HWP    , APERTYPE=UNDEFINED, APERTURE={.0645,.026,.0645,.026}, ANGLE := kMBW.X0430089, TILT := tilt.MBW.X0430089;
 MBT.X0430096_BEND4T                               : P42_MBW__HWP    , APERTYPE=UNDEFINED, APERTURE={.0645,.026,.0645,.026}, ANGLE := kMBW.X0430096, TILT := tilt.MBW.X0430096;
 MBV.X0450834_BEND12V                              : P42_MBXGDCWP    , APERTYPE=RECTELLIPSE, APERTURE={.06,.035,.095,.035}, ANGLE := kMBXGD.X0450834, TILT := tilt.MBXGD.X0450834;
 MCBV.X0430048_TRIM1V                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430048, HKICK := kMCXCA.X0430048;
 MCBV.X0430100_TRIM2V                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430100, HKICK := kMCXCA.X0430100;
 MCBH.X0430171_TRIM3H                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430171, HKICK := kMCXCA.X0430171;
 MCBV.X0430228_TRIM4V                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430228, HKICK := kMCXCA.X0430228;
 MCBV.X0430458_TRIM6V                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430458, HKICK := kMCXCA.X0430458;
 MCBV.X0430573_TRIM7V                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430573, HKICK := kMCXCA.X0430573;
 MCBH.X0430688_TRIM8H                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430688, HKICK := kMCXCA.X0430688;
 MCBV.X0430715_TRIM9V                              : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0430715, HKICK := kMCXCA.X0430715;
 MCBH.X0430820_TRIM10H                             : P42_MCXCAHWC    , TILT := tilt.MCXCA.X0450820, HKICK := kMCXCA.X0450820;
 MCBH.X0430401_TRIM5H                              : P42_MDSH        , APERTYPE=RECTANGLE, APERTURE={.065,.0345,.065,.0345}, TILT := tilt.MDSH.X0430401, HKICK := kMDSH.X0430401;
 MBH.X0430022_BEND2H                               : P42_MSN         , APERTYPE=RACETRACK, APERTURE={.57,.03,.57,.03}, ANGLE := kMSN.X0430022, TILT := tilt.MSN.X0430022;
 MBH.X0430029_BEND3H                               : P42_MSN         , APERTYPE=RACETRACK, APERTURE={.57,.03,.57,.03}, ANGLE := kMSN.X0430029, TILT := tilt.MSN.X0430029;
 MBH.X0400003_BEND3T                               : P42_MTN__HWP    , APERTYPE=RECTANGLE, APERTURE={.175,.03,.175,.03}, ANGLE := kMBH.X0400003_BEND3T, TILT := tilt.MBH.X0400003_BEND3T;
 MBH.X0400007_BEND3T                               : P42_MTN__HWP    , APERTYPE=RECTANGLE, APERTURE={.175,.03,.175,.03}, ANGLE := kMBH.X0400007_BEND3T, TILT := tilt.MBH.X0400007_BEND3T;
 MQF.X0430040_QUAD2F                               : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430040;
 MQD.X0430050_QUAD3D                               : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430050;
 MQD.X0430054_QUAD3D                               : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430054;
 MQF.X0430064_QUAD4F                               : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430064;
 MQD.X0430108_QUAD5D                               : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430108;
 MQD.X0430111_QUAD5D                               : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430111;
 MQF.X0430169_QUAD10F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430169;
 MQD.X0430226_QUAD11D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430226;
 MQF.X0430284_QUAD12F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430284;
 MQD.X0430341_QUAD13D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430341;
 MQF.X0430399_QUAD14F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430399;
 MQD.X0430456_QUAD13D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430456;
 MQF.X0430513_QUAD14F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430513;
 MQD.X0430571_QUAD15D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430571;
 MQF.X0430628_QUAD16F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430628;
 MQF.X0430685_QUAD17F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430685;
 MQD.X0430710_QUAD18D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430710;
 MQD.X0430714_QUAD18D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0430714;
 MQF.X0430770_QUAD19F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0450770;
 MQD.X0430792_QUAD20D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0450792;
 MQD.X0450795_QUAD20D                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0450795;
 MQF.X0430814_QUAD21F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0450814;
 MQF.X0450817_QUAD21F                              : P42_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kQNL.X0450817;
 MQF.X0430033_QUAD1F                               : P42_QSL__8WP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kQSL.X0430033;
 VXSV 343                                          : P42_XVVSB001    , APERTYPE=CIRCLE, APERTURE={.1305,.1305,.1305,.1305};
 VXSV 517                                          : P42_XVVSB001    , APERTYPE=CIRCLE, APERTURE={.1305,.1305,.1305,.1305};
 VXSV 648                                          : P42_XVVSB001    , APERTYPE=CIRCLE, APERTURE={.1305,.1305,.1305,.1305};
 VXSV 802                                          : P42_XVVSB001    , APERTYPE=CIRCLE, APERTURE={.1305,.1305,.1305,.1305};
 VXWT                                              : P42_XVWAB001    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};
 VXWP 150                                          : P42_XVWAC001    , APERTYPE=CIRCLE, APERTURE={.078,.078,.078,.078};
 VXW 150                                           : P42_XVWTA001;

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

P42 : SEQUENCE, refer = centre,         L = 838.16;
 TBACA.X0400000                : P42_OMK                     , at = .0000005     , slot_id = 56964667;
 TBID.241150                   : P42_TBID                    , at = .475         , slot_id = 47534194;
 TCMAA.X0400001                : P42_TCMAA                   , at = .85          , slot_id = 56992277;
 XVW.X0400001                  : VXWT                        , at = 1.129        , slot_id = 57310366;
 MTN.X0400003                  : MBH.X0400003_BEND3T         , at = 3.15         , slot_id = 56992202;
 MTN.X0400007                  : MBH.X0400007_BEND3T         , at = 7.35         , slot_id = 56992225;
 XVW.X0430009                  : P42_XVWAB001                , at = 9.372        , slot_id = 60073506;
 XTAX.X0430018                 : P42_XTAX_007                , at = 18.0075      , slot_id = 57087670;
 XTAX.X0430020                 : P42_XTAX_008                , at = 19.6325      , slot_id = 56992724;
 XCIO.X0430021                 : P42_XCIOP001                , at = 20.525       , slot_id = 56995006;
 XVW.X0430020                  : P42_XVWAD001                , at = 20.639       , slot_id = 57310475;
 MSN.X0430022                  : MBH.X0430022_BEND2H         , at = 22.45        , slot_id = 56992358;
 MSN.X0430029                  : MBH.X0430029_BEND3H         , at = 29.43        , slot_id = 56992392;
 QSL.X0430033                  : MQF.X0430033_QUAD1F         , at = 32.92        , slot_id = 56992417;
 QNL.X0430040                  : MQF.X0430040_QUAD2F         , at = 39.78        , slot_id = 56050728;
 XTCX.X0430042                 : P42_XTCX_005                , at = 42.425       , slot_id = 57087872;
 MCXCA.X0430048                : MCBV.X0430048_TRIM1V        , at = 48.095       , slot_id = 56051257;
 QNL.X0430050                  : MQD.X0430050_QUAD3D         , at = 50.28        , slot_id = 56050737;
 QNL.X0430054                  : MQD.X0430054_QUAD3D         , at = 53.71        , slot_id = 56050746;
 QNL.X0430064                  : MQF.X0430064_QUAD4F         , at = 64.21        , slot_id = 56050755;
 MBW.X0430069                  : MBT.X0430069_BEND4T         , at = 69.219       , slot_id = 40400557;
 MBW.X0430076                  : MBT.X0430076_BEND4T         , at = 75.859       , slot_id = 56050692;
 MBW.X0430082                  : MBH.X0430082_BEND5H         , at = 82.499       , slot_id = 40401984;
 MBW.X0430089                  : MBT.X0430089_BEND4T         , at = 89.139       , slot_id = 56050701;
 MBW.X0430096                  : MBT.X0430096_BEND4T         , at = 95.779       , slot_id = 40401985;
 MCXCA.X0430100                : MCBV.X0430100_TRIM2V        , at = 99.56        , slot_id = 56050791;
 QNL.X0430108                  : MQD.X0430108_QUAD5D         , at = 107.75       , slot_id = 56050764;
 QNL.X0430111                  : MQD.X0430111_QUAD5D         , at = 111.18       , slot_id = 56050773;
 QNL.X0430169                  : MQF.X0430169_QUAD10F        , at = 168.65       , slot_id = 56994620;
 MCXCA.X0430171                : MCBH.X0430171_TRIM3H        , at = 170.7        , slot_id = 56994644;
 BBS.X0430224                  : P42_BBS                     , at = 223.46       , slot_id = 57088220;
 BSP.X0430225                  : P42_BSP                     , at = 224.045      , slot_id = 57088055;
 QNL.X0430226                  : MQD.X0430226_QUAD11D        , at = 226.12       , slot_id = 56994696;
 MCXCA.X0430228                : MCBV.X0430228_TRIM4V        , at = 228.17       , slot_id = 56994723;
 BBS.X0430281                  : P42_BBS                     , at = 280.93       , slot_id = 57088229;
 BSP.X0430282                  : P42_BSP                     , at = 281.515      , slot_id = 57088064;
 BSI.X0430282                  : BSI 3                       , at = 281.7400005  , slot_id = 57088293;
 QNL.X0430284                  : MQF.X0430284_QUAD12F        , at = 283.59       , slot_id = 56994981;
 XCHV.X0430286                 : P42_XCHV_001                , at = 286.015      , slot_id = 56995032;
 QNL.X0430341                  : MQD.X0430341_QUAD13D        , at = 341.06       , slot_id = 56995143;
 XVVS.X0430343                 : VXSV 343                    , at = 343          , slot_id = 57524422;
 BSG.X0430389                  : P42_BSGV                    , at = 388.917      , slot_id = 57311266;
 XVW.X0430396                  : VXWP 150                    , at = 395.845      , slot_id = 57905286;
 XCHV.X0430396                 : P42_XCHV_001                , at = 396.093      , slot_id = 56997983;
 QNL.X0430399                  : MQF.X0430399_QUAD14F        , at = 398.53       , slot_id = 56998033;
 MDSH.X0430401                 : MCBH.X0430401_TRIM5H        , at = 400.789      , slot_id = 56998114;
 XCHV.X0430454                 : P42_XCHV_001                , at = 453.575      , slot_id = 56997992;
 QNL.X0430456                  : MQD.X0430456_QUAD13D        , at = 456          , slot_id = 56998042;
 MCXCA.X0430458                : MCBV.X0430458_TRIM6V        , at = 458.05       , slot_id = 56998123;
 BSG.X0430510                  : P42_BSGV                    , at = 510.32       , slot_id = 57311215;
 QNL.X0430513                  : MQF.X0430513_QUAD14F        , at = 513.47       , slot_id = 56998051;
 XFFV.X0430515                 : P42_XFF__001                , at = 515.323      , slot_id = 56998177;
 XFFH.X0430516                 : P42_XFF__001                , at = 515.599      , slot_id = 56998159;
 XVVS.X0430517                 : VXSV 517                    , at = 517          , slot_id = 57556195;
 MBNH.X0430520                 : MBH.X0430520_BEND71H        , at = 519.565      , slot_id = 56998186;
 MBNH.X0430525                 : MBH.X0430525_BEND72H        , at = 525.225      , slot_id = 56998195;
 MBNH.X0430531                 : MBH.X0430531_BEND71H        , at = 530.885      , slot_id = 56998204;
 MBNH.X0430537                 : MBH.X0430537_BEND72H        , at = 536.545      , slot_id = 56998213;
 MBNH.X0430542                 : MBH.X0430542_BEND71H        , at = 542.205      , slot_id = 56998222;
 MBNH.X0430548                 : MBH.X0430548_BEND72H        , at = 547.865      , slot_id = 56998231;
 MBNH.X0430554                 : MBH.X0430554_BEND71H        , at = 553.525      , slot_id = 56998240;
 MBNH.X0430559                 : MBH.X0430559_BEND72H        , at = 559.185      , slot_id = 56998249;
 MBNH.X0430565                 : MBH.X0430565_BEND71H        , at = 564.845      , slot_id = 56998258;
 BSP.X0430569                  : P42_BSP                     , at = 568.865      , slot_id = 57088073;
 QNL.X0430571                  : MQD.X0430571_QUAD15D        , at = 570.94       , slot_id = 56998060;
 MCXCA.X0430573                : MCBV.X0430573_TRIM7V        , at = 572.99       , slot_id = 56998132;
 BSP.X0430626                  : P42_BSP                     , at = 626.335      , slot_id = 57088082;
 QNL.X0430628                  : MQF.X0430628_QUAD16F        , at = 628.41       , slot_id = 56998069;
 XTCX.X0430631                 : P42_XTCX_002                , at = 631.455      , slot_id = 57087897;
 XVVS.X0430648                 : VXSV 648                    , at = 648          , slot_id = 57522841;
 BSG.X0430653                  : P42_BSGV                    , at = 653.441      , slot_id = 57311360;
 XCHV.X0430683                 : P42_XCHV_001                , at = 683.455      , slot_id = 56998003;
 QNL.X0430685                  : MQF.X0430685_QUAD17F        , at = 685.88       , slot_id = 56998078;
 MCXCA.X0430688                : MCBH.X0430688_TRIM8H        , at = 687.93       , slot_id = 56998141;
 XFFH.X0430708                 : P42_XFF__001                , at = 706.882      , slot_id = 57198319;
 QNL.X0430710                  : MQD.X0430710_QUAD18D        , at = 708.87       , slot_id = 57196968;
 QNL.X0430714                  : MQD.X0430714_QUAD18D        , at = 712.915      , slot_id = 56998087;
 MCXCA.X0430715                : MCBV.X0430715_TRIM9V        , at = 714.965      , slot_id = 56998150;
 MBNH.X0430718                 : MBH.X0430718_BEND8H         , at = 718.23       , slot_id = 56998267;
 MBNH.X0430724                 : MBH.X0430724_BEND8H         , at = 723.89       , slot_id = 56998276;
 MBNH.X0430730                 : MBH.X0430730_BEND9H         , at = 729.55       , slot_id = 56998285;
 MBNH.X0430735                 : MBH.X0430735_BEND9H         , at = 735.21       , slot_id = 56998294;
 BSP.X0450769                  : P42_BSP                     , at = 768.31       , slot_id = 57088184;
 BSI.X0450769                  : P42_BSI                     , at = 768.5350005  , slot_id = 57088316;
 QNL.X0450770                  : MQF.X0430770_QUAD19F        , at = 770.385      , slot_id = 56050809;
 QNL.X0450792                  : MQD.X0430792_QUAD20D        , at = 792.125      , slot_id = 56050818;
 QNL.X0450795                  : MQD.X0450795_QUAD20D        , at = 795.555      , slot_id = 56050827;
 XVVS.X0450802                 : VXSV 802                    , at = 802          , slot_id = 57556225;
 QNL.X0450814                  : MQF.X0430814_QUAD21F        , at = 813.865      , slot_id = 56050836;
 QNL.X0450817                  : MQF.X0450817_QUAD21F        , at = 817.295      , slot_id = 56051237;
 BSM.X0450819                  : P42_BSM                     , at = 819.235      , slot_id = 57212612;
 MCXCA.X0450820                : MCBH.X0430820_TRIM10H       , at = 819.895      , slot_id = 56051246;
 MBNV.X0450823                 : MBV.X0430823_BEND10V        , at = 823.06       , slot_id = 57087563;
 MBNV.X0450829                 : MBV.X0430829_BEND11V        , at = 828.72       , slot_id = 57087572;
 MBXGD.X0450834                : MBV.X0450834_BEND12V        , at = 834.128      , slot_id = 56050800;
 XVW.X0450836                  : VXW 150                     , at = 836.013001   , slot_id = 60484406;
 BSG.X0450836                  : P42_BSGV                    , at = 836.204      , slot_id = 57311552;
 TBIU.X0450837                 : P42_TBIU                    , at = 837.1000005  , slot_id = 56964638;
 TCMAC.X0450838                : P42_TCMAC                   , at = 837.5        , slot_id = 56993160;
 TBACB.X1010000                : P42_OMK                     , at = 838.06       , slot_id = 55490825;
ENDSEQUENCE;

return;