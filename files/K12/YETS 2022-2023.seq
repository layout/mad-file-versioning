/************************************************************************************************************************************************************************
*
* K12 version YETS 2022-2023 in MAD X SEQUENCE format
* Generated the 17-MAY-2024 02:20:15 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.K12_MBHHEHWC                 := 2;
l.K12_MBHHJHWC                 := 2;
l.K12_MBMNBHWC                 := 1.3;
l.K12_MBXHDCWP                 := 2.5;
l.K12_MCXCAHWC                 := 0.4;
l.K12_MQNEGTWP                 := 0.8;
l.K12_MTR__HWP                 := 3.6;
l.K12_OMK                      := 0;
l.K12_QNL__8WP                 := 2.99;
l.K12_QNRB_8WP                 := 2.99;
l.K12_TCCV                     := 0.95;
l.K12_TCMAD                    := 0.3;
l.K12_XCEDH                    := 5.669;
l.K12_XCHV_001                 := 1;
l.K12_XCMV                     := 5;
l.K12_XCON_010                 := 0.004;
l.K12_XCSH_003                 := 1;
l.K12_XCSV_004                 := 1;
l.K12_XFFB_001                 := 0.276;
l.K12_XFF__001                 := 0.276;
l.K12_XION_002                 := 0.25;
l.K12_XTAX_013                 := 1.615;
l.K12_XTAX_014                 := 1.615;
l.K12_XTCX_001                 := 1.615;
l.K12_XTCX_004                 := 1.2;
l.K12_XWCM_001                 := 0.1;

//---------------------- COLLIMATOR     ---------------------------------------------
K12_TCCV       : COLLIMATOR  , L := l.K12_TCCV;          ! Collimator Copper Vertical for T10 target
K12_TCMAD      : COLLIMATOR  , L := l.K12_TCMAD;         ! Collimation mask type D
K12_XCHV_001   : COLLIMATOR  , L := l.K12_XCHV_001;      ! SPS Collimator horizontal et vertical 4 blocks (design 1970)
K12_XCMV       : COLLIMATOR  , L := l.K12_XCMV;          ! Collimator Magnetic Vertical
K12_XCON_010   : COLLIMATOR  , L := l.K12_XCON_010;      ! Converter Type 010 - CONVERTER01 Radiateur de gamme 5 positions
K12_XCSH_003   : COLLIMATOR  , L := l.K12_XCSH_003;      ! Collimator 2 blocks horizontal  (design 1970) with special Cu Jaws
K12_XCSV_004   : COLLIMATOR  , L := l.K12_XCSV_004;      ! Collimator 2 blocks vertical  (design 1970) with special W jaws 

K12_XTAX_013   : COLLIMATOR  , L := l.K12_XTAX_013;      ! Target Absorber Type 013
K12_XTAX_014   : COLLIMATOR  , L := l.K12_XTAX_014;      ! Target Absorber Type 014
K12_XTCX_001   : COLLIMATOR  , L := l.K12_XTCX_001;      ! XTCX - Fixed collimator 1.6m water cooled, vac chamber
K12_XTCX_004   : COLLIMATOR  , L := l.K12_XTCX_004;      ! XTCX - Fixed collimator 1.2m, not cooled, Vacuum (Rasta)
//---------------------- INSTRUMENT     ---------------------------------------------
K12_XCEDH      : INSTRUMENT  , L := l.K12_XCEDH;         ! Cherenkov Differential Counter Hydrogen
//---------------------- KICKER         ---------------------------------------------
K12_MCXCAHWC   : KICKER      , L := l.K12_MCXCAHWC;      ! Corrector magnet, H or V, type MDX
//---------------------- MARKER         ---------------------------------------------
K12_OMK        : MARKER      , L := l.K12_OMK;           ! K12 markers
//---------------------- MONITOR        ---------------------------------------------
K12_XFFB_001   : MONITOR     , L := l.K12_XFFB_001;      ! Filament Scintillator Profile Monitor - Big
K12_XFF__001   : MONITOR     , L := l.K12_XFF__001;      ! Filament Scintillator Profile Monitor
K12_XION_002   : MONITOR     , L := l.K12_XION_002;      ! Assembly 2 Ionization Chambers and Support
K12_XWCM_001   : MONITOR     , L := l.K12_XWCM_001;      ! Ensemble: Multi Wire Proportional Chamber et Support cadre rouge not motorized
//---------------------- QUADRUPOLE     ---------------------------------------------
K12_MBMNBHWC   : QUADRUPOLE  , L := l.K12_MBMNBHWC;      ! Experimental magnet MNP33
K12_MQNEGTWP   : QUADRUPOLE  , L := l.K12_MQNEGTWP;      ! Quadrupole magnet, type QFS, 0.8m
K12_QNL__8WP   : QUADRUPOLE  , L := l.K12_QNL__8WP;      ! Quadrupole, secondary beams, type north area
K12_QNRB_8WP   : QUADRUPOLE  , L := l.K12_QNRB_8WP;      ! Quadrupole, secondary beams, reduced aperture, mineral isolation coil, north area
//---------------------- RBEND          ---------------------------------------------
K12_MBHHEHWC   : RBEND       , L := l.K12_MBHHEHWC;      ! Bending magnet, type M200, straight poles
K12_MBHHJHWC   : RBEND       , L := l.K12_MBHHJHWC;      ! Bending magnet, type M200, tappered poles
K12_MTR__HWP   : RBEND       , L := l.K12_MTR__HWP;      ! Bending magnet, Target
//---------------------- SBEND          ---------------------------------------------
K12_MBXHDCWP   : SBEND       , L := l.K12_MBXHDCWP;      ! Bending Magnet, H or V, type HB2, 2.5m gap 80mm

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kMBV.X1010017_BEND1V           := 0.39285;
kMBV.X1010021_BEND1V           := -0.39285;
kMBV.X1010029_BEND1V           := 0.39285;
kMBV.X1010033_BEND2V           := -0.39285;
kMBV.X1010082_BEND4V           := 0.2500500524039828;
kMBV.X1010085_BEND4V           := -0.2500500524039828;
kMBV.X1010095_BEND5V           := -0.2500500524039828;
kMBV.X1010098_BEND6V           := -0.2500500524039828;
tilt.MBV.X1010017_BEND1V       := 1.5708;
tilt.MBV.X1010021_BEND1V       := 1.5708;
tilt.MBV.X1010029_BEND1V       := 1.5708;
tilt.MBV.X1010033_BEND2V       := 1.5708;
tilt.MBV.X1010082_BEND4V       := 1.5708;
tilt.MBV.X1010085_BEND4V       := 1.5708;
tilt.MBV.X1010095_BEND5V       := 1.5708;
tilt.MBV.X1010098_BEND6V       := 1.5708;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 MBH.X1010051_BEND3H                               : K12_MBHHEHWC    , ANGLE := kMBH.X1010051_BEND3H, TILT := tilt.MBH.X1010051_BEND3H;
 MBH.X1010054_BEND3H                               : K12_MBHHEHWC;
 MBH.X1010057_BEND3H                               : K12_MBHHEHWC;
 MBH.X1010249_BEND9H                               : K12_MBHHJHWC;
 MBXH.X1010197_MNP33AH - MBXH.X1010197_MNP33BH     : K12_MBMNBHWC;
 MBV.X1010082_BEND4V                               : K12_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBV.X1010082_BEND4V, TILT := tilt.MBV.X1010082_BEND4V;
 MBV.X1010085_BEND4V                               : K12_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBV.X1010085_BEND4V, TILT := tilt.MBV.X1010085_BEND4V;
 MBV.X1010095_BEND5V                               : K12_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBV.X1010095_BEND5V, TILT := tilt.MBV.X1010095_BEND5V;
 MBV.X1010098_BEND6V                               : K12_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kMBV.X1010098_BEND6V, TILT := tilt.MBV.X1010098_BEND6V;
 MCBH.X1010011_TRIM1H                              : K12_MCXCAHWC;
 MCBH.X1010049_TRIM2H                              : K12_MCXCAHWC;
 MCBH.X1010059_TRIM3H                              : K12_MCXCAHWC;
 MCBV.X1010068_TRIM4V                              : K12_MCXCAHWC;
 MCBH.X1011002_TRIM5H                              : K12_MCXCAHWC;
 MQD.X1010077_QUAD9D                               : K12_MQNEGTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kMQD.X1010077_QUAD9D;
 MQF.X1010079_QUAD10F                              : K12_MQNEGTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05};
 MBV.X1010017_BEND1V                               : K12_MTR__HWP    , APERTYPE=RECTANGLE, APERTURE={.162,.018,.162,.018}, ANGLE := kMBV.X1010017_BEND1V, TILT := tilt.MBV.X1010017_BEND1V;
 MBV.X1010021_BEND1V                               : K12_MTR__HWP    , APERTYPE=RECTANGLE, APERTURE={.162,.018,.162,.018}, ANGLE := kMBV.X1010021_BEND1V, TILT := tilt.MBV.X1010021_BEND1V;
 MBV.X1010029_BEND1V                               : K12_MTR__HWP    , APERTYPE=RECTANGLE, APERTURE={.162,.018,.162,.018}, ANGLE := kMBV.X1010029_BEND1V, TILT := tilt.MBV.X1010029_BEND1V;
 MBV.X1010033_BEND2V                               : K12_MTR__HWP    , APERTYPE=RECTANGLE, APERTURE={.162,.018,.162,.018}, ANGLE := kMBV.X1010033_BEND2V, TILT := tilt.MBV.X1010033_BEND2V;
 MQD.X1010036_QUAD4D                               : K12_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X1010036_QUAD4D;
 MQF.X1010041_QUAD5F                               : K12_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X1010041_QUAD5F;
 MQD.X1010046_QUAD6D                               : K12_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X1010046_QUAD6D;
 MQD.X1010061_QUAD7D                               : K12_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQD.X1010061_QUAD7D;
 MQF.X1010065_QUAD8F                               : K12_QNL__8WP    , APERTYPE=CIRCLE, APERTURE={.04,.04,.04,.04}, K1 := kMQF.X1010065_QUAD8F;
 MQD.X1010005_QUAD1D                               : K12_QNRB_8WP    , APERTYPE=CIRCLE, APERTURE={.025,.025,.025,.025}, K1 := kMQD.X1010005_QUAD1D;
 MQF.X1010009_QUAD2F                               : K12_QNRB_8WP    , APERTYPE=CIRCLE, APERTURE={.025,.025,.025,.025}, K1 := kMQF.X1010009_QUAD2F;
 MQF.X1010013_QUAD3D                               : K12_QNRB_8WP    , APERTYPE=CIRCLE, APERTURE={.025,.025,.025,.025}, K1 := kMQF.X1010013_QUAD3D;
 XCM.X1010090_SCR1V                                : K12_XCMV        , APERTYPE=RECTANGLE, APERTURE={.020001,.020001,.02,.02};
 CONVERTER01                                       : K12_XCON_010;

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

K12 : SEQUENCE, refer = centre,         L = 268.3;
 TBACB.X1010000                : K12_OMK                     , at = 0            , slot_id = 55490825;
 TCCV.X1010001                 : K12_TCCV                    , at = .775         , slot_id = 55491004;
 TCMAD.X1010001                : K12_TCMAD                   , at = 1.45         , slot_id = 56995904;
 XTCX.X1010003                 : K12_XTCX_001                , at = 2.4925       , slot_id = 55351611;
 QNRB.X1010005                 : MQD.X1010005_QUAD1D         , at = 5.245        , slot_id = 55348291;
 QNRB.X1010009                 : MQF.X1010009_QUAD2F         , at = 8.675        , slot_id = 55348300;
 MCXCA.X1010011                : MCBH.X1010011_TRIM1H        , at = 10.825       , slot_id = 55335520;
 QNRB.X1010013                 : MQF.X1010013_QUAD3D         , at = 12.875       , slot_id = 55348395;
 MTR.X1010017                  : MBV.X1010017_BEND1V         , at = 16.76        , slot_id = 55348426;
 MTR.X1010021                  : MBV.X1010021_BEND1V         , at = 20.96        , slot_id = 55348449;
 XTAX.X1010024                 : K12_XTAX_013                , at = 23.8775      , slot_id = 55348472;
 XCON.X1010025                 : CONVERTER01                 , at = 24.695       , slot_id = 55348525;
 XTAX.X1010026                 : K12_XTAX_014                , at = 25.5125      , slot_id = 55348548;
 MTR.X1010029                  : MBV.X1010029_BEND1V         , at = 28.47        , slot_id = 55348495;
 MTR.X1010033                  : MBV.X1010033_BEND2V         , at = 32.67        , slot_id = 55348571;
 QNL.X1010036                  : MQD.X1010036_QUAD4D         , at = 36.555       , slot_id = 55348598;
 XCSV.X1010038                 : K12_XCSV_004                , at = 39.005       , slot_id = 55348621;
 QNL.X1010041                  : MQF.X1010041_QUAD5F         , at = 41.33        , slot_id = 55348644;
 XCSH.X1010043                 : K12_XCSH_003                , at = 43.68        , slot_id = 55348668;
 QNL.X1010046                  : MQD.X1010046_QUAD6D         , at = 46.105       , slot_id = 55348691;
 XCSV.X1010048                 : K12_XCSV_004                , at = 48.455       , slot_id = 55348718;
 MCXCA.X1010049                : MCBH.X1010049_TRIM2H        , at = 49.5         , slot_id = 55348746;
 MBHHE.X1010051                : MBH.X1010051_BEND3H         , at = 51.335       , slot_id = 55348790;
 MBHHE.X1010054                : MBH.X1010054_BEND3H         , at = 54.235       , slot_id = 55348813;
 MBHHE.X1010057                : MBH.X1010057_BEND3H         , at = 57.135       , slot_id = 55348836;
 MCXCA.X1010059                : MCBH.X1010059_TRIM3H        , at = 58.97        , slot_id = 55348859;
 QNL.X1010061                  : MQD.X1010061_QUAD7D         , at = 61.155       , slot_id = 55348882;
 QNL.X1010065                  : MQF.X1010065_QUAD8F         , at = 64.585       , slot_id = 55348905;
 XCHV.X1010067                 : K12_XCHV_001                , at = 67.01        , slot_id = 55348929;
 MCXCA.X1010068                : MCBV.X1010068_TRIM4V        , at = 67.98        , slot_id = 55348952;
 XFFV.X1010069                 : K12_XFF__001                , at = 68.553       , slot_id = 55349036;
 XFFH.X1010070                 : K12_XFF__001                , at = 68.829       , slot_id = 55349059;
 XCED.X1010072                 : K12_XCEDH                   , at = 72.7325      , slot_id = 57616094;
 XFFV.X1010075                 : K12_XFF__001                , at = 75.693       , slot_id = 55349114;
 XFFH.X1010076                 : K12_XFF__001                , at = 75.969       , slot_id = 55349137;
 MQNEG.X1010077                : MQD.X1010077_QUAD9D         , at = 76.705       , slot_id = 55349160;
 MQNEG.X1010079                : MQF.X1010079_QUAD10F        , at = 78.705       , slot_id = 55349183;
 MBXHD.X1010082                : MBV.X1010082_BEND4V         , at = 81.41        , slot_id = 55349208;
 MBXHD.X1010085                : MBV.X1010085_BEND4V         , at = 85.01        , slot_id = 55349231;
 XCMV.X1010090                 : XCM.X1010090_SCR1V          , at = 89.81        , slot_id = 56720547;
 MBXHD.X1010095                : MBV.X1010095_BEND5V         , at = 94.29        , slot_id = 55539387;
 MBXHD.X1010098                : MBV.X1010098_BEND6V         , at = 97.89        , slot_id = 55539399;
 XTCX.X1010101                 : K12_XTCX_004                , at = 100.81       , slot_id = 56720098;
 MCXCA.X1010102                : MCBH.X1011002_TRIM5H        , at = 101.8        , slot_id = 55539411;
 MBMNB.X1010196                : MBXH.X1010197_MNP33AH - MBXH, at = 196.995      , slot_id = 56048447;
 XFFV.X1010245                 : K12_XFFB_001                , at = 245.4835     , slot_id = 57769055;
 XFFH.X1010246                 : K12_XFFB_001                , at = 245.6665     , slot_id = 57769091;
 MBHHJ.X1010249                : MBH.X1010249_BEND9H         , at = 249.2        , slot_id = 55539634;
 XWCM.X1010264                 : K12_XWCM_001                , at = 264.375      , slot_id = 57769766;
 XION.X1010265                 : K12_XION_002                , at = 264.625      , slot_id = 57577000;
ENDSEQUENCE;

return;