

  /**********************************************************************************
  *
  * LNE06 version (draft) YETS 2021-2022 in MAD X SEQUENCE format
  * Generated the 07-NOV-2024 03:08:00 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.LNE06_BSGWAMEC001            := 0.3;
l.LNE06_VVGBH                  := 0.085;
l.LNE06_ZCH                    := 0.037;
l.LNE06_ZCV                    := 0.037;
l.LNE06_ZDFHR                  := 0.4;
l.LNE06_ZDSHR001               := 0.371546;
l.LNE06_ZQ                     := 0.1;
l.LNE06_ZQFD                   := 0.1;
l.LNE06_ZQFF                   := 0.1;
l.LNE06_ZQMD                   := 0.1;
l.LNE06_ZQMF                   := 0.1;
l.LNE06_ZQNA                   := 0.39;

//---------------------- HCORRECTOR     ---------------------------------------------
LNE06_ZCH                : HCORRECTOR  , L := l.LNE06_ZCH;         ! Electrostatic Corrector, Horizontal
LNE06_ZDFHR              : HCORRECTOR  , L := l.LNE06_ZDFHR;       ! Electrostatic Deflector, Fast pulsed, Horizontal, Right
LNE06_ZDSHR001           : HCORRECTOR  , L := l.LNE06_ZDSHR001;    ! Electrostatic Deflector, Slow pulsed, Horizontal, Right, 48.085 deg
//---------------------- INSTRUMENT     ---------------------------------------------
LNE06_BSGWAMEC001        : INSTRUMENT  , L := l.LNE06_BSGWAMEC001; ! SEM Grid with Wire, type A - Mechanical Assembly
LNE06_VVGBH              : INSTRUMENT  , L := l.LNE06_VVGBH;       ! Vacuum valve gate all metal, electropneumatic controlled, DN100-CFF100 (VAT-S48)
//---------------------- QUADRUPOLE     ---------------------------------------------
LNE06_ZQ                 : QUADRUPOLE  , L := l.LNE06_ZQ;          ! Electrostatic Quadrupoles
LNE06_ZQFD               : QUADRUPOLE  , L := l.LNE06_ZQFD;        ! Electrostatic Quadrupoles, powered in series, Defocussing
LNE06_ZQFF               : QUADRUPOLE  , L := l.LNE06_ZQFF;        ! Electrostatic Quadrupoles, powered in series, Focussing
LNE06_ZQMD               : QUADRUPOLE  , L := l.LNE06_ZQMD;        ! Electrostatic Quadrupoles, powered independently, Defocussing
LNE06_ZQMF               : QUADRUPOLE  , L := l.LNE06_ZQMF;        ! Electrostatic Quadrupoles, powered independently, Focussing
LNE06_ZQNA               : QUADRUPOLE  , Lrad := l.LNE06_ZQNA;        ! Electrostatic Quadrupole, Normal, type A
//---------------------- VCORRECTOR     ---------------------------------------------
LNE06_ZCV                : VCORRECTOR  , L := l.LNE06_ZCV;         ! Electrostatic Corrector, Vertical


/************************************************************************************/
/*                      LNE06 SEQUENCE                                              */
/************************************************************************************/

LNE06 : SEQUENCE, refer = centre,       L = 11.018522;
 LNE.ZDFHR.0601                : LNE06_ZDFHR            , at = .3           , slot_id = 52781081;
 LNE.ZDSHR.0606                : LNE06_ZDSHR001         , at = .898486      , slot_id = 52781096;
 LNE.VV.0608                   : LNE06_VVGBH            , at = 1.238896     , slot_id = 52781128;
 LNE.BSGWA.0611                : LNE06_BSGWAMEC001      , at = 1.571896     , slot_id = 52781139;
 LNE.ZQMD.0612                 : LNE06_ZQMD             , at = 1.794396     , slot_id = 52781178, assembly_id= 52781163;
 LNE.ZCV.0612                  : LNE06_ZCV              , at = 1.892896     , slot_id = 52781179, assembly_id= 52781163;
 LNE.ZQNA.0612                 : LNE06_ZQNA             , at = 1.916896     , slot_id = 52781163;
 LNE.ZCH.0612                  : LNE06_ZCH              , at = 1.939896     , slot_id = 52781180, assembly_id= 52781163;
 LNE.ZQMF.0613                 : LNE06_ZQMF             , at = 2.038396     , slot_id = 52781181, assembly_id= 52781163;
 LNE.ZQMD.0618                 : LNE06_ZQMD             , at = 2.912896     , slot_id = 52781253, assembly_id= 52781238;
 LNE.ZCV.0618                  : LNE06_ZCV              , at = 3.011396     , slot_id = 52781254, assembly_id= 52781238;
 LNE.ZQNA.0618                 : LNE06_ZQNA             , at = 3.035396     , slot_id = 52781238;
 LNE.ZCH.0618                  : LNE06_ZCH              , at = 3.058396     , slot_id = 52781255, assembly_id= 52781238;
 LNE.ZQMF.0619                 : LNE06_ZQMF             , at = 3.156896     , slot_id = 52781256, assembly_id= 52781238;
 LNE.BSGWA.0624                : LNE06_BSGWAMEC001      , at = 3.709396     , slot_id = 52781313;
 LNE.ZQ.0625                   : LNE06_ZQ               , at = 3.931896     , slot_id = 52781352, assembly_id= 52781337;
 LNE.ZCV.0625                  : LNE06_ZCV              , at = 4.030396     , slot_id = 52781353, assembly_id= 52781337;
 LNE.ZQNA.0625                 : LNE06_ZQNA             , at = 4.054396     , slot_id = 52781337;
 LNE.ZCH.0625                  : LNE06_ZCH              , at = 4.077396     , slot_id = 52781354, assembly_id= 52781337;
 LNE.ZQFD.0626                 : LNE06_ZQFD             , at = 4.175896     , slot_id = 52781355, assembly_id= 52781337;
 LNE.BSGWA.0631                : LNE06_BSGWAMEC001      , at = 6.171096     , slot_id = 52781412;
 LNE.ZQ.0632                   : LNE06_ZQ               , at = 6.393596     , slot_id = 52781451, assembly_id= 52781436;
 LNE.ZCV.0632                  : LNE06_ZCV              , at = 6.492096     , slot_id = 52781452, assembly_id= 52781436;
 LNE.ZQNA.0632                 : LNE06_ZQNA             , at = 6.516096     , slot_id = 52781436;
 LNE.ZCH.0632                  : LNE06_ZCH              , at = 6.539096     , slot_id = 52781453, assembly_id= 52781436;
 LNE.ZQFF.0633                 : LNE06_ZQFF             , at = 6.637596     , slot_id = 52781454, assembly_id= 52781436;
 LNE.ZDFHR.0638                : LNE06_ZDFHR            , at = 7.076627     , slot_id = 52781513;
 LNE.ZDSHR.0643                : LNE06_ZDSHR001         , at = 7.675113     , slot_id = 52781528;
 LNE.ZQMD.0648                 : LNE06_ZQMD             , at = 8.201022     , slot_id = 52781568, assembly_id= 52781553;
 LNE.ZCV.0648                  : LNE06_ZCV              , at = 8.299522     , slot_id = 52781569, assembly_id= 52781553;
 LNE.ZQNA.0648                 : LNE06_ZQNA             , at = 8.323522     , slot_id = 52781553;
 LNE.ZCH.0648                  : LNE06_ZCH              , at = 8.346522     , slot_id = 52781570, assembly_id= 52781553;
 LNE.ZQMD.0649                 : LNE06_ZQMD             , at = 8.445022     , slot_id = 56476379, assembly_id= 52781553;
 LNE.BSGWA.0650                : LNE06_BSGWAMEC001      , at = 8.668522     , slot_id = 52781628;
 LNE.ZQMF.0655                 : LNE06_ZQMF             , at = 9.031022     , slot_id = 56476402, assembly_id= 52781652;
 LNE.ZCV.0655                  : LNE06_ZCV              , at = 9.129522     , slot_id = 52781668, assembly_id= 52781652;
 LNE.ZQNA.0655                 : LNE06_ZQNA             , at = 9.153522     , slot_id = 52781652;
 LNE.ZCH.0655                  : LNE06_ZCH              , at = 9.176522     , slot_id = 52781669, assembly_id= 52781652;
 LNE.ZQMD.0656                 : LNE06_ZQMD             , at = 9.275022     , slot_id = 56476425, assembly_id= 52781652;
 LNE.BSGWA.0657                : LNE06_BSGWAMEC001      , at = 9.498522     , slot_id = 52781727;
 LNE.VV.0662                   : LNE06_VVGBH            , at = 9.796022     , slot_id = 52781751;
ENDSEQUENCE;

return;