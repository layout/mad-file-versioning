

  /**********************************************************************************
  *
  * TT24 version (draft) EYETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 11-OCT-2024 03:39:25 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.TT24_BSG                     := 0;
l.TT24_BSGV_005                := 0;
l.TT24_BSI                     := 0;
l.TT24_BSM                     := 0.45;
l.TT24_BSPH_002                := 0;
l.TT24_BTV                     := 0;
l.TT24_MBB                     := 6.26;
l.TT24_MBNH_HWP                := 5;
l.TT24_MCXCDHWC                := 0.3;
l.TT24_MDAV                    := 1.4;
l.TT24_MDLH                    := 1.4;
l.TT24_MDLV                    := 1.4;
l.TT24_MTN__HWP                := 3.6;
l.TT24_OMK                     := 0;
l.TT24_QNLF                    := 0;
l.TT24_QTAD                    := 0;
l.TT24_QTLD                    := 2.99;
l.TT24_QTLF                    := 2.99;
l.TT24_TBID                    := 0.25;
l.TT24_TBIU                    := 0;
l.TT24_TCMAA                   := 0.4;
l.TT24_TCMAE                   := 0.35;
l.TT24_VVSA                    := 0.175;

//---------------------- COLLIMATOR     ---------------------------------------------
TT24_TCMAA               : COLLIMATOR  , L := l.TT24_TCMAA;        ! Collimation mask type A
TT24_TCMAE               : COLLIMATOR  , L := l.TT24_TCMAE;        ! Collimation mask type E
//---------------------- INSTRUMENT     ---------------------------------------------
TT24_BSGV_005            : INSTRUMENT  , L := l.TT24_BSGV_005;     ! SEM grid, vertical
TT24_BSI                 : INSTRUMENT  , L := l.TT24_BSI;          ! SEM intensity
TT24_BSM                 : INSTRUMENT  , L := l.TT24_BSM;          ! SEM position, moveable
TT24_BTV                 : INSTRUMENT  , L := l.TT24_BTV;          ! Beam observation TV with screen
TT24_TBIU                : INSTRUMENT  , L := l.TT24_TBIU;         ! target beam instrumentation, upstream
TT24_VVSA                : INSTRUMENT  , L := l.TT24_VVSA;         ! vacuum valve, sector, diameter 100 mm
//---------------------- KICKER         ---------------------------------------------
TT24_MCXCDHWC            : KICKER      , L := l.TT24_MCXCDHWC;     ! Corrector magnet, H or V, type MNPA30 - Old type name:  MNPA30.
//---------------------- MARKER         ---------------------------------------------
TT24_OMK                 : MARKER      , L := l.TT24_OMK;          ! TT24 markers
//---------------------- MONITOR        ---------------------------------------------
TT24_BSG                 : MONITOR     , L := l.TT24_BSG;          ! SEM grid, horizontal + vertical
TT24_BSPH_002            : MONITOR     , L := l.TT24_BSPH_002;     ! SEM position, horizontal
TT24_TBID                : MONITOR     , L := l.TT24_TBID;         ! target beam instrumentation, downstream
//---------------------- QUADRUPOLE     ---------------------------------------------
TT24_QNLF                : QUADRUPOLE  , L := l.TT24_QNLF;         ! quadrupole, secondary beams, north area type, focussing
TT24_QTAD                : QUADRUPOLE  , L := l.TT24_QTAD;         ! quadrupole, BT line, long, enlarged aperture, defocussing
TT24_QTLD                : QUADRUPOLE  , L := l.TT24_QTLD;         ! quadrupole, BT line, long, defocussing - Drawing: SPS8033070007
TT24_QTLF                : QUADRUPOLE  , L := l.TT24_QTLF;         ! quadrupole, BT line, long, focussing
//---------------------- RBEND          ---------------------------------------------
TT24_MBB                 : RBEND       , L := l.TT24_MBB;          ! Bending Magnet, main, type B2
TT24_MBNH_HWP            : RBEND       , L := l.TT24_MBNH_HWP;     ! Bending magnet, secondary beams, horizontal, north area
TT24_MDAV                : RBEND       , L := l.TT24_MDAV;         ! Correcting dipole, BT line, enlarged aperture, vertical deflection
TT24_MDLH                : RBEND       , L := l.TT24_MDLH;         ! Correcting dipole, BT line, long, horizontal deflection
TT24_MDLV                : RBEND       , L := l.TT24_MDLV;         ! Correcting dipole, BT line, long, vertical deflection - Drawing: SPSMDLV_0001
TT24_MTN__HWP            : RBEND       , L := l.TT24_MTN__HWP;     ! Bending magnet, Target N


/************************************************************************************/
/*                      TT24 SEQUENCE                                               */
/************************************************************************************/

TT24 : SEQUENCE, refer = centre,        L = 171.05094;
 BSGV.240102                   : TT24_BSGV_005          , at = 21.02714     , slot_id = 47534158;
 MDLV.240104                   : TT24_MDLV              , at = 22.43214     , slot_id = 47534159;
 MDLV.240107                   : TT24_MDLV              , at = 24.55014     , slot_id = 47534160;
 BTV.240119                    : TT24_BTV               , at = 31.26584     , slot_id = 47534161;
 QTLD.240200                   : TT24_QTLD              , at = 38.65884     , slot_id = 47534162;
 MDLV.240206                   : TT24_MDLV              , at = 41.58684     , slot_id = 47534164;
 MDLV.240209                   : TT24_MDLV              , at = 43.70484     , slot_id = 47534166;
 BSPH.240212                   : TT24_BSPH_002          , at = 45.11784     , slot_id = 47534167;
 BSGV.240212                   : TT24_BSGV_005          , at = 45.56785     , slot_id = 47534168;
 VVSA.240214                   : TT24_VVSA              , at = 45.68034     , slot_id = 57158687;
 QTLF.240300                   : TT24_QTLF              , at = 51.33704     , slot_id = 47534169;
 QTLF.240400                   : TT24_QTLF              , at = 55.00304     , slot_id = 47534170;
 MBB.240406                    : TT24_MBB               , at = 60.32704     , slot_id = 47534171;
 MBB.240417                    : TT24_MBB               , at = 66.96704     , slot_id = 47534172;
 MBB.240428                    : TT24_MBB               , at = 73.60704     , slot_id = 47534173;
 MBB.240439                    : TT24_MBB               , at = 80.24704     , slot_id = 47534174;
 MBB.240450                    : TT24_MBB               , at = 86.88704     , slot_id = 47534175;
 QNLF.240500                   : TT24_QNLF              , at = 91.86904     , slot_id = 47534176;
 QNLF.240600                   : TT24_QNLF              , at = 95.53504     , slot_id = 47534177;
 BSI.240610                    : TT24_BSI               , at = 100.74534    , slot_id = 47534178;
 BSG.240611                    : TT24_BSG               , at = 101.44534    , slot_id = 47534179;
 MDAV.240613                   : TT24_MDAV              , at = 102.85034    , slot_id = 47534180;
 QTAD.240700                   : TT24_QTAD              , at = 105.70634    , slot_id = 47534181;
 QTAD.240800                   : TT24_QTAD              , at = 109.37234    , slot_id = 47534182;
 QTAD.240900                   : TT24_QTAD              , at = 113.03834    , slot_id = 47534183;
 MDLH.240913                   : TT24_MDLH              , at = 120.35394    , slot_id = 47534184;
 QNLF.241000                   : TT24_QNLF              , at = 123.20994    , slot_id = 47534185;
 QNLF.241100                   : TT24_QNLF              , at = 126.87594    , slot_id = 47534187;
 BSM.241105                    : TT24_BSM               , at = 129.09894    , slot_id = 47534188;
 MBN.241107                    : TT24_MBNH_HWP          , at = 132.15394    , slot_id = 47534189;
 MCXCD.241119                  : TT24_MCXCDHWC          , at = 136.81544    , slot_id = 57343173;
 MTN.241128                    : TT24_MTN__HWP          , at = 143.53594    , slot_id = 47534190;
 MTN.241135                    : TT24_MTN__HWP          , at = 147.73594    , slot_id = 47534191;
 MTN.241142                    : TT24_MTN__HWP          , at = 151.93594    , slot_id = 47534192;
 TCMAE.241148                  : TT24_TCMAE             , at = 154.21094    , slot_id = 57128872;
 TBIU.241149                   : TT24_TBIU              , at = 154.56094    , slot_id = 47534193;
 TBACA.X0400000                : TT24_OMK               , at = 155.08594    , slot_id = 56964667;
 TBID.241150                   : TT24_TBID              , at = 155.41094    , slot_id = 47534194;
 TCMAA.X0400001                : TT24_TCMAA             , at = 155.93594    , slot_id = 56992277;
ENDSEQUENCE;

return;