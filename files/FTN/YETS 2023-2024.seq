

  /**********************************************************************************
  *
  * FTN version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 04-OCT-2023 02:13:42 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.FTN_BCWAB002                 := 0;
l.FTN_BSG                      := 0;
l.FTN_MBHGGHWC                 := 1;
l.FTN_MBXHCCWP                 := 2.5;
l.FTN_MQNDCTWP                 := 0.82;
l.FTN_MQNEGTWP                 := 0.8;
l.FTN_OMK                      := 0;
l.FTN_TBS__XX2                 := 1.457;
l.FTN_TRA                      := 0;
l.FTN_VWBTA                    := 0;

//---------------------- KICKER         ---------------------------------------------
FTN_MBHGGHWC   : KICKER      , L := l.FTN_MBHGGHWC;      ! Bending magnet, type M100, straight poles
//---------------------- MARKER         ---------------------------------------------
FTN_OMK        : MARKER      , L := l.FTN_OMK;           ! FTN markers
//---------------------- MONITOR        ---------------------------------------------
FTN_BCWAB002   : MONITOR     , L := l.FTN_BCWAB002;      ! Beam Wall Current monitor type B
FTN_BSG        : MONITOR     , L := l.FTN_BSG;           ! SEM grid, horizontal + vertical
FTN_TRA        : MONITOR     , L := l.FTN_TRA;           ! Beam Current Transformer Fast
//---------------------- PLACEHOLDER    ---------------------------------------------
FTN_TBS__XX2   : PLACEHOLDER , L := l.FTN_TBS__XX2;      ! Beam Stopper variant 002 - Same type as PXTBS__002, but with different flange-flange length
FTN_VWBTA      : PLACEHOLDER , L := l.FTN_VWBTA;         ! Vacuum Window DN200, material: Titanium, type A - ! Type is not confirmed, and needs to be checked !
//---------------------- QUADRUPOLE     ---------------------------------------------
FTN_MQNDCTWP   : QUADRUPOLE  , L := l.FTN_MQNDCTWP;      ! Quadrupole magnet, ISR, type QDS, 0.82m
FTN_MQNEGTWP   : QUADRUPOLE  , L := l.FTN_MQNEGTWP;      ! Quadrupole magnet, type QFS, 0.8m
//---------------------- RBEND          ---------------------------------------------
FTN_MBXHCCWP   : RBEND       , L := l.FTN_MBXHCCWP;      ! Bending Magnet, H or V, type HB1, 2.5m gap 80mm


/************************************************************************************/
/*                       FTN SEQUENCE                                               */
/************************************************************************************/

FTN : SEQUENCE, refer = centre,         L = 93.2146284;
 MBIH.100003.MARKER            : FTN_OMK         , at = 1.95         , slot_id = 5359533;
 MBIH.100012.MARKER            : FTN_OMK         , at = 5.305        , slot_id = 5359534;
 F16.QDN380                    : FTN_MQNDCTWP    , at = 8.83         , slot_id = 54873214;
 FTN.BHZ403                    : FTN_MBXHCCWP    , at = 15.047       , slot_id = 6154514;
 FTN.BHZ406                    : FTN_MBXHCCWP    , at = 18.403       , slot_id = 6154535;
 FTN.BHZ409                    : FTN_MBXHCCWP    , at = 21.757       , slot_id = 56051932;
 FTN.BTV414                    : FTN_OMK         , at = 25.839       , slot_id = 54873351;
 FTN.QFN415                    : FTN_MQNEGTWP    , at = 26.71        , slot_id = 56051982;
 FTN.STP426                    : FTN_TBS__XX2    , at = 37.010028    , slot_id = 6154536;
 FTN.STP428                    : FTN_TBS__XX2    , at = 38.551028    , slot_id = 6154537;
 FTN.QDN430                    : FTN_MQNDCTWP    , at = 39.962528    , slot_id = 56052018;
 FTN.QFN435                    : FTN_MQNEGTWP    , at = 53.123548    , slot_id = 56052057;
 FTN.DHZ436                    : FTN_MBHGGHWC    , at = 54.873548    , slot_id = 56056500;
 FTN.QDN450                    : FTN_MQNDCTWP    , at = 65.746468    , slot_id = 56056554;
 FTN.DVT451                    : FTN_MBHGGHWC    , at = 67.436468    , slot_id = 56056590;
 FTN.BTV454                    : FTN_OMK         , at = 74.697       , slot_id = 54873360;
 FTN.BHZ456                    : FTN_MBXHCCWP    , at = 76.654       , slot_id = 56056824;
 FTN.BHZ459                    : FTN_MBXHCCWP    , at = 80.06        , slot_id = 56056860;
 FTN.BHZ462                    : FTN_MBXHCCWP    , at = 83.464       , slot_id = 56056917;
 FTN.BCW474                    : FTN_BCWAB002    , at = 87.050558    , slot_id = 56433285;
 FTN.BCT477                    : FTN_TRA         , at = 87.578058    , slot_id = 56433308;
 FTN.BSGF484                   : FTN_BSG         , at = 90.284058    , slot_id = 56057038;
 FTN.VWB490                    : FTN_VWBTA       , at = 90.59527     , slot_id = 57367766;
ENDSEQUENCE;

return;