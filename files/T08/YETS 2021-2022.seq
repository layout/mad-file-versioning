/************************************************************************************************************************************************************************
*
* T08 version YETS 2021-2022 in MAD X SEQUENCE format
* Generated the 16-JAN-2025 03:46:20 from https://layout.cern.ch
*
*************************************************************************************************************************************************************************/



/************************************************************************************************************************************************************************/
/* TYPES DEFINITION                                                                                                                                                     */
/************************************************************************************************************************************************************************/

l.T08_BCTFV                    := 0;
l.T08_BTVMC101                 := 0.306;
l.T08_BTVMC150                 := 0.306;
l.T08_BTVMF007                 := 0.202;
l.T08_BXION001                 := 0.24;
l.T08_BXMWPCL01                := 0.13;
l.T08_MBXHDCWP                 := 2.5;
l.T08_MCXCFHWP                 := 0.4;
l.T08_MQNEFTWP                 := 1.2;
l.T08_MQNFKTWP                 := 2;
l.T08_OMK                      := 0;
l.T08_TMCTV                    := 0;
l.T08_XIBPM                    := 0.202;
l.T08_XIBPM002                 := 0.202;
l.T08_XIHTABCG1                := 0.61;
l.T08_XIHTABST1                := 1.6;
l.T08_XIHTABST2                := 1.05;
l.T08_XSEC_L01                 := 0.24;
l.T08_XVWAS002                 := 0.0001;

//---------------------- HKICKER        ---------------------------------------------
T08_MCXCFHWP   : HKICKER     , L := l.T08_MCXCFHWP;      ! Corrector magnet, H or V, type MDX laminated aperture 150mm
//---------------------- INSTRUMENT     ---------------------------------------------
T08_TMCTV      : INSTRUMENT  , L := l.T08_TMCTV;         ! Mobile Target for CHARM - TV Screen
T08_XIHTABCG1  : INSTRUMENT  , L := l.T08_XIHTABCG1;     ! Experiments - IRRAD - Support for irradiation table, cryogenic
T08_XIHTABST1  : INSTRUMENT  , L := l.T08_XIHTABST1;     ! Experiments - IRRAD - Support for irradiation table, standard (3 supports)
T08_XIHTABST2  : INSTRUMENT  , L := l.T08_XIHTABST2;     ! Experiments - IRRAD - Support for irradiation table, standard (2 supports)
T08_XVWAS002   : INSTRUMENT  , L := l.T08_XVWAS002;      ! X Vacuum Window Aluminium + Pumping Port DN159x99X0.1
//---------------------- MARKER         ---------------------------------------------
T08_OMK        : MARKER      , L := l.T08_OMK;           ! T08 markers
//---------------------- MONITOR        ---------------------------------------------
T08_BCTFV      : MONITOR     , L := l.T08_BCTFV;         ! Fast Beam Current Transformer, Type V
T08_BTVMC101   : MONITOR     , L := l.T08_BTVMC101;      ! Beam observation TV, with Magnetic Coupling, screen 100 type 1
T08_BTVMC150   : MONITOR     , L := l.T08_BTVMC150;      ! Beam observation TV, with Magnetic Coupling, screen 150
T08_BTVMF007   : MONITOR     , L := l.T08_BTVMF007;      ! Beam TV Motorised Flip, variant 007
T08_BXION001   : MONITOR     , L := l.T08_BXION001;      ! Secondary Emission Chamber with Gas - Detector
T08_BXMWPCL01  : MONITOR     , L := l.T08_BXMWPCL01;     ! Multi-Wire Proportional Chamber - Detector - Large
T08_XIBPM      : MONITOR     , L := l.T08_XIBPM;         ! Experiments - IRRAD - Beam Profile Monitor Support
T08_XIBPM002   : MONITOR     , L := l.T08_XIBPM002;      ! Experiments - IRRAD - Beam Profile Monitor Support - Long
T08_XSEC_L01   : MONITOR     , L := l.T08_XSEC_L01;      ! Ensemble: Secondary Emission Chamber et Support HXBEA - Long Version
//---------------------- QUADRUPOLE     ---------------------------------------------
T08_MQNEFTWP   : QUADRUPOLE  , L := l.T08_MQNEFTWP;      ! Quadrupole magnet, type QFL, 1.2m
T08_MQNFKTWP   : QUADRUPOLE  , L := l.T08_MQNFKTWP;      ! Quadrupole magnet, type Q200 laminated
//---------------------- SBEND          ---------------------------------------------
T08_MBXHDCWP   : SBEND       , L := l.T08_MBXHDCWP;      ! Bending Magnet, H or V, type HB2, 2.5m gap 80mm

/************************************************************************************************************************************************************************/
/* STRENGTH CONSTANTS                                                                                                                                                   */
/************************************************************************************************************************************************************************/

kF61.BHZ033                    := 0.045318;
kT08.BHZ005                    := 0.022599;
kT08.BHZ044                    := -0.03492;
kT08.BHZ047                    := -0.038;
kT08.DHZ025                    := 0;
kT08.DHZ059                    := 0;
tilt.F61.BHZ033                := -0;
tilt.T08.BHZ005                := -0;
tilt.T08.BHZ044                := -0;
tilt.T08.BHZ047                := -0;
tilt.T08.DHZ025                := -0;
tilt.T08.DHZ059                := -0;
tilt.T08.DVT026                := -0;
tilt.T08.DVT057                := -0;
kT08.DVT026                    := 0;
kT08.DVT057                    := 0;

/************************************************************************************************************************************************************************/
/* EXPERT NAMES                                                                                                                                                         */
/************************************************************************************************************************************************************************/

 T08.BCT072                                        : T08_BCTFV;
 T08.BTV035                                        : T08_BTVMC101;
 F62.BTV002                                        : T08_BTVMC150;
 T08.BTV020                                        : T08_BTVMF007;
 T08.XION071                                       : T08_BXION001;
 T08.XION094                                       : T08_BXION001;
 T08.BXMWPC103                                     : T08_BXMWPCL01;
 F61.BHZ033                                        : T08_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kF61.BHZ033, TILT := tilt.F61.BHZ033;
 T08.BHZ005                                        : T08_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kT08.BHZ005, TILT := tilt.T08.BHZ005;
 T08.BHZ044                                        : T08_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kT08.BHZ044, TILT := tilt.T08.BHZ044;
 T08.BHZ047                                        : T08_MBXHDCWP    , APERTYPE=RECTANGLE, APERTURE={.16,.04,.16,.04}, ANGLE := kT08.BHZ047, TILT := tilt.T08.BHZ047;
 T08.DHZ025                                        : T08_MCXCFHWP    , APERTYPE=RECTANGLE, APERTURE={.1,.075,.1,.075}, TILT := tilt.T08.DHZ025, HKICK := kT08.DHZ025;
 T08.DVT026                                        : T08_MCXCFHWP    , APERTYPE=RECTANGLE, APERTURE={.1,.075,.1,.075}, TILT := tilt.T08.DVT026, VKICK := kT08.DVT026;
 T08.DVT057                                        : T08_MCXCFHWP    , APERTYPE=RECTANGLE, APERTURE={.1,.075,.1,.075}, TILT := tilt.T08.DVT057, VKICK := kT08.DVT057;
 T08.DHZ059                                        : T08_MCXCFHWP    , APERTYPE=RECTANGLE, APERTURE={.1,.075,.1,.075}, TILT := tilt.T08.DHZ059, HKICK := kT08.DHZ059;
 T08.QFN021                                        : T08_MQNEFTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kT08.QFN021;
 T08.QDN023                                        : T08_MQNEFTWP    , APERTYPE=CIRCLE, APERTURE={.05,.05,.05,.05}, K1 := kT08.QDN023;
 T08.QDN061                                        : T08_MQNFKTWP    , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1}, K1 := kT08.QDN061;
 T08.QFN066                                        : T08_MQNFKTWP    , APERTYPE=CIRCLE, APERTURE={.1,.1,.1,.1}, K1 := kT08.QFN066;
 T08.XCHV038                                       : T08_OMK;
 T08.TBS049                                        : T08_OMK;
 T08.TBS050                                        : T08_OMK;
 T08.TBS063                                        : T08_OMK;
 T08.TBS064                                        : T08_OMK;
 T08.BLM068                                        : T08_OMK;
 T08.TMCRT097                                      : T08_OMK;
 T08.TDE115                                        : T08_OMK;
 T08.TDE119                                        : T08_OMK;
 T08.BTV096                                        : T08_TMCTV;
 T08.BPM02                                         : T08_XIBPM;
 T08.BPM03                                         : T08_XIBPM;
 T08.BPM04                                         : T08_XIBPM;
 T08.BPM01                                         : T08_XIBPM002;
 T08.TAB03                                         : T08_XIHTABCG1;
 T08.TAB01                                         : T08_XIHTABST1;
 T08.TAB02                                         : T08_XIHTABST1;
 T08.TAB04                                         : T08_XIHTABST2;
 T08.XSEC070                                       : T08_XSEC_L01;
 T08.XSEC095                                       : T08_XSEC_L01;
 VXWP                                              : T08_XVWAS002    , APERTYPE=CIRCLE, APERTURE={.3,.3,.3,.3};

/************************************************************************************************************************************************************************/
/* SEQUENCE                                                                                                                                                             */
/************************************************************************************************************************************************************************/

T08 : SEQUENCE, refer = centre,         L = 119.58;
 F61.MBXHD033                  : F61.BHZ033                  , at = 1.25         , slot_id = 53123107;
 F62.BTV002                    : F62.BTV002                  , at = 3.071        , slot_id = 53139085;
 T08.MBXHD005                  : T08.BHZ005                  , at = 7.901        , slot_id = 53174714;
 T08.BTV020                    : T08.BTV020                  , at = 22.14        , slot_id = 53165139;
 T08.MQNEF021                  : T08.QFN021                  , at = 23.29        , slot_id = 53185257;
 T08.MQNEF023                  : T08.QDN023                  , at = 25.29        , slot_id = 53192183;
 T08.MCXCF025                  : T08.DHZ025                  , at = 27.29        , slot_id = 53195604, assembly_id= 53198046;
 T08.MCXCF026                  : T08.DVT026                  , at = 28.34        , slot_id = 53195619, assembly_id= 53198046;
 T08.BTV035                    : T08.BTV035                  , at = 41.496       , slot_id = 53165148;
 T08.LJ038                     : T08.XCHV038                 , at = 43.689       , slot_id = 56477216;
 T08.MBXHD044                  : T08.BHZ044                  , at = 46.05        , slot_id = 53198055;
 T08.MBXHD047                  : T08.BHZ047                  , at = 49.542       , slot_id = 53198085;
 T08.TBS049                    : T08.TBS049                  , at = 51.601       , slot_id = 53237996;
 T08.TBS050                    : T08.TBS050                  , at = 52.37        , slot_id = 53238027;
 T08.MCXCF057                  : T08.DVT057                  , at = 59.873       , slot_id = 53301808;
 T08.MCXCF059                  : T08.DHZ059                  , at = 61.638       , slot_id = 53301829;
 T08.MQNFK061                  : T08.QDN061                  , at = 63.583       , slot_id = 53301889;
 T08.TBS063                    : T08.TBS063                  , at = 65.503       , slot_id = 53301910;
 T08.TBS064                    : T08.TBS064                  , at = 66.272       , slot_id = 53301925;
 T08.MQNFK066                  : T08.QFN066                  , at = 68.143       , slot_id = 53301940;
 T08.TBS068                    : T08.BLM068                  , at = 70.295       , slot_id = 53301970;
 T08.XVW070                    : VXWP                        , at = 70.7908      , slot_id = 57952498;
 T08.XSEC070                   : T08.XSEC070                 , at = 71.022       , slot_id = 53301985;
 T08.XION071                   : T08.XION071                 , at = 71.362       , slot_id = 54949486;
 T08.BCTFV072                  : T08.BCT072                  , at = 71.7045      , slot_id = 54910711;
 T08.XIBPM073                  : T08.BPM01                   , at = 72.221       , slot_id = 54949511;
 T08.XIHTAB078                 : T08.TAB01                   , at = 77.343       , slot_id = 54949582;
 T08.XIBPM080                  : T08.BPM02                   , at = 80           , slot_id = 54949520;
 T08.XIHTAB081                 : T08.TAB02                   , at = 80.833       , slot_id = 54949588;
 T08.XIBPM085                  : T08.BPM03                   , at = 87.233       , slot_id = 54949629;
 T08.XIHTAB091                 : T08.TAB03                   , at = 89.244       , slot_id = 54949607;
 T08.XIHTAB092                 : T08.TAB04                   , at = 91.639       , slot_id = 55031018;
 T08.XIBPM092                  : T08.BPM04                   , at = 92           , slot_id = 54949529;
 T08.XION094                   : T08.XION094                 , at = 94.9335      , slot_id = 53302032;
 T08.XSEC095                   : T08.XSEC095                 , at = 95.3735      , slot_id = 53302051;
 T08.BTV096                    : T08.BTV096                  , at = 99.0905      , slot_id = 54935319;
 T08.TMCRT097                  : T08.TMCRT097                , at = 99.7884      , slot_id = 53302073;
 T08.TMCM102                   : T08_OMK                     , at = 104.3034     , slot_id = 57745244;
 T08.XWCM103                   : T08.BXMWPC103               , at = 104.4895     , slot_id = 53302086;
 T08.TDE115                    : T08.TDE115                  , at = 111.572      , slot_id = 57123529;
 T08.TDE119                    : T08.TDE119                  , at = 118.3805     , slot_id = 57123485;
ENDSEQUENCE;

return;