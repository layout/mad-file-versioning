

  /**********************************************************************************
  *
  * FTA version (draft) YETS 2023-2024 in MAD X SEQUENCE format
  * Generated the 04-OCT-2023 10:53:20 from Layout
  *
  ***********************************************************************************/



/************************************************************************************/
/*                       TYPES DEFINITION                                           */
/************************************************************************************/

l.FTA_BCT                      := 0;
l.FTA_BTVBP051                 := 0;
l.FTA_BTVPL119                 := 0;
l.FTA_BTVTA                    := 0;
l.FTA_MBHBDCWP                 := 1.96;
l.FTA_MBHFDCWP                 := 1.96;
l.FTA_MBXHCCWP                 := 2.5;
l.FTA_MCXCCHWC                 := 0;
l.FTA_MQMDATAC                 := 0;
l.FTA_MQNDCTWP                 := 0.82;
l.FTA_MSG                      := 0;
l.FTA_OMK                      := 0;

//---------------------- CORRECTOR      ---------------------------------------------
FTA_MCXCCHWC   : CORRECTOR   , L := l.FTA_MCXCCHWC;      ! Corrector magnet, H or V, type MEA19
//---------------------- MARKER         ---------------------------------------------
FTA_OMK        : MARKER      , L := l.FTA_OMK;           ! FTA markers
//---------------------- MONITOR        ---------------------------------------------
FTA_BCT        : MONITOR     , L := l.FTA_BCT;           ! Beam current transformer
FTA_BTVBP051   : MONITOR     , L := l.FTA_BTVBP051;      ! Beam TV Booster Type, Pneumatic
FTA_BTVPL119   : MONITOR     , L := l.FTA_BTVPL119;      ! Beam observation TV Pneumatic Linear, variant 119
FTA_BTVTA      : MONITOR     , L := l.FTA_BTVTA;         ! Beam observation TV, Target Area (AD)
FTA_MSG        : MONITOR     , L := l.FTA_MSG;           ! SEM grid
//---------------------- QUADRUPOLE     ---------------------------------------------
FTA_MQMDATAC   : QUADRUPOLE  , L := l.FTA_MQMDATAC;      ! Quadrupole high gradient permanent magnet, AD target
FTA_MQNDCTWP   : QUADRUPOLE  , L := l.FTA_MQNDCTWP;      ! Quadrupole magnet, ISR, type QDS, 0.82m
//---------------------- RBEND          ---------------------------------------------
FTA_MBXHCCWP   : RBEND       , L := l.FTA_MBXHCCWP;      ! Bending Magnet, H or V, type HB1, 2.5m gap 80mm
//---------------------- SBEND          ---------------------------------------------
FTA_MBHBDCWP   : SBEND       , L := l.FTA_MBHBDCWP;      ! Bending magnet, type FO56
FTA_MBHFDCWP   : SBEND       , L := l.FTA_MBHFDCWP;      ! Bending magnet, type B190


/************************************************************************************/
/*                       FTA SEQUENCE                                               */
/************************************************************************************/

FTA : SEQUENCE, refer = centre,         L = 60.145965;
 F16.BTI247                    : FTA_MBXHCCWP    , at = 1.879215     , slot_id = 5359495;
 F16.BTI248                    : FTA_MBXHCCWP    , at = 5.209215     , slot_id = 5359496;
 FTA.BTV9003                   : FTA_BTVBP051    , at = 7.709411     , slot_id = 56424141;
 FTA.QDN9010                   : FTA_MQNDCTWP    , at = 10.128915    , slot_id = 53781841;
 FTA.BCT9012                   : FTA_BCT         , at = 11.044265    , slot_id = 54963570;
 FTA.BTI9015                   : FTA_MBHFDCWP    , at = 12.529673    , slot_id = 6154540;
 FTA.BTI9016                   : FTA_MBHFDCWP    , at = 15.130933    , slot_id = 14323258;
 FTA.QFN9020                   : FTA_MQNDCTWP    , at = 26.572615    , slot_id = 53781850;
 FTA.STP9021                   : FTA_OMK         , at = 27.952615    , slot_id = 6154541;
 FTA.STP9022                   : FTA_OMK         , at = 29.512615    , slot_id = 6154542;
 FTA.BSG9027                   : FTA_MSG         , at = 31.903615    , slot_id = 54964754;
 FTA.DHZ9028                   : FTA_MCXCCHWC    , at = 32.428615    , slot_id = 53781880;
 FTA.DVT9029                   : FTA_MCXCCHWC    , at = 33.028615    , slot_id = 53781889;
 FTA.QDN9030                   : FTA_MQNDCTWP    , at = 33.878615    , slot_id = 53781907;
 FTA.BTV9031                   : FTA_BTVPL119    , at = 34.704615    , slot_id = 53455436;
 FTA.QFN9040                   : FTA_MQNDCTWP    , at = 41.478615    , slot_id = 53781916;
 FTA.BTV9043                   : FTA_BTVPL119    , at = 48.553715    , slot_id = 53455451;
 FTA.BVT9044                   : FTA_MBHBDCWP    , at = 49.0928155   , slot_id = 53781861;
 FTA.BVT9045                   : FTA_MBHFDCWP    , at = 50.88787884  , slot_id = 53781870;
 FTA.DHZ9047                   : FTA_MCXCCHWC    , at = 52.433815    , slot_id = 53781898;
 FTA.QDN9050                   : FTA_MQMDATAC    , at = 55.205365    , slot_id = 52785661;
 FTA.QFN9052                   : FTA_MQMDATAC    , at = 56.705365    , slot_id = 52785676;
 FTA.BCT9053                   : FTA_BCT         , at = 57.497365    , slot_id = 53455346;
 FTA.BTV9064                   : FTA_BTVTA       , at = 58.916365    , slot_id = 53455356;
 FTA.TAR9065                   : FTA_OMK         , at = 59.468365    , slot_id = 53455378;
ENDSEQUENCE;

return;